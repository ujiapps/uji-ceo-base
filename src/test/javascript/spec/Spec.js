describe("ContentType", function()
{
    it("Debe extraerse los tags introducidos en un texto", function()
    {
        var text = '${tag1} y ${tag2} ${tag2}con un ${tag3}';

        var tags = UJI.SEU.Application.extractTags(text);

        expect(3).toEqual(tags.length);

        expect('${tag1}').toEqual(tags[0]);
        expect('${tag2}').toEqual(tags[1]);
        expect('${tag3}').toEqual(tags[2]);
    });

    it("Deben reemplazarse los tags introducidos en un texto", function()
    {
        var text = '${tag1} y ${tag2} ${tag2}con un ${tag3}';

        var replacedText = UJI.SEU.Application.replaceAll(text, '${tag2}', 'pepe');

        expect(replacedText).toEqual('${tag1} y pepe pepecon un ${tag3}');
    });
});