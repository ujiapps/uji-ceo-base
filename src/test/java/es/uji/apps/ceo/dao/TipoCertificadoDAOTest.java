package es.uji.apps.ceo.dao;

import es.uji.apps.ceo.builders.TipoCertificadoBuilder;
import es.uji.apps.ceo.model.TipoCertificado;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext-test.xml" })
@TransactionConfiguration(defaultRollback = false)
@Ignore
public class TipoCertificadoDAOTest
{
    @Autowired
    private TipoCertificadoDAO tipoCertificadoDAO;

    private Long tipoCertificadoId;

    @Before
    @Transactional
    public void rellenaDatos()
    {
        TipoCertificado tipoCertificado = new TipoCertificadoBuilder(tipoCertificadoDAO).withDescripcionCa("Tipo Certificat 1")
                .withDescripcionEs("Tipo Certificado 1").withDescripcionUk("Certificate type 1")
                .withPlantillaCa("Plantilla CA").withPlantillaEs("Plantilla ES")
                .withPlantillaUk("Plantilla UK").build();
        tipoCertificadoId = tipoCertificado.getId();
    }

    @Test
    public void getTipoCertificadoByIdTest() throws RegistroNoEncontradoException
    {
        TipoCertificado tipoCertificado = TipoCertificado.getTipoCertificadoById(tipoCertificadoId);
        assertThat(tipoCertificado.getId(), is(new Long(tipoCertificadoId)));
    }
}
