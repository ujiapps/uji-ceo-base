package es.uji.apps.ceo.services;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;
import es.uji.apps.ceo.model.Certificado;
import es.uji.commons.rest.UIEntity;
import org.springframework.http.MediaType;

import java.util.List;

import static es.uji.commons.testing.hamcrest.ClientOkResponseMatcher.okClientResponse;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class TipoCertificadoAdapter
{
    private static final String URL_RECURSO_TIPOCERTIFICADO = "tipocertificado/";
    private static final String URL_RECURSO_TIPOCERTIFICADO_MODERAR = "tipocertificado/moderacion";

    private WebResource resource;

    public TipoCertificadoAdapter(WebResource resource)
    {
        this.resource = resource;
    }

    public List<UIEntity> getTiposCertificado()
    {
        ClientResponse response = resource.path(URL_RECURSO_TIPOCERTIFICADO).accept(MediaType.TEXT_XML_VALUE)
                .get(ClientResponse.class);

        assertThat(response, is(okClientResponse()));

        return response.getEntity(new GenericType<List<UIEntity>>()
        {
        });
    }

    public Certificado addCertificadoPendienteAprobarAModerador() {
        Certificado certificado = new Certificado();

        return certificado;
    }

    public List<UIEntity> getTiposCertificadoPendientesModeracion() {
        ClientResponse response = resource.path(URL_RECURSO_TIPOCERTIFICADO_MODERAR).accept(MediaType.TEXT_XML_VALUE)
                .get(ClientResponse.class);

        assertThat(response, is(okClientResponse()));

        return response.getEntity(new GenericType<List<UIEntity>>()
        {
        });

    }
}
