package es.uji.apps.ceo.services;

import es.uji.apps.ceo.builders.TipoCertificadoBuilder;
import es.uji.apps.ceo.builders.TipoCertificadoModeracionBuilder;
import es.uji.apps.ceo.builders.TipoCertificadoPersonaBuilder;
import es.uji.apps.ceo.dao.TipoCertificadoDAO;
import es.uji.apps.ceo.dao.TipoCertificadoModDAO;
import es.uji.apps.ceo.dao.TipoCertificadoPersonaDAO;
import es.uji.apps.ceo.model.TipoCertificado;

public class TipoCertificadoConstructor {
    private TipoCertificadoDAO tipoCertificadoDAO;
    private TipoCertificadoPersonaDAO tipoCertificadoPersonaDAO;
    private TipoCertificadoModDAO tipoCertificadoModDAO;

    private TipoCertificado tipoCertificadoAdmin;
    private TipoCertificado tipoCertificadoPersona;
    private TipoCertificado tipoCertificadoModerador;

    public static Long administradorId = new Long(1);
    public static Long moderadorId = new Long(2);
    public static Long personaId = new Long(3);

    public TipoCertificado getTipoCertificadoAdmin() {
        return tipoCertificadoAdmin;
    }

    public TipoCertificado getTipoCertificadoPersona() {
        return tipoCertificadoPersona;
    }

    public TipoCertificado getTipoCertificadoModerador() {
        return tipoCertificadoModerador;
    }

    public TipoCertificadoConstructor(TipoCertificadoDAO tipoCertificadoDAO) {
        this.tipoCertificadoDAO = tipoCertificadoDAO;
        this.init();
    }

    public TipoCertificadoConstructor(TipoCertificadoDAO tipoCertificadoDAO, TipoCertificadoPersonaDAO tipoCertificadoPersonaDAO) {
        this.tipoCertificadoPersonaDAO = tipoCertificadoPersonaDAO;
        this.tipoCertificadoDAO = tipoCertificadoDAO;
        this.init();
    }

    public TipoCertificadoConstructor(TipoCertificadoDAO tipoCertificadoDAO, TipoCertificadoModDAO tipoCertificadoModDAO) {
        this.tipoCertificadoModDAO = tipoCertificadoModDAO;
        this.tipoCertificadoDAO = tipoCertificadoDAO;
        this.init();
    }

    public void init() {
        tipoCertificadoAdmin = new TipoCertificadoBuilder(tipoCertificadoDAO)
                .withDescripcionCa("Tipo Certificat 1 permiso solo Administrador").withDescripcionEs("Tipo Certificado 1")
                .withDescripcionUk("Certificate type 1").withPlantillaCa("Plantilla CA")
                .withPlantillaEs("Plantilla ES").withPlantillaUk("Plantilla UK").build();

        tipoCertificadoPersona = new TipoCertificadoBuilder(tipoCertificadoDAO)
                .withDescripcionCa("Tipo Certificat 2 permiso usuario").withDescripcionEs("Tipo Certificado 2")
                .withDescripcionUk("Certificate type 2").withPlantillaCa("Plantilla CA")
                .withPlantillaEs("Plantilla ES").withPlantillaUk("Plantilla UK").build();
        new TipoCertificadoPersonaBuilder(tipoCertificadoPersonaDAO).withPerId(personaId).withTipoCertificadoId(tipoCertificadoPersona.getId()).build();

        tipoCertificadoModerador = new TipoCertificadoBuilder(tipoCertificadoDAO)
                .withDescripcionCa("Tipo Certificat 3 permiso moderador").withDescripcionEs("Tipo Certificado 3")
                .withDescripcionUk("Certificate type 3").withPlantillaCa("Plantilla CA")
                .withPlantillaEs("Plantilla ES").withPlantillaUk("Plantilla UK").build();
        new TipoCertificadoModeracionBuilder(tipoCertificadoModDAO).withPerId(moderadorId).withTipoCertificadoId(tipoCertificadoModerador.getId()).build();
    }
}
