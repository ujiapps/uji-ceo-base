package es.uji.apps.ceo.services;

import es.uji.apps.ceo.dao.TipoCertificadoDAO;
import es.uji.apps.ceo.model.TipoCertificado;
import es.uji.commons.rest.UIEntity;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/applicationContext-test.xml" })
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = false)
public class TipoCertificadoAdminServiceTest extends JerseyBaseTest
{
    @Autowired
    private TipoCertificadoDAO tipoCertificadoDAO;

    private TipoCertificado tipoCertificado;

    public TipoCertificadoAdminServiceTest() {
        super("administrador", TipoCertificadoConstructor.administradorId.toString());
    }

    @Before
    @Transactional
    public void init()
    {
        TipoCertificadoConstructor tipoCertificadoConstructor = new TipoCertificadoConstructor(tipoCertificadoDAO);
    }

    @Test
    @Transactional
    public void recuperaLaListaDeTiposDeCertificadoPorUnAdministrador()
    {
        List<UIEntity> listaTiposCertificado = tipoCertificadoAdapter.getTiposCertificado();
        assertThat(listaTiposCertificado, hasSize(3));
    }
}
