package es.uji.apps.ceo.services;

import ch.qos.logback.ext.spring.web.LogbackConfigListener;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.spi.spring.container.servlet.SpringServlet;
import com.sun.jersey.test.framework.JerseyTest;
import com.sun.jersey.test.framework.WebAppDescriptor;
import com.sun.jersey.test.framework.spi.container.TestContainerFactory;
import com.sun.jersey.test.framework.spi.container.grizzly.web.GrizzlyWebTestContainerFactory;
import es.uji.apps.ceo.UIEntityMessageBodyReader;
import es.uji.commons.rest.xml.UIEntityListMessageBodyReader;
import es.uji.commons.rest.xml.UIEntityMessageBodyWriter;
import es.uji.commons.sso.filters.UjiAppsFilter;
import es.uji.si.SSO.filter.LsmFilter;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.request.RequestContextListener;

import java.util.HashMap;
import java.util.Map;

public class JerseyBaseTest extends JerseyTest
{
    protected WebResource resource;
    protected TipoCertificadoAdapter tipoCertificadoAdapter;

    public JerseyBaseTest(String login, String personaId)
    {
        super(new WebAppDescriptor.Builder("es.uji.commons.rest.shared; es.uji.commons.rest.xml; es.uji.apps.ceo.services")
                        .contextParam("contextConfigLocation", "classpath:applicationContext-test.xml")
                        .contextParam("webAppRootKey", "uji-ceo-base.root")
                        .contextListenerClass(LogbackConfigListener.class)
                        .contextListenerClass(ContextLoaderListener.class)
                        .requestListenerClass(RequestContextListener.class)
                        .servletClass(SpringServlet.class)
                        .addFilter(LsmFilter.class, "/*", getLsmAuthFilterConfig(login, personaId))
                        .addFilter(UjiAppsFilter.class, "/*", getUjiAppsAuthFilterConfig(login, personaId))
                        .clientConfig(createClientConfig()).build()
        );

        this.resource = resource();
        tipoCertificadoAdapter = new TipoCertificadoAdapter(resource);
    }

    private static Map<String, String> getLsmAuthFilterConfig(String login, String personaId)
    {
        Map<String, String> params = new HashMap<String, String>();

        params.put("ssoAuthentication", "true");
        params.put("dontFilterIfLocalhost", "true");

        return params;
    }

    private static Map<String, String> getUjiAppsAuthFilterConfig(String login, String personaId)
    {
        Map<String, String> params = new HashMap<String, String>();

        params.put("defaultUserName", login);
        params.put("defaultUserId", personaId);

        return params;
    }

    private static ClientConfig createClientConfig()
    {
        ClientConfig config = new DefaultClientConfig();
        config.getClasses().add(UIEntityMessageBodyReader.class);
        config.getClasses().add(UIEntityListMessageBodyReader.class);
        config.getClasses().add(UIEntityMessageBodyWriter.class);

        return config;
    }

    @Override
    protected TestContainerFactory getTestContainerFactory()
    {
        return new GrizzlyWebTestContainerFactory();
    }
}
