package es.uji.apps.ceo.services;

import es.uji.apps.ceo.dao.TipoCertificadoDAO;
import es.uji.apps.ceo.model.TipoCertificado;
import es.uji.commons.rest.UIEntity;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/applicationContext-test.xml" })
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = false)
public class TipoCertificadoModeradorServiceTest extends JerseyBaseTest
{
    @Autowired
    private TipoCertificadoDAO tipoCertificadoDAO;

    private TipoCertificado tipoCertificado;

    public TipoCertificadoModeradorServiceTest() {
        super("moderador", TipoCertificadoConstructor.moderadorId.toString());
    }

    @Before
    @Transactional
    public void init()
    {
        TipoCertificadoConstructor tipoCertificadoConstructor = new TipoCertificadoConstructor(tipoCertificadoDAO);
        tipoCertificado = tipoCertificadoConstructor.getTipoCertificadoModerador();
    }

    @Test
    @Transactional
    public void recuperaLaListaDeTiposDeCertificadoSinCertificadosPendientesDeAprobar()
    {
        List<UIEntity> tiposCertificado = tipoCertificadoAdapter.getTiposCertificadoPendientesModeracion();
        assertThat(tiposCertificado, hasSize(0));
    }

    @Test
    @Transactional
    public void recuperaLaListaDeTiposDeCertificadoSinCertificadosPendienteDeAprobar()
    {
        List<UIEntity> tiposCertificado = tipoCertificadoAdapter.getTiposCertificadoPendientesModeracion();
        assertThat(tiposCertificado, hasSize(0));
    }

    @Test
    @Transactional
    public void recuperaLaListaDeTiposDeCertificadoConUnCertificadoPendienteDeAprobar()
    {

        tipoCertificadoAdapter.addCertificadoPendienteAprobarAModerador();
        List<UIEntity> tiposCertificado = tipoCertificadoAdapter.getTiposCertificadoPendientesModeracion();
        assertThat(tiposCertificado, hasSize(1));
    }

}