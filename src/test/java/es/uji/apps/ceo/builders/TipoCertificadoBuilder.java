package es.uji.apps.ceo.builders;
import es.uji.apps.ceo.dao.TipoCertificadoDAO;
import es.uji.apps.ceo.model.TipoCertificado;

public class TipoCertificadoBuilder
{
    private TipoCertificado tipoCertificado;
    private TipoCertificadoDAO tipoCertificadoDAO;
    
    public TipoCertificadoBuilder(TipoCertificadoDAO tipoCertificadoDAO) {
        this.tipoCertificadoDAO = tipoCertificadoDAO;
        tipoCertificado = new TipoCertificado();
    }
    
    public TipoCertificadoBuilder() {
        this(null);
    }
    
    public TipoCertificadoBuilder withId(Long id) {
        tipoCertificado.setId(id);
        return this;
    }
    
    public TipoCertificadoBuilder withDescripcionCa(String descripcionCa) {
        tipoCertificado.setDescripcionCa(descripcionCa);
        return this;
    }
    
    public TipoCertificadoBuilder withDescripcionEs(String descripcionEs) {
        tipoCertificado.setDescripcionEs(descripcionEs);
        return this;
    }

    public TipoCertificadoBuilder withDescripcionUk(String descripcionUk) {
        tipoCertificado.setDescripcionUk(descripcionUk);
        return this;
    }
    
    public TipoCertificadoBuilder withPlantillaCa(String plantillaCa) {
        tipoCertificado.setPlantillaCa(plantillaCa);
        return this;
    }

    public TipoCertificadoBuilder withPlantillaEs(String plantillaEs) {
        tipoCertificado.setPlantillaEs(plantillaEs);
        return this;
    }

    public TipoCertificadoBuilder withPlantillaUk(String plantillaUk) {
        tipoCertificado.setPlantillaUk(plantillaUk);
        return this;
    }
    
    public TipoCertificado build()
    {
        if (tipoCertificadoDAO != null)
        {
            tipoCertificado = tipoCertificadoDAO.insert(tipoCertificado);
        }

        return tipoCertificado;
    }

}