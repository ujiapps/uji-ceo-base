package es.uji.apps.ceo.builders;

import es.uji.apps.ceo.dao.TipoCertificadoPersonaDAO;
import es.uji.apps.ceo.model.TipoCertificadoPersona;

public class TipoCertificadoPersonaBuilder
{
    private TipoCertificadoPersona tipoCertificadoPersona;
    private TipoCertificadoPersonaDAO tipoCertificadoPersonaDAO;

    public TipoCertificadoPersonaBuilder(TipoCertificadoPersonaDAO tipoCertificadoPersonaDAO) {
        this.tipoCertificadoPersonaDAO = tipoCertificadoPersonaDAO;
        tipoCertificadoPersona = new TipoCertificadoPersona();
    }

    public TipoCertificadoPersonaBuilder() {
        this(null);
    }
    
    public TipoCertificadoPersonaBuilder withPerId(Long perId) {
        tipoCertificadoPersona.setPerId(perId);
        return this;
    }
    
    public TipoCertificadoPersonaBuilder withTipoCertificadoId(Long tipoCertificadoId) {
        tipoCertificadoPersona.setTipoCertificadoId(tipoCertificadoId);
        return this;
    }
    
   public TipoCertificadoPersona build()
    {
        if (tipoCertificadoPersonaDAO != null)
        {
            tipoCertificadoPersona = tipoCertificadoPersonaDAO.insert(tipoCertificadoPersona);
        }

        return tipoCertificadoPersona;
    }
}