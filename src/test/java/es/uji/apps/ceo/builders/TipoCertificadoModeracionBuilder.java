package es.uji.apps.ceo.builders;

import es.uji.apps.ceo.dao.TipoCertificadoModDAO;
import es.uji.apps.ceo.model.TipoCertificadoModeracion;

public class TipoCertificadoModeracionBuilder
{
    private TipoCertificadoModeracion tipoCertificadoModeracion;
    private TipoCertificadoModDAO tipoCertificadoModDAO;

    public TipoCertificadoModeracionBuilder(TipoCertificadoModDAO tipoCertificadoModDAO) {
        this.tipoCertificadoModDAO = tipoCertificadoModDAO;
        tipoCertificadoModeracion = new TipoCertificadoModeracion();
    }

    public TipoCertificadoModeracionBuilder() {
        this(null);
    }
    
    public TipoCertificadoModeracionBuilder withPerId(Long perId) {
        tipoCertificadoModeracion.setPerId(perId);
        return this;
    }
    
    public TipoCertificadoModeracionBuilder withTipoCertificadoId(Long tipoCertificadoId) {
        tipoCertificadoModeracion.setTipoCertificadoId(tipoCertificadoId);
        return this;
    }
    
   public TipoCertificadoModeracion build()
    {
        if (tipoCertificadoModDAO != null)
        {
            tipoCertificadoModeracion = tipoCertificadoModDAO.insert(tipoCertificadoModeracion);
        }

        return tipoCertificadoModeracion;
    }

}