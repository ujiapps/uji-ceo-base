package es.uji.apps.ceo;

import es.uji.commons.rest.UIEntity;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import javax.ws.rs.Consumes;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyReader;
import javax.ws.rs.ext.Provider;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

@Provider
@Consumes({ MediaType.APPLICATION_XML, MediaType.TEXT_XML, MediaType.APPLICATION_FORM_URLENCODED })
public class UIEntityMessageBodyReader implements MessageBodyReader<UIEntity>
{
    @Override
    public boolean isReadable(Class<?> arg0, Type arg1, Annotation[] arg2, MediaType arg3)
    {
        return "class es.uji.commons.rest.UIEntity".equals(arg0.toString());
    }

    @Override
    public UIEntity readFrom(Class<UIEntity> arg0, Type arg1, Annotation[] arg2, MediaType arg3,
            MultivaluedMap<String, String> arg4, InputStream arg5) throws IOException,
            WebApplicationException
    {
        UIEntity params = new UIEntity();

        String receivedContentType = arg3.toString();

        if (receivedContentType.startsWith(MediaType.TEXT_XML)
                || receivedContentType.startsWith(MediaType.APPLICATION_XML))
        {
            try
            {
                DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory
                        .newInstance();
                documentBuilderFactory.setNamespaceAware(true);
                DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
                Document document = documentBuilder.parse(arg5);

                return getParams(document.getDocumentElement());
            }
            catch (Exception e)
            {
            }
        }

        return params;
    }

    public UIEntity getParams(Node root)
    {
        NodeList nodeList = root.getChildNodes();
        UIEntity params = new UIEntity(root.getNodeName());

        for (int i = 0; i < nodeList.getLength(); i++)
        {
            Element currentNode = (Element) nodeList.item(i);

            if (currentNode.getNodeType() == Node.ELEMENT_NODE
                    && currentNode.getFirstChild() != null
                    && currentNode.getFirstChild().getNodeType() == Node.TEXT_NODE
                    && currentNode.getFirstChild().getNodeValue() != null)
            {
                params.put(currentNode.getNodeName(), currentNode.getFirstChild().getNodeValue());
            }
            else
            {
                params.setBaseClass(currentNode.getNodeName());
                params.putAll(getParams(currentNode));
            }
        }

        return params;
    }
}