package es.uji.apps.ceo.model.domains;

public enum EstadoCertificado
{
    PENDIENTE_MODERAR("moderar"), PENDIENTE_REVISAR("revisar"), APROBADO("aprobado"), DENEGADO("denegado");

    private String descripcion;

    private EstadoCertificado(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public String getDescripcion()
    {
        return descripcion;
    }

    public static EstadoCertificado getEstadoByDescripcion(String descripcion) {
        for (EstadoCertificado tipo: EstadoCertificado.values()) {
            if (tipo.getDescripcion().equalsIgnoreCase(descripcion)) {
                return tipo;
            }
        }

        return null;
    }
}
