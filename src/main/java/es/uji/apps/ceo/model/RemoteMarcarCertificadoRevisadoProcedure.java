package es.uji.apps.ceo.model;

import es.uji.apps.ceo.GeneralCEOException;
import es.uji.apps.ceo.dao.ErroresAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Component;

import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

@Component
public class RemoteMarcarCertificadoRevisadoProcedure
{
    private MarcarComoRevisadoProcedure marcarComoRevisadoProcedure;
    private static DataSource dataSource;

    @Autowired
    public void setDataSource(DataSource dataSource)
    {
        RemoteMarcarCertificadoRevisadoProcedure.dataSource = dataSource;
    }

    public void init()
    {
        this.marcarComoRevisadoProcedure = new MarcarComoRevisadoProcedure(dataSource);
    }

    public Map<String, Object> execute(Long certificadoId, Long revisorPerId) throws GeneralCEOException {
        return marcarComoRevisadoProcedure.execute(certificadoId, revisorPerId);
    }

    private class MarcarComoRevisadoProcedure extends StoredProcedure
    {
        private static final String SQL = "ceo_certificados_api.marcar_como_revisado";
        private static final String ID_REVISOR = "p_per_id_revisor";
        private static final String ID_CERTIFICADO = "p_id_certificado";

        public MarcarComoRevisadoProcedure(DataSource dataSource)
        {
            setDataSource(dataSource);
            setSql(SQL);
            declareParameter(new SqlParameter(ID_REVISOR, Types.BIGINT));
            declareParameter(new SqlParameter(ID_CERTIFICADO, Types.BIGINT));
            compile();
        }

        public Map<String, Object> execute(Long certificadoId, Long revisorPerId) throws GeneralCEOException {
            Map<String, Object> results = new HashMap<String, Object>();
            try
            {
                Map<String, Object> inParams = new HashMap<String, Object>();
                inParams.put(ID_REVISOR, revisorPerId);
                inParams.put(ID_CERTIFICADO, certificadoId);
                results = execute(inParams);
            }
            catch (Exception e)
            {
                e.printStackTrace();
                SQLException sqlException = (SQLException) e.getCause();
                ErroresAPI erroresAPI = ErroresAPI.getErrorById(sqlException.getErrorCode());
                throw new GeneralCEOException(erroresAPI.getExceptionMessage());
            }
            return results;
        }
    }
}
