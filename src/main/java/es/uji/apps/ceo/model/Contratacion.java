package es.uji.apps.ceo.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.ceo.dao.ContratacionDAO;


@Entity
@Table(name = "CEO_EXT_CONTRATACIONES")
@SuppressWarnings("serial")
@Component
public class Contratacion implements Serializable
{
    @Id
    @Column(name = "PER_ID")
    private Long personaId;

    @Column(name = "NREG_EXP")
    private Long numeroExpediente;

    @Column(name = "N_PLAZA")
    private Long numeroPlaza;

    @Column(name = "ACT_ID")
    private String tipoExpediente;

    // bi-directional many-to-one association to UbicacionLogica
    @ManyToOne
    @JoinColumn(name = "UBICACION_ID")
    private UbicacionLogica ubicacionLogica;

    protected static ContratacionDAO contratacionDAO;

    @Autowired
    protected void setContratacionDAO(ContratacionDAO contratacionDAO)
    {
        Contratacion.contratacionDAO = contratacionDAO;
    }

    public Contratacion()
    {
    }

    public Long getPersonaId()
    {
        return personaId;
    }

    public void setPersonaId(Long personaId)
    {
        this.personaId = personaId;
    }

    public Long getNumeroExpediente()
    {
        return numeroExpediente;
    }

    public void setNumeroExpediente(Long numeroExpediente)
    {
        this.numeroExpediente = numeroExpediente;
    }

    public Long getNumeroPlaza()
    {
        return numeroPlaza;
    }

    public void setNumeroPlaza(Long numeroPlaza)
    {
        this.numeroPlaza = numeroPlaza;
    }

    public String getTipoExpediente()
    {
        return tipoExpediente;
    }

    public void setTipoExpediente(String tipoExpediente)
    {
        this.tipoExpediente = tipoExpediente;
    }

    public UbicacionLogica getUbicacionLogica()
    {
        return ubicacionLogica;
    }

    public void setUbicacionLogica(UbicacionLogica ubicacionLogica)
    {
        this.ubicacionLogica = ubicacionLogica;
    }
}