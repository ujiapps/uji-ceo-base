package es.uji.apps.ceo.model;

public enum VariablePlantilla
{
    DNI("DNI detinatari"), NOMBRE("Nom destinatari"), EMAIL("EMail destinatari");

    String descripcion;

    VariablePlantilla(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public String getDescripcion()
    {
        return descripcion;
    }

    public String getVariable()
    {
        return "${" + this.name().toLowerCase() + "}";
    }
}