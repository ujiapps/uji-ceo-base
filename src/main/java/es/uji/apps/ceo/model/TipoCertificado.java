package es.uji.apps.ceo.model;

import com.mysema.query.Tuple;
import com.sun.jersey.core.util.Base64;

import es.uji.apps.ceo.dao.CertificadoDAO;
import es.uji.apps.ceo.dao.TipoCertificadoDAO;
import es.uji.commons.rest.Role;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.dao.ApaDAO;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.PersistenceException;
import javax.persistence.Table;

@Entity
@Table(name = "CEO_TIPOS_CERTIFICADOS")
@SuppressWarnings("serial")
@Component
public class TipoCertificado implements Serializable
{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "DESCRIPCION_CA")
    private String descripcionCa;

    @Column(name = "DESCRIPCION_ES")
    private String descripcionEs;

    @Column(name = "DESCRIPCION_UK")
    private String descripcionUk;

    @Column(name = "EMISION_MANUAL")
    private Long emisionManual;

    @Lob()
    @Column(name = "PLANTILLA_CA")
    private String plantillaCa;

    @Lob()
    @Column(name = "PLANTILLA_ES")
    private String plantillaEs;

    @Lob()
    @Column(name = "PLANTILLA_UK")
    private String plantillaUk;

    @Lob()
    private byte[] logo;

    @Column(name = "LOGO_BASE64")
    private String logoBASE64;

    @Column(name = "LOGO_CONTENT_TYPE")
    private String contentType;

    @Column(name = "CARGO_PER_ID")
    private Long cargoPerId;

    @Column(name = "CARGO_CONECTOR_CA")
    private String cargoConectorCa;

    @Column(name = "CARGO_CONECTOR_ES")
    private String cargoConectorEs;

    @Column(name = "CARGO_CONECTOR_UK")
    private String cargoConectorUk;

    @Column(name = "CARGO_TEXTO_CA")
    private String cargoTextoCa;

    @Column(name = "CARGO_TEXTO_ES")
    private String cargoTextoEs;

    @Column(name = "CARGO_TEXTO_UK")
    private String cargoTextoUk;

    // bi-directional many-to-one association to Certificado
    @OneToMany(mappedBy = "tipoCertificado")
    private Set<Certificado> certificadosTiposCertificados;

    // bi-directional many-to-one association to Permiso
    @OneToMany(mappedBy = "tipoCertificado")
    private Set<Permiso> permisosTiposCertificados;

    protected static TipoCertificadoDAO tipoCertificadoDAO;
    protected static ApaDAO apaDAO;
    protected static CertificadoDAO certificadoDAO;

    @Autowired
    protected void setTipoCertificadoDAO(TipoCertificadoDAO tipoCertificadoDAO)
    {
        TipoCertificado.tipoCertificadoDAO = tipoCertificadoDAO;
    }

    @Autowired
    protected void setapaDAO(ApaDAO apaDAO)
    {
        TipoCertificado.apaDAO = apaDAO;
    }

    public TipoCertificado()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getDescripcionCa()
    {
        return this.descripcionCa;
    }

    public void setDescripcionCa(String descripcionCa)
    {
        this.descripcionCa = descripcionCa;
    }

    public String getDescripcionEs()
    {
        return this.descripcionEs;
    }

    public void setDescripcionEs(String descripcionEs)
    {
        this.descripcionEs = descripcionEs;
    }

    public String getDescripcionUk()
    {
        return this.descripcionUk;
    }

    public void setDescripcionUk(String descripcionUk)
    {
        this.descripcionUk = descripcionUk;
    }

    public String getPlantillaCa()
    {
        return this.plantillaCa;
    }

    public void setPlantillaCa(String plantillaCa)
    {
        this.plantillaCa = plantillaCa;
    }

    public String getPlantillaEs()
    {
        return this.plantillaEs;
    }

    public void setPlantillaEs(String plantillaEs)
    {
        this.plantillaEs = plantillaEs;
    }

    public Long getEmisionManual()
    {
        return emisionManual;
    }

    public void setEmisionManual(Long emisionManual)
    {
        this.emisionManual = emisionManual;
    }

    public String getPlantillaUk()
    {
        return this.plantillaUk;
    }

    public void setPlantillaUk(String plantillaUk)
    {
        this.plantillaUk = plantillaUk;
    }

    public Long getCargoPerId()
    {
        return cargoPerId;
    }

    public void setCargoPerId(Long cargoPerId)
    {
        this.cargoPerId = cargoPerId;
    }

    public String getCargoConectorCa()
    {
        return cargoConectorCa;
    }

    public void setCargoConectorCa(String cargoConectorCa)
    {
        this.cargoConectorCa = cargoConectorCa;
    }

    public String getCargoConectorEs()
    {
        return cargoConectorEs;
    }

    public void setCargoConectorEs(String cargoConectorEs)
    {
        this.cargoConectorEs = cargoConectorEs;
    }

    public String getCargoConectorUk()
    {
        return cargoConectorUk;
    }

    public void setCargoConectorUk(String cargoConectorUk)
    {
        this.cargoConectorUk = cargoConectorUk;
    }

    public String getCargoTextoCa()
    {
        return cargoTextoCa;
    }

    public void setCargoTextoCa(String cargoTextoCa)
    {
        this.cargoTextoCa = cargoTextoCa;
    }

    public String getCargoTextoEs()
    {
        return cargoTextoEs;
    }

    public void setCargoTextoEs(String cargoTextoEs)
    {
        this.cargoTextoEs = cargoTextoEs;
    }

    public String getCargoTextoUk()
    {
        return cargoTextoUk;
    }

    public void setCargoTextoUk(String cargoTextoUk)
    {
        this.cargoTextoUk = cargoTextoUk;
    }

    public Set<Certificado> getCertificadosTiposCertificados()
    {
        return this.certificadosTiposCertificados;
    }

    public void setCertificadosTiposCertificados(Set<Certificado> certificadosTiposCertificados)
    {
        this.certificadosTiposCertificados = certificadosTiposCertificados;
    }

    public Set<Permiso> getPermisosTiposCertificados()
    {
        return this.permisosTiposCertificados;
    }

    public void setPermisosTiposCertificados(Set<Permiso> permisosTiposCertificados)
    {
        this.permisosTiposCertificados = permisosTiposCertificados;
    }

    public static List<Tuple> getTiposCertificado(Long connectedUserId)
    {
        Long userIdAFiltrar = null;
        if (!apaDAO.hasPerfil("CEO", "ADMIN", connectedUserId))
        {
            userIdAFiltrar = connectedUserId;
        }

        return tipoCertificadoDAO.getTiposCertificado(userIdAFiltrar);

    }

    @Role({"ADMIN", "COORDINADOR"})
    public TipoCertificado insert(Long connectedUserId)
    {
        return tipoCertificadoDAO.insert(this);
    }

    @Role({"ADMIN", "COORDINADOR"})
    @Transactional
    public TipoCertificado update(Long connectedUserId)
    {
        return tipoCertificadoDAO.update(this);
    }

    @Role({"ADMIN", "COORDINADOR"})
    public static void delete(Long tipoCertificadoId, Long connectedUserId)
            throws RegistroConHijosException
    {
        try
        {
            tipoCertificadoDAO.delete(tipoCertificadoId);
        }
        catch (PersistenceException e)
        {
            throw new RegistroConHijosException();
        }

    }

    public static TipoCertificado getTipoCertificadoById(Long tipoCertificadoId)
            throws RegistroNoEncontradoException
    {
        List<TipoCertificado> listaTipoCertificado = new ArrayList<TipoCertificado>();

        listaTipoCertificado = tipoCertificadoDAO.get(TipoCertificado.class, tipoCertificadoId);

        if (listaTipoCertificado.size() == 1)
        {
            return listaTipoCertificado.get(0);
        }
        else
        {
            throw new RegistroNoEncontradoException();
        }
    }

    public static List<TipoCertificado> getTiposCertificadoModeracion(Long connectedUserId)
            throws UnauthorizedUserException
    {
        List<TipoCertificado> tiposCertificado;

        compruebaPerfilModerador(connectedUserId);

        if (apaDAO.hasPerfil("CEO", "ADMIN", connectedUserId))
        {
            tiposCertificado = tipoCertificadoDAO.getTiposCertificadoModeracion();
        }
        else
        {
            tiposCertificado = tipoCertificadoDAO.getTiposCertificadoModeracionConPermiso(connectedUserId);
        }

        return tiposCertificado;
    }

    public static List<TipoCertificado> getTiposCertificadoRevision(Long connectedUserId)
            throws UnauthorizedUserException
    {
        List<TipoCertificado> tiposCertificado;

        compruebaPerfilUsuario(connectedUserId);

        if (apaDAO.hasPerfil("CEO", "ADMIN", connectedUserId))
        {
            tiposCertificado = tipoCertificadoDAO.getTiposCertificadoRevision();
        }
        else
        {
            tiposCertificado = tipoCertificadoDAO.getTiposCertificadoRevisionConPermiso(connectedUserId);
        }

        return tiposCertificado;
    }

    private static void compruebaPerfilUsuario(Long connectedUserId)
            throws UnauthorizedUserException
    {
        if (!(apaDAO.hasPerfil("CEO", "ADMIN", connectedUserId)
                || apaDAO.hasPerfil("CEO", "USUARIO", connectedUserId)
                || apaDAO.hasPerfil("CEO", "MODERADOR", connectedUserId)))
        {
            throw new UnauthorizedUserException();
        }
    }

    private static void compruebaPerfilModerador(Long connectedUserId)
            throws UnauthorizedUserException
    {
        if (!(apaDAO.hasPerfil("CEO", "ADMIN", connectedUserId)
                || apaDAO.hasPerfil("CEO", "MODERADOR", connectedUserId)))
        {
            throw new UnauthorizedUserException();
        }
    }

    @Role({"ADMIN", "COORDINADOR"})
    public void updatePlantillas(String plantillaCa, String plantillaEs, String plantillaUk, Long connectedUserId)
    {

        if (plantillaCa != null && !plantillaCa.isEmpty())
        {
            this.setPlantillaCa(plantillaCa);
        }

        if (plantillaEs != null && !plantillaEs.isEmpty())
        {
            this.setPlantillaEs(plantillaEs);
        }

        if (plantillaUk != null && !plantillaUk.isEmpty())
        {
            this.setPlantillaUk(plantillaUk);
        }

        this.update(connectedUserId);
        return;
    }

    @Role({"ADMIN", "COORDINADOR"})
    public void updateResponsable(Long personaId, String cargoConectorCa, String cargoTextoCa, String cargoConectorEs, String cargoTextoEs, String cargoConectorUk, String cargoTextoUk, Long connectedUserId)
    {
        this.setCargoPerId(personaId);

        if (cargoConectorCa != null && !cargoConectorCa.isEmpty())
        {
            this.setCargoConectorCa(cargoConectorCa);
        }

        if (cargoConectorEs != null && !cargoConectorEs.isEmpty())
        {
            this.setCargoConectorEs(cargoConectorEs);
        }

        if (cargoConectorUk != null && !cargoConectorUk.isEmpty())
        {
            this.setCargoConectorUk(cargoConectorUk);
        }

        if (cargoTextoCa != null && !cargoTextoCa.isEmpty())
        {
            this.setCargoTextoCa(cargoTextoCa);
        }

        if (cargoTextoEs != null && !cargoTextoEs.isEmpty())
        {
            this.setCargoTextoEs(cargoTextoEs);
        }

        if (cargoTextoUk != null && !cargoTextoUk.isEmpty())
        {
            this.setCargoTextoUk(cargoTextoUk);
        }

        this.update(connectedUserId);
    }

    @Role({"ADMIN", "COORDINADOR"})
    public void updateLogo(byte[] imagen, String mimeType, Long connectedUserId)
    {
        this.setContentType(mimeType);
        this.setLogo(imagen);

        String base64 = new String(Base64.encode(imagen));
        this.setLogoBASE64(base64);
        this.update(connectedUserId);

        return;
    }

    @Role({"ADMIN", "COORDINADOR"})
    @Transactional
    public void updateTipoCertificado(String descripcionCa, String descripcionEs, String descripcionUk, Long emisionManual, Long connectedUserId)
    {
        this.setDescripcionCa(descripcionCa);
        this.setDescripcionEs(descripcionEs);
        this.setDescripcionUk(descripcionUk);
        this.setEmisionManual(emisionManual);
        this.update(connectedUserId);
    }

    public String getContentType()
    {
        return contentType;
    }

    public void setContentType(String contentType)
    {
        this.contentType = contentType;
    }

    public byte[] getLogo()
    {
        return logo;
    }

    public void setLogo(byte[] logo)
    {
        this.logo = logo;
    }

    @Role({"ADMIN", "COORDINADOR"})
    @Transactional
    public void borraImagen(Long connectedUserId)
    {
        this.setContentType("");
        this.setLogo(null);
        this.update(connectedUserId);
    }

    public List<String> extraeListaDeEmailsDeEmisores()
    {
        List<String> listaEmailDestinatarios = new ArrayList<String>();

        List<Long> listaEmisoresPerIds = TipoCertificadoPersona.getEmisoresPerIds(this.getId());

        for (Long perId : listaEmisoresPerIds)
        {
            Persona persona = Persona.getPersonaById(perId);
            listaEmailDestinatarios.add(persona.getMail());
        }

        return listaEmailDestinatarios;
    }

    public List<String> extraeEmailModerador()
    {
        List<String> listaEmaiDestinatario = new ArrayList<String>();


        return listaEmaiDestinatario;

    }

    public List<String> extraeListaDeEmailsDeModeradores()
    {
        List<String> listaEmailDestinatarios = new ArrayList<String>();

        List<Long> listaModeradoresPerIds = TipoCertificadoModeracion.getModeradoresPerIds(this.getId());

        for (Long perId : listaModeradoresPerIds)
        {
            Persona persona = Persona.getPersonaById(perId);
            listaEmailDestinatarios.add(persona.getMail());
        }

        return listaEmailDestinatarios;
    }

    public String getTextoConEtiquetasFop(String idiomaId)
    {
        String texto = getPlantillaCa();

        if ("ES".equals(idiomaId))
        {
            texto = getPlantillaEs();
        }

        if ("EN".equals(idiomaId))
        {
            texto = getPlantillaUk();
        }

        Map<String, String> mapaSustituciones = new HashMap<String, String>();

        mapaSustituciones.put("<b>", "<fo:inline font-weight=\"bold\">");
        mapaSustituciones.put("</b>", "</fo:inline>");
        mapaSustituciones.put("<i>", "<fo:inline font-style=\"italic\">");
        mapaSustituciones.put("</i>", "</fo:inline>");
        mapaSustituciones.put("<u>", "<fo:inline font-style=\"underscore\">");
        mapaSustituciones.put("</u>", "</fo:inline>");
        mapaSustituciones.put("\r\n", "<fo:block margin-bottom=\"0.25cm\"></fo:block>");
        mapaSustituciones.put("\n", "<fo:block margin-bottom=\"0.25cm\"></fo:block>");

        for (Map.Entry<String, String> entry : mapaSustituciones.entrySet())
        {
            String key = entry.getKey();
            String value = entry.getValue();

            texto = texto.replace(key, value);
        }

        return texto;
    }

    public String getLogoBASE64()
    {
        return logoBASE64;
    }

    public void setLogoBASE64(String logoBASE64)
    {
        this.logoBASE64 = logoBASE64;
    }

    public static List<Tuple> getTiposCertificadoEmisionManual(Long connectedUserId)
    {
        Long userIdAFiltrar = null;
        if (!apaDAO.hasPerfil("CEO", "ADMIN", connectedUserId))
        {
            userIdAFiltrar = connectedUserId;
        }

        return tipoCertificadoDAO.getTiposCertificadoEmisionManual(userIdAFiltrar);
    }
}
