package es.uji.apps.ceo.model;

import es.uji.apps.ceo.dao.TipoCertificadoPersonaDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;


@Entity
@Table(name="CEO_VW_TIPOCERTIFICADO_PER")
@SuppressWarnings("serial")
@Component
@IdClass(TipoCertificadoId.class)
public class TipoCertificadoPersona implements Serializable {

    @Id
	@Column(name="PER_ID")
	private Long perId;

    @Id
	@Column(name="TIPO_CERTIFICADO_ID")
	private Long tipoCertificadoId;

    protected static TipoCertificadoPersonaDAO tipoCertificadoPersonaDAO;

    @Autowired
    protected void setTipoCertificadoPersonaDAO(TipoCertificadoPersonaDAO tipoCertificadoPersonaDAO)
    {
        TipoCertificadoPersona.tipoCertificadoPersonaDAO = tipoCertificadoPersonaDAO;
    }

    public TipoCertificadoPersona() {
    }

	public Long getPerId() {
		return this.perId;
	}

	public void setPerId(Long perId) {
		this.perId = perId;
	}

	public Long getTipoCertificadoId() {
		return this.tipoCertificadoId;
	}

	public void setTipoCertificadoId(Long tipoCertificadoId) {
		this.tipoCertificadoId = tipoCertificadoId;
	}

    public static Boolean compruebaPermisoPersonaACertificado(Long connectedUserId,
            Long tipoCertificadoId)
    {
        List<TipoCertificadoPersona> listaTipoCertificadoPersona = tipoCertificadoPersonaDAO.getPermisoPorUsuarioYTipoCertificado(connectedUserId, tipoCertificadoId);
        
        return listaTipoCertificadoPersona.size() > 0;
    }

    public static List<Long> getEmisoresPerIds(Long tipoCertificadoId)
    {
        List<TipoCertificadoPersona> listaTipoCertificadoPersona = tipoCertificadoPersonaDAO.getListaTipoCertificadoPersonaByTipoCertificadoId(tipoCertificadoId);
        
        List<Long> listaPerIds = new ArrayList<Long>();
        
        for (TipoCertificadoPersona tipoCertificadoPersona: listaTipoCertificadoPersona) {
            listaPerIds.add(tipoCertificadoPersona.getPerId());
        }
        
        return listaPerIds;
    }
}