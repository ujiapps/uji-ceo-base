package es.uji.apps.ceo.model;

import java.io.Serializable;

public class TipoCertificadoId implements Serializable
{
    private static final long serialVersionUID = 1L;

    private Long perId;

    private Long tipoCertificadoId;

    public int hashCode()
    {
        return (int) (perId + tipoCertificadoId);
    }

    public Long getPerId()
    {
        return perId;
    }

    public void setPerId(Long perId)
    {
        this.perId = perId;
    }

    public Long getTipoCertificadoId()
    {
        return tipoCertificadoId;
    }

    public void setTipoCertificadoId(Long tipoCertificadoId)
    {
        this.tipoCertificadoId = tipoCertificadoId;
    }

    public boolean equals(Object o)
    {
        return ((o instanceof TipoCertificadoId) && perId == ((TipoCertificadoId) o).getPerId() && tipoCertificadoId == ((TipoCertificadoId) o)
                .getTipoCertificadoId());
    }

}
