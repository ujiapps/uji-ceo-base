package es.uji.apps.ceo.model;

import es.uji.apps.ceo.dao.TipoCertificadoModDAO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "CEO_VW_TIPO_CERTIFICADO_MOD")
@IdClass(TipoCertificadoId.class)
@SuppressWarnings("serial")
@Component
public class TipoCertificadoModeracion implements Serializable
{

    @Id
    @Column(name = "PER_ID")
    private Long perId;

    @Id
    @Column(name = "TIPO_CERTIFICADO_ID")
    private Long tipoCertificadoId;

    protected static TipoCertificadoModDAO tipoCertificadoModDAO;

    @Autowired
    protected void setTipoCertificadoModDAO(TipoCertificadoModDAO tipoCertificadoModDAO)
    {
        TipoCertificadoModeracion.tipoCertificadoModDAO = tipoCertificadoModDAO;
    }

    public TipoCertificadoModeracion()
    {
    }

    public Long getPerId()
    {
        return this.perId;
    }

    public void setPerId(Long perId)
    {
        this.perId = perId;
    }

    public Long getTipoCertificadoId()
    {
        return this.tipoCertificadoId;
    }

    public void setTipoCertificadoId(Long tipoCertificadoId)
    {
        this.tipoCertificadoId = tipoCertificadoId;
    }

    public static List<Long> getModeradoresPerIds(Long tipoCertificadoId)
    {
        List<TipoCertificadoModeracion> listaTipoCertificadoModeracion = tipoCertificadoModDAO.getListaTipoCertificadoModeracionByTipoCertificadoId(tipoCertificadoId);

        List<Long> listaPerIds = new ArrayList<Long>();

        for (TipoCertificadoModeracion tipoCertificadoModeracion : listaTipoCertificadoModeracion)
        {
            listaPerIds.add(tipoCertificadoModeracion.getPerId());
        }

        return listaPerIds;
    }

    public static Boolean compruebaPermisoModeradorACertificado(Long connectedUserId, Long tipoCertificadoId)
    {
        List<TipoCertificadoModeracion> listaTipoCertificadoModeracion = tipoCertificadoModDAO.getPermisoPorUsuarioYTipoCertificado(connectedUserId, tipoCertificadoId);

        return listaTipoCertificadoModeracion.size() > 0;
    }
}
