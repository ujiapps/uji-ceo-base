package es.uji.apps.ceo.model;

public enum TipoUsuarioEnum
{
    INTERNO, EXTERNO;
}