package es.uji.apps.ceo.model.domains;

public enum TipoPermiso
{
    MODERAR("moderar"), REVISAR("revisar");

    private String descripcion;

    private TipoPermiso(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public String getDescripcion()
    {
        return descripcion;
    }

    public static TipoPermiso getTipoPermisoByDescripcion(String descripcion) {
        for (TipoPermiso tipo: TipoPermiso.values()) {
            if (tipo.getDescripcion().equalsIgnoreCase(descripcion)) {
                return tipo;
            }
        }

        return null;
    }
}
