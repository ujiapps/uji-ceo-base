package es.uji.apps.ceo.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.ceo.dao.SecretarioGeneralDAO;
import es.uji.commons.db.BaseDAO;

@Entity
@Table(name = "CEO_SECGENERAL")
@SuppressWarnings("serial")
@Component
public class SecretarioGeneral implements Serializable
{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "NOMBRE")
    private String nombre;

    @Column(name = "CARGO_CA")
    private String cargoCA;

    @Column(name = "CARGO_ES")
    private String cargoES;

    @Column(name = "CARGO_EN")
    private String cargoEN;

    @Column(name = "PREFACIO_CA")
    private String prefacioCA;

    @Column(name = "PREFACIO_ES")
    private String prefacioES;

    @Column(name = "PREFACIO_EN")
    private String prefacioEN;

    @Column(name = "FIRMA_BASE64")
    private String firmaBASE64;

    @Column(name = "DNI")
    private String dni;


    protected static SecretarioGeneralDAO secretarioGeneralDAO;

    @Autowired
    protected void setSecretarioGeneralDAO(SecretarioGeneralDAO secretarioGeneralDAO)
    {
        SecretarioGeneral.secretarioGeneralDAO = secretarioGeneralDAO;
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCargoCA() {
        return cargoCA;
    }

    public void setCargoCA(String cargoCA) {
        this.cargoCA = cargoCA;
    }

    public String getCargoES() {
        return cargoES;
    }

    public void setCargoES(String cargoES) {
        this.cargoES = cargoES;
    }

    public String getCargoEN() {
        return cargoEN;
    }

    public void setCargoEN(String cargoEN) {
        this.cargoEN = cargoEN;
    }

    public String getPrefacioCA() {
        return prefacioCA;
    }

    public void setPrefacioCA(String prefacioCA) {
        this.prefacioCA = prefacioCA;
    }

    public String getFirmaBASE64() {
        return firmaBASE64;
    }

    public void setFirmaBASE64(String firmaBASE64) {
        this.firmaBASE64 = firmaBASE64;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getPrefacioES() {
        return prefacioES;
    }

    public void setPrefacioES(String prefacioES) {
        this.prefacioES = prefacioES;
    }

    public String getPrefacioEN() {
        return prefacioEN;
    }

    public void setPrefacioEN(String prefacioEN) {
        this.prefacioEN = prefacioEN;
    }

    public static SecretarioGeneral getSecretarioGeneralById(Long secretarioGeneralId) {
        return secretarioGeneralDAO.get(SecretarioGeneral.class, secretarioGeneralId).get(0);
    }
}
