package es.uji.apps.ceo.model;

import es.uji.apps.ceo.GeneralCEOException;
import es.uji.apps.ceo.dao.ErroresAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Component;

import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

@Component
public class RemoteMarcarCertificadoModeradoProcedure
{
    private MarcarComoModeradoProcedure marcarComoModeradoProcedure;
    private static DataSource dataSource;

    @Autowired
    public void setDataSource(DataSource dataSource)
    {
        RemoteMarcarCertificadoModeradoProcedure.dataSource = dataSource;
    }

    public void init()
    {
        this.marcarComoModeradoProcedure = new MarcarComoModeradoProcedure(dataSource);
    }

    public Map<String, Object> execute(Long certificadoId, Long revisorPerId, Boolean aprobado,
            String mensajeDenegacion) throws GeneralCEOException
    {
        return marcarComoModeradoProcedure.execute(certificadoId, revisorPerId, aprobado,
                mensajeDenegacion);
    }

    private class MarcarComoModeradoProcedure extends StoredProcedure
    {
        private static final String SQL = "ceo_certificados_api.marcar_como_moderado";
        private static final String ID_MODERADOR = "p_per_id_moderador";
        private static final String ID_CERTIFICADO = "p_id_certificado";
        private static final String APROBADO = "p_aprobado";
        private static final String MENSAJE_DENEGACION = "p_mensaje_denegacion";

        public MarcarComoModeradoProcedure(DataSource dataSource)
        {
            setDataSource(dataSource);
            setSql(SQL);
            declareParameter(new SqlParameter(ID_MODERADOR, Types.BIGINT));
            declareParameter(new SqlParameter(ID_CERTIFICADO, Types.BIGINT));
            declareParameter(new SqlParameter(APROBADO, Types.BIGINT));
            declareParameter(new SqlParameter(MENSAJE_DENEGACION, Types.VARCHAR));
            compile();
        }

        public Map<String, Object> execute(Long certificadoId, Long revisorPerId, Boolean aprobado,
                String mensajeDenegacion) throws GeneralCEOException
        {
            Map<String, Object> results = new HashMap<String, Object>();
            try
            {
                Map<String, Object> inParams = new HashMap<String, Object>();
                inParams.put(ID_MODERADOR, revisorPerId);
                inParams.put(ID_CERTIFICADO, certificadoId);
                inParams.put(APROBADO, aprobado.equals(true) ? 1 : 0);
                inParams.put(MENSAJE_DENEGACION, mensajeDenegacion);
                results = execute(inParams);
            }
            catch (Exception e)
            {
                e.printStackTrace();
                SQLException sqlException = (SQLException) e.getCause();
                ErroresAPI erroresAPI = ErroresAPI.getErrorById(sqlException.getErrorCode());
                throw new GeneralCEOException("Error en la moderació del certificat");
            }
            return results;
        }
    }
}
