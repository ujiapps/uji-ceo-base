package es.uji.apps.ceo.model;

import java.util.List;

import javax.ws.rs.core.MediaType;

import es.uji.commons.messaging.client.MessageNotSentException;
import es.uji.commons.messaging.client.MessagingClient;
import es.uji.commons.messaging.client.model.MailMessage;

public class Email
{
    public static void enviaEmail(List<String> listaDestinatarios, String titulo, String mensaje)
    {
        try
        {
            for (String destinatario : listaDestinatarios)
            {
                MailMessage email = new MailMessage("CEO");
                email.setTitle(titulo);
                email.setSender("noreply@uji.es");
                email.setContent(mensaje);
                email.setContentType(MediaType.TEXT_PLAIN);
                email.addToRecipient(destinatario.trim());
                MessagingClient emailClient = new MessagingClient();
                emailClient.send(email);
            }
        }
        catch (MessageNotSentException e)
        {
        }
    }
}
