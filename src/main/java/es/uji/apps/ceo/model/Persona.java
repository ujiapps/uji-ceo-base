package es.uji.apps.ceo.model;

import es.uji.apps.ceo.dao.PersonaDAO;
import es.uji.commons.sso.dao.ApaDAO;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "CEO_EXT_PER_PERSONAS")
@SuppressWarnings("serial")
@Component
public class Persona implements Serializable
{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String identificacion;

    private String mail;

    private String nombre;

    private String apellido1;

    private String apellido2;

    @Column(name = "NOMBRE_COMPLETO")
    private String nombreCompleto;

    @Column(name = "NOMBRE_BUSQUEDA")
    private String nombreBusqueda;

    // bi-directional many-to-one association to Certificado
    @OneToMany(mappedBy = "usuario")
    private Set<Certificado> certificadosUsuarios;

    // bi-directional many-to-one association to Permiso
    @OneToMany(mappedBy = "persona")
    private Set<Permiso> permisosUsuarios;

    protected static PersonaDAO personaDAO;

    @Autowired
    protected void setPersonaDAO(PersonaDAO personaDAO)
    {
        Persona.personaDAO = personaDAO;
    }

    protected static ApaDAO apaDAO;

    @Autowired
    protected void setApaDAO(ApaDAO apaDAO)
    {
        Persona.apaDAO = apaDAO;
    }

    public Persona()
    {
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public String getNombreBusqueda() {
        return nombreBusqueda;
    }

    public void setNombreBusqueda(String nombreBusqueda) {
        this.nombreBusqueda = nombreBusqueda;
    }

    public String getApellido2()
    {
        return apellido2;
    }

    public void setApellido2(String apellido2)
    {
        this.apellido2 = apellido2;
    }

    public String getApellido1()
    {
        return apellido1;
    }

    public void setApellido1(String apellido1)
    {
        this.apellido1 = apellido1;
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getIdentificacion()
    {
        return this.identificacion;
    }

    public void setIdentificacion(String identificacion)
    {
        this.identificacion = identificacion;
    }

    public String getMail()
    {
        return this.mail;
    }

    public void setMail(String mail)
    {
        this.mail = mail;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Set<Certificado> getCertificadosUsuarios()
    {
        return this.certificadosUsuarios;
    }

    public void setCertificadosUsuarios(Set<Certificado> certificadosUsuarios)
    {
        this.certificadosUsuarios = certificadosUsuarios;
    }

    public Set<Permiso> getPermisosUsuarios()
    {
        return this.permisosUsuarios;
    }

    public void setPermisosUsuarios(Set<Permiso> permisosUsuarios)
    {
        this.permisosUsuarios = permisosUsuarios;
    }

    public static Persona getPersonaById(Long personaId)
    {
        return personaDAO.get(Persona.class, personaId).get(0);
    }

    public static Persona getPersonaByDNI(String dni, Long connectedUserId)
            throws UnauthorizedUserException
    {
        compruebaPerfilUsuario(connectedUserId);

        List<Persona> personas = personaDAO.getPesonaByDNI(dni);

        if (personas.size() > 0)
        {
            return personas.get(0);
        }
        else
        {
            return new Persona();
        }
    }

    private static void compruebaPerfilUsuario(Long connectedUserId)
            throws UnauthorizedUserException
    {

        if (!(apaDAO.hasPerfil("CEO", "ADMIN", connectedUserId)
                || apaDAO.hasPerfil("CEO", "USUARIO", connectedUserId) || apaDAO.hasPerfil("CEO",
                "MODERADOR", connectedUserId)))
        {
            throw new UnauthorizedUserException();
        }
    }

}
