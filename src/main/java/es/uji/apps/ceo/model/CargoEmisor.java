package es.uji.apps.ceo.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class CargoEmisor
{
    private String cargo;
    private String nombre;

    @Autowired
    public CargoEmisor(@Value("${uji.firma.cargo}") String cargo,
            @Value("${uji.firma.nombre}") String nombre)
    {
        this.cargo = cargo;
        this.nombre = nombre;
    }
}