package es.uji.apps.ceo.model;

import com.mysema.query.Tuple;
import es.uji.apps.ceo.dao.PermisoDAO;
import es.uji.apps.ceo.dao.TipoCertificadoDAO;
import es.uji.apps.ceo.dao.TipoCertificadoPersonaDAO;
import es.uji.commons.rest.Role;
import es.uji.commons.sso.dao.ApaDAO;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "CEO_PERM_DOC")
@SuppressWarnings("serial")
@Component
public class Permiso implements Serializable
{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    // bi-directional many-to-one association to Persona
    @ManyToOne
    @JoinColumn(name = "PER_ID")
    private Persona persona;

    // bi-directional many-to-one association to UbicacionLogica
    @ManyToOne
    @JoinColumn(name = "ULOGICA_ID")
    private UbicacionLogica ubicacionLogica;

    // bi-directional many-to-one association to TipoCertificado
    @ManyToOne
    @JoinColumn(name = "TIPO_CERTIFICADO_ID")
    private TipoCertificado tipoCertificado;

    protected static TipoCertificadoPersonaDAO tipoCertificadoPersonaDAO;
    protected static PermisoDAO permisoDAO;
    protected static ApaDAO apaDAO;

    @Autowired
    protected void setapaDAO(ApaDAO apaDAO)
    {
        Permiso.apaDAO = apaDAO;
    }

    @Autowired
    protected void setPermisoDAO(PermisoDAO permisoDAO)
    {
        Permiso.permisoDAO = permisoDAO;
    }

    @Autowired
    protected void setTipoCertificadoPersonaDAO(TipoCertificadoPersonaDAO tipoCertificadoPersonaDAO)
    {
        Permiso.tipoCertificadoPersonaDAO = tipoCertificadoPersonaDAO;
    }

    public Permiso()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Persona getPersona()
    {
        return this.persona;
    }

    public void setPersona(Persona persona)
    {
        this.persona = persona;
    }

    public UbicacionLogica getUbicacionLogica()
    {
        return this.ubicacionLogica;
    }

    public void setUbicacionLogica(UbicacionLogica ubicacionLogica)
    {
        this.ubicacionLogica = ubicacionLogica;
    }

    public TipoCertificado getTipoCertificado()
    {
        return this.tipoCertificado;
    }

    public void setTipoCertificado(TipoCertificado tipoCertificado)
    {
        this.tipoCertificado = tipoCertificado;
    }

    public static List<Persona> getPersonasConPermisoPorTipoCertificadoId(Long tipoCertificadoId,
            Long connectedUserId) throws UnauthorizedUserException
    {

        compruebaPerfilUsuario(connectedUserId);

        if (usuarioTienePermisosAlTipoCertificado(tipoCertificadoId, connectedUserId))
        {
            return permisoDAO.getPersonasConPermisoPorTipoCertificadoId(tipoCertificadoId);
        }
        else
        {
            throw new UnauthorizedUserException();
        }
    }

    private static boolean usuarioTienePermisosAlTipoCertificado(Long tipoCertificadoId,
            Long connectedUserId) throws UnauthorizedUserException
    {
        if (!apaDAO.hasPerfil("CEO", "ADMIN", connectedUserId)
                && !apaDAO.hasPerfil("CEO", "COORDINADOR", connectedUserId))
        {
            return tipoCertificadoPersonaDAO.getByUserIdAndTipoCertificadoId(connectedUserId, tipoCertificadoId) != null;
        }

        return true;
    }

    @Role({ "ADMIN", "COORDINADOR" })
    public static List<UbicacionLogica> getUbicacionesLogicasConPermisoPorTipoCertificadoId(
            Long tipoCertificadoId, Long connectedUserId) throws UnauthorizedUserException
    {
        compruebaPerfilUsuario(connectedUserId);

        if (usuarioTienePermisosAlTipoCertificado(tipoCertificadoId, connectedUserId))
        {
            return permisoDAO.getUbicacionesConPermisoPorTipoCertificadoId(tipoCertificadoId);
        }
        else
        {
            throw new UnauthorizedUserException();
        }
    }

    public void insert(Long connectedUserId) throws UnauthorizedUserException
    {
        compruebaPerfilUsuario(connectedUserId);

        permisoDAO.insert(this);
    }

    @Role({ "ADMIN", "COORDINADOR" })
    public static void deletePersonaConPermisoPorTipoCertificadoId(Long personaId,
            Long tipoCertificadoId, Long connectedUserId) throws UnauthorizedUserException
    {
        compruebaPerfilUsuario(connectedUserId);

        permisoDAO.deletePersonaConPermisoPorTipoCertificadoId(personaId, tipoCertificadoId);
    }

    @Role({ "ADMIN", "COORDINADOR" })
    public static void deleteUbicacionLogicaConPermisoPorTipoCertificadoId(Long ubicacionLogicaId,
            Long tipoCertificadoId, Long connectedUserId) throws UnauthorizedUserException
    {
        compruebaPerfilUsuario(connectedUserId);

        permisoDAO.deleteUbicacionLogicaConPermisoPorTipoCertificadoId(ubicacionLogicaId,
                tipoCertificadoId);
    }

    public static boolean personaTienePermisoATipoCertificado(Long personaId,
            Long tipoCertificadoId)
    {
        List<Persona> listaPersonasConPermiso = permisoDAO
                .getPersonasConPermisoPorTipoCertificadoId(tipoCertificadoId);

        for (Persona p : listaPersonasConPermiso)
        {
            if (personaId.equals(p.getId()))
            {
                return true;
            }
        }
        return false;
    }

    public static boolean ubicacionLogicaTienePermisoATipoCertificado(Long ubicacionLogicaId,
            Long tipoCertificadoId)
    {
        List<UbicacionLogica> listaUbicacionesLogicasConPermiso = permisoDAO
                .getUbicacionesConPermisoPorTipoCertificadoId(tipoCertificadoId);

        for (UbicacionLogica u : listaUbicacionesLogicasConPermiso)
        {
            if (ubicacionLogicaId.equals(u.getId()))
            {
                return true;
            }
        }
        return false;
    }

    private static void compruebaPerfilUsuario(Long connectedUserId)
            throws UnauthorizedUserException
    {
        if (!(apaDAO.hasPerfil("CEO", "ADMIN", connectedUserId)
                || apaDAO.hasPerfil("CEO", "COORDINADOR", connectedUserId)
                || apaDAO.hasPerfil("CEO", "USUARIO", connectedUserId)
                || apaDAO.hasPerfil("CEO", "MODERADOR", connectedUserId)))
        {
            throw new UnauthorizedUserException();
        }
    }

    @Role({ "ADMIN", "COORDINADOR" })
    public void insertTipoCertificado(Long connectedUserId) throws UnauthorizedUserException
    {
        this.insert(connectedUserId);
    }

    @Role({ "ADMIN", "COORDINADOR" })
    public void insertUbicacionLogica(Long connectedUserId) throws UnauthorizedUserException
    {
        this.insert(connectedUserId);
    }

}