package es.uji.apps.ceo.model;

import es.uji.apps.ceo.dao.FirmaNotificacionDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "CEO_VW_FIRMAS_NOTIFICACIONES")
@SuppressWarnings("serial")
@Component
public class FirmaNotificacion implements Serializable
{

    @Lob()
    private String contenido;

    @Lob()
    @Column(name = "CONTENIDO_NOTIFICACION")
    private String contenidoNotificacion;

    private String estado;

    @Temporal(TemporalType.DATE)
    @Column(name = "F_ENTRADA_NOTIFICACION")
    private Date fechaEntradaNotificacion;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_ACEPTA_RECHAZA")
    private Date fechaAceptaRechaza;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_EMISION")
    private Date fechaEmision;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_FIRMA")
    private Date fechaFirma;

    @Id
    private Long id;

    @Column(name = "MOTIVO_RECHAZO")
    private String motivoRechazo;

    @Column(name = "NOMBRE_SOLICITANTE")
    private String nombreSolicitante;

    @Column(name = "PER_ID_EMISOR")
    private Long perIdEmisor;

    @Column(name = "PER_ID_SOLICITANTE")
    private Long perIdSolicitante;

    @Column(name = "REF_INTERNA")
    private String referenciaInterna;

    private String referencia;

    @Lob()
    private String xml;

    protected static FirmaNotificacionDAO firmaNotificacionDAO;

    @Autowired
    protected void setFirmaNotificacionDAO(FirmaNotificacionDAO firmaNotificacionDAO)
    {
        FirmaNotificacion.firmaNotificacionDAO = firmaNotificacionDAO;
    }

    public FirmaNotificacion()
    {
    }

    public String getContenido()
    {
        return this.contenido;
    }

    public void setContenido(String contenido)
    {
        this.contenido = contenido;
    }

    public String getContenidoNotificacion()
    {
        return this.contenidoNotificacion;
    }

    public void setContenidoNotificacion(String contenidoNotificacion)
    {
        this.contenidoNotificacion = contenidoNotificacion;
    }

    public String getEstado()
    {
        return this.estado;
    }

    public void setEstado(String estado)
    {
        this.estado = estado;
    }

    public Date getFechaEntradaNotificacion()
    {
        return this.fechaEntradaNotificacion;
    }

    public void setFechaEntradaNotificacion(Date fechaEntradaNotificacion)
    {
        this.fechaEntradaNotificacion = fechaEntradaNotificacion;
    }

    public Date getFechaAceptaRechaza()
    {
        return this.fechaAceptaRechaza;
    }

    public void setFechaAceptaRechaza(Date fechaAceptaRechaza)
    {
        this.fechaAceptaRechaza = fechaAceptaRechaza;
    }

    public Date getFechaEmision()
    {
        return this.fechaEmision;
    }

    public void setFechaEmision(Date fechaEmision)
    {
        this.fechaEmision = fechaEmision;
    }

    public Date getFechaFirma()
    {
        return this.fechaFirma;
    }

    public void setFechaFirma(Date fechaFirma)
    {
        this.fechaFirma = fechaFirma;
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getMotivoRechazo()
    {
        return this.motivoRechazo;
    }

    public void setMotivoRechazo(String motivoRechazo)
    {
        this.motivoRechazo = motivoRechazo;
    }

    public String getNombreSolicitante()
    {
        return this.nombreSolicitante;
    }

    public void setNombreSolicitante(String nombreSolicitante)
    {
        this.nombreSolicitante = nombreSolicitante;
    }

    public Long getPerIdEmisor()
    {
        return this.perIdEmisor;
    }

    public void setPerIdEmisor(Long perIdEmisor)
    {
        this.perIdEmisor = perIdEmisor;
    }

    public Long getPerIdSolicitante()
    {
        return this.perIdSolicitante;
    }

    public void setPerIdSolicitante(Long perIdSolicitante)
    {
        this.perIdSolicitante = perIdSolicitante;
    }

    public String getReferenciaInterna()
    {
        return this.referenciaInterna;
    }

    public void setReferenciaInterna(String referenciaInterna)
    {
        this.referenciaInterna = referenciaInterna;
    }

    public String getReferencia()
    {
        return this.referencia;
    }

    public void setReferencia(String referencia)
    {
        this.referencia = referencia;
    }

    public String getXml()
    {
        return this.xml;
    }

    public void setXml(String xml)
    {
        this.xml = xml;
    }

    public static List<FirmaNotificacion> getFirmaNotificacionesByCertificadoId(String certificadoId)
    {
        return firmaNotificacionDAO.getFirmaNotificacionesByCertificadoId(certificadoId);
    }

    public static List<FirmaNotificacion> getFirmaNotificacionSinFechaEntrada()
    {
        return firmaNotificacionDAO.getFirmaNotificacionSinFechaEntrada();
    }

}