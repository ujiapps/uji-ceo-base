package es.uji.apps.ceo.model;

import org.springframework.stereotype.Component;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Component
@Entity
@Table(name = "WWW_VW_SESIONES_ACTIVAS", schema="UJI_APA")
@SuppressWarnings("serial")
public class SessionActivaEujier implements Serializable
{
    @Id
    private String id;

    @Column(name = "per_id")
    private Long personaId;

    @Column(name = "per_cuenta")
    private String personaCuenta;

    public SessionActivaEujier()
    {
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public Long getPersonaId()
    {
        return personaId;
    }

    public void setPersonaId(Long personaId)
    {
        this.personaId = personaId;
    }

    public String getPersonaCuenta()
    {
        return personaCuenta;
    }

    public void setPersonaCuenta(String personaCuenta)
    {
        this.personaCuenta = personaCuenta;
    }
}