package es.uji.apps.ceo.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.ceo.dao.PersonaDAO;
import es.uji.commons.sso.dao.ApaDAO;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;

@Entity
@Table(name = "POD_CERT_AV_CERTIFICADOS", schema="GRA_POD")
@SuppressWarnings("serial")
@Component
public class CertificadoAulaVirtual implements Serializable
{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "FECHA")
    private Date fecha;

    @Column(name = "DATOS")
    private String datos;

    @Column(name = "PER_ID_SOLICITANTE")
    private Long perIdSolicitante;

    @Column(name = "PER_ID_CERTIFICADO")
    private Long perIdCertificado;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getDatos() {
        return datos;
    }

    public void setDatos(String datos) {
        this.datos = datos;
    }

    public Long getPerIdSolicitante() {
        return perIdSolicitante;
    }

    public void setPerIdSolicitante(Long perIdSolicitante) {
        this.perIdSolicitante = perIdSolicitante;
    }

    public Long getPerIdCertificado() {
        return perIdCertificado;
    }

    public void setPerIdCertificado(Long perIdCertificado) {
        this.perIdCertificado = perIdCertificado;
    }
}
