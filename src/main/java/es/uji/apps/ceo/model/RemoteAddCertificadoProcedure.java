package es.uji.apps.ceo.model;

import es.uji.apps.ceo.GeneralCEOException;
import es.uji.apps.ceo.dao.ErroresAPI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

@Component
public class RemoteAddCertificadoProcedure
{
    private AddCertificadoProcedure addCertificadoProcedure;
    private static DataSource dataSource;

    @Autowired
    public void setDataSource(DataSource dataSource)
    {
        RemoteAddCertificadoProcedure.dataSource = dataSource;
    }

    public void init()
    {
        this.addCertificadoProcedure = new AddCertificadoProcedure(dataSource);
    }

    public Long execute(Certificado certificado) throws GeneralCEOException, IOException
    {
        return addCertificadoProcedure.execute(certificado);
    }

    private class AddCertificadoProcedure extends StoredProcedure
    {
        private static final String SQL = "ceo_certificados_api.add_certificado";
        private static final String TIPO_CERTIFICADO_ID = "p_tipo_certificado_id";
        private static final String ID_USUARIO = "p_per_id_emisor";
        private static final String FECHA_CERTIFICADO = "p_fecha_certificado";
        private static final String TEXTO_CERTIFICADO_CA = "p_texto_certificado_ca";
        private static final String ID_DESTINATARIO = "p_per_id_destinatario";
        private static final String TEXTO_CERTIFICADO_ES = "p_texto_certificado_es";
        private static final String TEXTO_CERTIFICADO_UK = "p_texto_certificado_uk";
        private static final String DESCRIPCION_CURSO_CA = "p_descripcion_curso_ca";
        private static final String DESCRIPCION_CURSO_ES = "p_descripcion_curso_es";
        private static final String DESCRIPCION_CURSO_UK = "p_descripcion_curso_uk";
        private static final String MAIL_DESTINATARIO = "p_mail_destinatario";
        private static final String DNI_DESTINATARIO = "p_dni_destinatario";
        private static final String NOMBRE_DESTINATARIO = "p_nombre_destinatario";
        private static final String TEXTO_ANVERSO_CA = "p_texto_anverso_ca";
        private static final String TEXTO_ANVERSO_ES = "p_texto_anverso_es";
        private static final String TEXTO_ANVERSO_UK = "p_texto_anverso_uk";
        private static final String FECHA_INICIO_CURSO = "p_fecha_inicio_curso";
        private static final String IMAGEN_CABECERA = "p_imagen_cabecera";
        private static final String IMAGEN_FIRMA = "p_imagen_firma";

        public AddCertificadoProcedure(DataSource dataSource)
        {
            super(dataSource, SQL);
            declareParameter(new SqlOutParameter("certificadoId", Types.BIGINT));
            declareParameter(new SqlParameter(TIPO_CERTIFICADO_ID, Types.BIGINT));
            declareParameter(new SqlParameter(ID_USUARIO, Types.BIGINT));
            declareParameter(new SqlParameter(FECHA_CERTIFICADO, Types.DATE));
            declareParameter(new SqlParameter(TEXTO_CERTIFICADO_CA, Types.VARCHAR));
            declareParameter(new SqlParameter(ID_DESTINATARIO, Types.BIGINT));
            declareParameter(new SqlParameter(TEXTO_CERTIFICADO_ES, Types.VARCHAR));
            declareParameter(new SqlParameter(TEXTO_CERTIFICADO_UK, Types.VARCHAR));
            declareParameter(new SqlParameter(DESCRIPCION_CURSO_CA, Types.VARCHAR));
            declareParameter(new SqlParameter(DESCRIPCION_CURSO_ES, Types.VARCHAR));
            declareParameter(new SqlParameter(DESCRIPCION_CURSO_UK, Types.VARCHAR));
            declareParameter(new SqlParameter(MAIL_DESTINATARIO, Types.VARCHAR));
            declareParameter(new SqlParameter(DNI_DESTINATARIO, Types.VARCHAR));
            declareParameter(new SqlParameter(NOMBRE_DESTINATARIO, Types.VARCHAR));
            declareParameter(new SqlParameter(TEXTO_ANVERSO_CA, Types.VARCHAR));
            declareParameter(new SqlParameter(TEXTO_ANVERSO_ES, Types.VARCHAR));
            declareParameter(new SqlParameter(TEXTO_ANVERSO_UK, Types.VARCHAR));
            declareParameter(new SqlParameter(FECHA_INICIO_CURSO, Types.DATE));
            setFunction(true);
            compile();
        }

        public Long execute(Certificado certificado) throws GeneralCEOException, IOException
        {
            Map<String, Object> inParams = new HashMap<String, Object>();
            inParams.put(TIPO_CERTIFICADO_ID, certificado.getTipoCertificado().getId());
            inParams.put(ID_USUARIO, certificado.getUsuario().getId());
            inParams.put(FECHA_CERTIFICADO, certificado.getFechaCertificado());
            inParams.put(TEXTO_CERTIFICADO_CA, certificado.getTextoCa());

            if (certificado.getDestinatarioId() != null)
            {
                inParams.put(ID_DESTINATARIO, certificado.getDestinatarioId());
            }
            else
            {
                inParams.put(ID_DESTINATARIO, null);
            }

            inParams.put(TEXTO_CERTIFICADO_ES, certificado.getTextoEs());
            inParams.put(TEXTO_CERTIFICADO_UK, certificado.getTextoUk());
            inParams.put(DESCRIPCION_CURSO_CA, certificado.getDescripcionCursoCA());
            inParams.put(DESCRIPCION_CURSO_ES, certificado.getDescripcionCursoES());
            inParams.put(DESCRIPCION_CURSO_UK, certificado.getDescripcionCursoUK());
            inParams.put(MAIL_DESTINATARIO, certificado.getMailDestinatario());
            inParams.put(DNI_DESTINATARIO, certificado.getDniDestinatario());
            inParams.put(NOMBRE_DESTINATARIO, certificado.getNombreDestinatario());
            inParams.put(TEXTO_ANVERSO_CA, certificado.getTextoAnversoCA());
            inParams.put(TEXTO_ANVERSO_ES, certificado.getTextoAnversoES());
            inParams.put(TEXTO_ANVERSO_UK, certificado.getTextoAnversoUK());
            inParams.put(FECHA_INICIO_CURSO, certificado.getFechaInicioCurso());

            Map<String, Object> results = new HashMap<String, Object>();
            try
            {
                results = execute(inParams);
            }
            catch (Exception e)
            {
                e.printStackTrace();
                SQLException sqlException = (SQLException) e.getCause();
                ErroresAPI erroresAPI = ErroresAPI.getErrorById(sqlException.getErrorCode());
                throw new GeneralCEOException(erroresAPI.getExceptionMessage());
            }

            if (!results.isEmpty())
            {
                return (Long) results.get("certificadoId");
            }
            else
            {
                return null;
            }
        }
    }
}
