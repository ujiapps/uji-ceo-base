package es.uji.apps.ceo.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import es.uji.apps.ceo.dao.UbicacionLogicaDAODatabaseImpl;
import es.uji.commons.rest.Role;


@Entity
@Table(name = "CEO_EXT_ULOGICAS")
@SuppressWarnings("serial")
@Component
public class UbicacionLogica implements Serializable
{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String mail;

    private String nombre;

    //bi-directional many-to-one association to SeuPermDoc
    @OneToMany(mappedBy = "ubicacionLogica")
    private Set<Permiso> permisosUbicacionesLogicas;

    //bi-directional many-to-one association to SeuPermDoc
    @OneToMany(mappedBy = "ubicacionLogica")
    private Set<Contratacion> contrataciones;

    protected static UbicacionLogicaDAODatabaseImpl ubicacionLogicaDAO;

    @Autowired
    protected void setUbicacionLogicaDAO(UbicacionLogicaDAODatabaseImpl ubicacionLogicaDAO)
    {
        UbicacionLogica.ubicacionLogicaDAO = ubicacionLogicaDAO;
    }

    public UbicacionLogica()
    {
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getMail()
    {
        return this.mail;
    }

    public void setMail(String mail)
    {
        this.mail = mail;
    }

    public String getNombre()
    {
        return this.nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public Set<Permiso> getPermisosUbicacionesLogicas()
    {
        return this.permisosUbicacionesLogicas;
    }

    public void setPermisosUbicacionesLogicas(Set<Permiso> permisosUbicacionesLogicas)
    {
        this.permisosUbicacionesLogicas = permisosUbicacionesLogicas;
    }

    public Set<Contratacion> getContrataciones()
    {
        return contrataciones;
    }

    public void setContrataciones(Set<Contratacion> contrataciones)
    {
        this.contrataciones = contrataciones;
    }

    @Role({"ADMIN", "COORDINADOR"})
    public static UbicacionLogica getUbicacionLogicaById(long ubicacionLogicaId, Long connectedUserId)
    {
        return ubicacionLogicaDAO.get(UbicacionLogica.class, ubicacionLogicaId).get(0);
    }

}