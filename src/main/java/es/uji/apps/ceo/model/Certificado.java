package es.uji.apps.ceo.model;

import java.io.IOException;
import java.io.Serializable;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.Tuple;
import com.mysema.query.annotations.QueryProjection;

import es.uji.apps.ceo.ErrorInsercionCertificadoException;
import es.uji.apps.ceo.GeneralCEOException;
import es.uji.apps.ceo.dao.CertificadoDAO;
import es.uji.apps.ceo.dao.PersonaDAO;
import es.uji.apps.ceo.dao.UbicacionLogicaDAO;
import es.uji.apps.ceo.model.domains.EstadoCertificado;
import es.uji.apps.ceo.model.domains.TipoPermiso;
import es.uji.commons.rest.Role;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.dao.ApaDAO;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;

@Entity
@Table(name = "CEO_CERTIFICADOS")
@SuppressWarnings("serial")
@Component
public class Certificado implements Serializable, Comparable<Certificado>
{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "DNI_DESTINATARIO")
    private String dniDestinatario;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_CERTIFICADO")
    private Date fechaCertificado;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_CREACION")
    private Date fechaCreacion;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_INICIO_CURSO")
    private Date fechaInicioCurso;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_RECOGIDA")
    private Date fechaRecogida;

    @Column(name = "MAIL_DESTINATARIO")
    private String mailDestinatario;

    @Column(name = "NOMBRE_DESTINATARIO")
    private String nombreDestinatario;

    @Lob()
    @Column(name = "TEXTO_ANVERSO_CA")
    private String textoAnversoCA;

    @Lob()
    @Column(name = "TEXTO_ANVERSO_ES")
    private String textoAnversoES;

    @Lob()
    @Column(name = "TEXTO_ANVERSO_UK")
    private String textoAnversoUK;

    @Lob()
    @Column(name = "TEXTO_CA")
    private String textoCa;

    @Lob()
    @Column(name = "TEXTO_ES")
    private String textoEs;

    @Lob()
    @Column(name = "TEXTO_UK")
    private String textoUk;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "MODERACION_FECHA")
    private Date fechaModeracion;

    @Temporal(TemporalType.DATE)
    @Column(name = "FECHA_REVISION")
    private Date fechaRevision;

    @Column(name = "MODERACION_PER_ID")
    private Long moderadorPerId;

    @Column(name = "REVISOR_PER_ID")
    private Long revisorPerId;

    @Column(name = "MODERACION_ACEPTADO")
    private Integer moderacionAceptada;

    @Column(name = "MODERACION_MOTIVO_RECHAZO")
    private String moderacionMotivoRechazo;

    @Column(name = "DESCRIPCION_CURSO_CA")
    private String descripcionCursoCA;

    @Column(name = "DESCRIPCION_CURSO_ES")
    private String descripcionCursoES;

    @Column(name = "DESCRIPCION_CURSO_UK")
    private String descripcionCursoUK;

    @Column(name = "REFERENCIA_FIRMA")
    private String referenciaFirma;

    @Column(name = "SECRETARIO_ID")
    private Long secretarioId;

    // bi-directional many-to-one association to Persona
    @ManyToOne
    @JoinColumn(name = "USUARIO_ID")
    private Persona usuario;

    @Column(name = "DESTINATARIO_PER_ID")
    private Long destinatarioId;

    // bi-directional many-to-one association to TipoCertificado
    @ManyToOne
    @JoinColumn(name = "TIPO_CERTIFICADO_ID")
    private TipoCertificado tipoCertificado;

    protected static CertificadoDAO certificadoDAO;
    protected static ApaDAO apaDAO;
    protected static UbicacionLogicaDAO ubicacionLogicaDAO;
    protected static PersonaDAO personaDAO;

    private static List<Certificado> certificadosPendientesRevisar;

    @Autowired
    protected void setCertificadoDAO(CertificadoDAO certificadoDAO)
    {
        Certificado.certificadoDAO = certificadoDAO;
    }

    @Autowired
    protected void setApaDAO(ApaDAO apaDAO)
    {
        Certificado.apaDAO = apaDAO;
    }

    @Autowired
    protected void setPersonaDAO(PersonaDAO personaDAO)
    {
        Certificado.personaDAO = personaDAO;
    }

    @Autowired
    protected void setUbicacionLogicaDAO(UbicacionLogicaDAO ubicacionLogicaDAO)
    {
        Certificado.ubicacionLogicaDAO = ubicacionLogicaDAO;
    }

    public Certificado()
    {
    }

    @QueryProjection
    public Certificado(Long id, String dniDestinatario, Date fechaCertificado,
                       Date fechaCreacion, Date fechaInicioCurso, Date fechaRecogida,
                       String mailDestinatario, String nombreDestinatario, Date fechaModeracion,
                       Date fechaRevision, Long moderadorPerId, Long revisorPerId, Integer moderacionAceptada,
                       String moderacionMotivoRechazo, String descripcionCursoCA, String descripcionCursoES,
                       String descripcionCursoUK, String referenciaFirma, Long secretarioId, Persona usuario,
                       Long destinatarioId,TipoCertificado tipoCertificado
    ) {
        this.id = id;
        this.dniDestinatario = dniDestinatario;
        this.fechaCertificado = fechaCertificado;
        this.fechaCreacion = fechaCreacion;
        this.fechaInicioCurso = fechaInicioCurso;
        this.fechaRecogida = fechaRecogida;
        this.mailDestinatario = mailDestinatario;
        this.nombreDestinatario = nombreDestinatario;
        this.fechaModeracion = fechaModeracion;
        this.fechaRevision = fechaRevision;
        this.moderadorPerId = moderadorPerId;
        this.revisorPerId = revisorPerId;
        this.moderacionAceptada = moderacionAceptada;
        this.moderacionMotivoRechazo = moderacionMotivoRechazo;
        this.descripcionCursoCA = descripcionCursoCA;
        this.descripcionCursoES = descripcionCursoES;
        this.descripcionCursoUK = descripcionCursoUK;
        this.referenciaFirma = referenciaFirma;
        this.secretarioId = secretarioId;
        this.usuario = usuario;
        this.destinatarioId = destinatarioId;
        this.tipoCertificado= tipoCertificado;
    }

    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Date getFechaRevision()
    {
        return fechaRevision;
    }

    public void setFechaRevision(Date fechaRevision)
    {
        this.fechaRevision = fechaRevision;
    }

    public String getDniDestinatario()
    {
        return this.dniDestinatario;
    }

    public void setDniDestinatario(String dniDestinatario)
    {
        this.dniDestinatario = dniDestinatario;
    }

    public Date getFechaCertificado()
    {
        return this.fechaCertificado;
    }

    public void setFechaCertificado(Date fechaCertificado)
    {
        this.fechaCertificado = fechaCertificado;
    }

    public Date getFechaCreacion()
    {
        return this.fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion)
    {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaRecogida()
    {
        return this.fechaRecogida;
    }

    public void setFechaRecogida(Date fechaRecogida)
    {
        this.fechaRecogida = fechaRecogida;
    }

    public Date getFechaInicioCurso()
    {
        return this.fechaInicioCurso;
    }

    public void setFechaInicioCurso(Date fechaInicioCurso)
    {
        this.fechaInicioCurso = fechaInicioCurso;
    }

    public String getMailDestinatario()
    {
        return this.mailDestinatario;
    }

    public void setMailDestinatario(String mailDestinatario)
    {
        this.mailDestinatario = mailDestinatario;
    }

    public String getNombreDestinatario()
    {
        return this.nombreDestinatario;
    }

    public void setNombreDestinatario(String nombreDestinatario)
    {
        this.nombreDestinatario = nombreDestinatario;
    }

    public String getTextoCa()
    {
        return this.textoCa;
    }

    public void setTextoCa(String textoCa)
    {
        this.textoCa = textoCa;
    }

    public String getTextoEs()
    {
        return this.textoEs;
    }

    public void setTextoEs(String textoEs)
    {
        this.textoEs = textoEs;
    }

    public String getTextoUk()
    {
        return this.textoUk;
    }

    public void setTextoUk(String textoUk)
    {
        this.textoUk = textoUk;
    }

    public String getTextoAnversoCA()
    {
        return textoAnversoCA;
    }

    public void setTextoAnversoCA(String textoAnversoCA)
    {
        this.textoAnversoCA = textoAnversoCA;
    }

    public String getTextoAnversoES()
    {
        return textoAnversoES;
    }

    public void setTextoAnversoES(String textoAnversoES)
    {
        this.textoAnversoES = textoAnversoES;
    }

    public String getTextoAnversoUK()
    {
        return textoAnversoUK;
    }

    public void setTextoAnversoUK(String textoAnversoUK)
    {
        this.textoAnversoUK = textoAnversoUK;
    }

    public Persona getUsuario()
    {
        return this.usuario;
    }

    public void setUsuario(Persona usuario)
    {
        this.usuario = usuario;
    }

    public Long getDestinatarioId()
    {
        return this.destinatarioId;
    }

    public void setDestinatarioId(Long destinatarioId)
    {
        this.destinatarioId = destinatarioId;
    }

    public TipoCertificado getTipoCertificado()
    {
        return this.tipoCertificado;
    }

    public void setTipoCertificado(TipoCertificado tipoCertificado)
    {
        this.tipoCertificado = tipoCertificado;
    }

    public Date getFechaModeracion()
    {
        return fechaModeracion;
    }

    public void setFechaModeracion(Date fechaModeracion)
    {
        this.fechaModeracion = fechaModeracion;
    }

    public Long getModeradorPerId()
    {
        return moderadorPerId;
    }

    public void setModeradorPerId(Long moderadorPerId)
    {
        this.moderadorPerId = moderadorPerId;
    }

    public Long getRevisorPerId()
    {
        return revisorPerId;
    }

    public void setRevisorPerId(Long revisorPerId)
    {
        this.revisorPerId = revisorPerId;
    }

    public Integer getModeracionAceptada() {
        return moderacionAceptada;
    }

    public void setModeracionAceptada(Integer moderacionAceptada) {
        this.moderacionAceptada = moderacionAceptada;
    }

        public void setModeracionAceptada(Boolean moderacionAceptada)
    {
        if (this.moderacionAceptada == null)
        {
            this.moderacionAceptada = null;
        }
        else
        {
            this.moderacionAceptada = moderacionAceptada ? 1 : 0;
        }
    }

//    public Integer getModeracionAceptada()
//    {
//        return moderacionAceptada;
//    }
//
//    public void setModeracionAceptada(Integer moderacionAceptada)
//    {
//        this.moderacionAceptada = moderacionAceptada;
//    }

    public String getModeracionMotivoRechazo()
    {
        return moderacionMotivoRechazo;
    }

    public void setModeracionMotivoRechazo(String moderacionMotivoRechazo)
    {
        this.moderacionMotivoRechazo = moderacionMotivoRechazo;
    }

    public String getDescripcionCursoCA()
    {
        return descripcionCursoCA;
    }

    public void setDescripcionCursoCA(String descripcionCursoCA)
    {
        this.descripcionCursoCA = descripcionCursoCA;
    }


//    public Boolean isModeracionAceptada()
//    {
//        if (this.moderacionAceptada == null)
//        {
//            return null;
//        }
//
//        return this.moderacionAceptada == 1;
//    }

    public static List<Certificado> getCertificados(Long tipoCertificadoId, String descripcionCurso, Long anyo, EstadoCertificado estadoCertificado, Long connectedUserId)
    {
        Map<String, Object> filtros = new HashMap<>();
        filtros.put("tipoCertificadoId", tipoCertificadoId);

        if (descripcionCurso != null)
        {
            filtros.put("descripcionCurso", descripcionCurso);
        }

        if (estadoCertificado != null)
        {
            filtros.put("estado", estadoCertificado);
        }

        if (anyo != null)
        {
            filtros.put("anyo", anyo);
        }

        if (!apaDAO.hasPerfil("CEO", "ADMIN", connectedUserId))
        {
            filtros.put("connectedUserId", connectedUserId);
        }

        return certificadoDAO.getCertificados(filtros);
    }

    public static Certificado getCertificadoById(Long certificadoId, Long connectedUserId)
            throws UnauthorizedUserException
    {
        if (apaDAO.hasPerfil("CEO", "ADMIN", connectedUserId))
        {
            return certificadoDAO.get(Certificado.class, certificadoId).get(0);
        }

        List<Certificado> listaCertificado = certificadoDAO.get(Certificado.class, certificadoId);

        if (listaCertificado.size() > 0)
        {
            Certificado certificado = listaCertificado.get(0);
            compruebaAccesoUsuarioATipoCertificado(connectedUserId, certificado.getTipoCertificado().getId());

            List<Certificado> listaCertificados = certificadoDAO.getCertificadoByUserIdAndId(connectedUserId, certificadoId);

            if (listaCertificados.size() > 0)
            {
                return listaCertificados.get(0);
            }
            else
            {
                return new Certificado();
            }
        }

        return new Certificado();
    }

    public void insert() throws ErrorInsercionCertificadoException, GeneralCEOException, IOException
    {

        RemoteAddCertificadoProcedure remoteAddCertificadoProcedure = new RemoteAddCertificadoProcedure();
        remoteAddCertificadoProcedure.init();
        Long certificadoId = remoteAddCertificadoProcedure.execute(this);

        if (certificadoId != null)
        {
            this.setId(certificadoId);
        }
        else
        {
            throw new ErrorInsercionCertificadoException();
        }
    }

    public static Certificado update(Certificado certificado)
    {
        return certificadoDAO.update(certificado);
    }

    @Role({"ADMIN", "MODERADOR"})
    public static void apruebaCertificadosByListaIds(List<String> listaCertificadosIds, Long connectedUserId)
            throws RegistroNoEncontradoException, GeneralCEOException
    {
        for (String certificadoIdString : listaCertificadosIds)
        {
            Long certificadoId = Long.parseLong(certificadoIdString);
            RemoteMarcarCertificadoModeradoProcedure remoteMarcarCertificadoModeradoProcedure = new RemoteMarcarCertificadoModeradoProcedure();
            remoteMarcarCertificadoModeradoProcedure.init();
            Map<String, Object> result = remoteMarcarCertificadoModeradoProcedure.execute(certificadoId, connectedUserId, true, "");
        }

        Certificado certificado = Certificado.getDatosCertificadoById(Long.parseLong(listaCertificadosIds.get(0)));


        String titulo = "[IGLU@CEO] Nous certificats emesos i aprovats";
        Integer numMensajes = listaCertificadosIds.size();
        String mensaje = componerTextoEmailCertificadosEmitidos(numMensajes, certificado.getDescripcionCursoCA(), true);

        TipoCertificado tipoCertificado = TipoCertificado.getTipoCertificadoById(certificado.getTipoCertificado().getId());
        List<String> listaEmailsEmisores = tipoCertificado.extraeListaDeEmailsDeEmisores();
        Email.enviaEmail(listaEmailsEmisores, titulo, mensaje);

        // Log provisional calvarin
        titulo = titulo + " - DEBUG";
        mensaje = mensaje + "\r\rEnviats a: " + listaEmailsEmisores + "\rTipoCertificado: " + tipoCertificado.getId();
        Email.enviaEmail(Arrays.asList("calvarin@uji.es"), titulo, mensaje);
        // fin log provisional
    }

    private static Certificado getDatosCertificadoById(Long certificadoId)
    {
        List<Certificado> listaCertificado = certificadoDAO.get(Certificado.class, certificadoId);
        return listaCertificado.get(0);
    }

    private static String componerTextoEmailCertificadosEmitidos(Integer numAsistentes, String descripcionCurso, Boolean aprobado)
    {
        if (aprobado)
        {
            return MessageFormat.format("S''han aprovat {0} certificats nous amb el títol ''{1}''. Pots accedir a la gestió de CEO per a consultar-los:\r\nhttp://ujiapps.uji.es/ceo", numAsistentes, descripcionCurso);
        }
        else
        {
            return MessageFormat.format("S''han denegat {0} certificats nous amb el títol ''{1}''. Pots accedir a la gestió de CEO per a consultar-los:\r\nhttp://ujiapps.uji.es/ceo", numAsistentes, descripcionCurso);
        }
    }

    private static String componerTextoEmailCertificadosPendientesDeModerar(Integer numAsistentes, String descripcionCurso)
    {
        return MessageFormat.format("S''han emés nous certificats amb el títol ''{0}'' per a {1} usuaris. Es requereix una acció per la teva part. Accedeix a:\r\nhttp://ujiapps.uji.es/ceo", descripcionCurso, numAsistentes);
    }

    @Role({"ADMIN", "USUARIO"})
    public static void apruebaRevisionCertificadosByListaIds(List<String> listaCertificadosIds, Long connectedUserId)
            throws UnauthorizedUserException, RegistroNoEncontradoException, GeneralCEOException
    {
        for (String certificadoIdString : listaCertificadosIds)
        {
            Long certificadoId = Long.parseLong(certificadoIdString);
            RemoteMarcarCertificadoRevisadoProcedure remoteMarcarCertificadoRevisadoProcedure = new RemoteMarcarCertificadoRevisadoProcedure();
            remoteMarcarCertificadoRevisadoProcedure.init();
            Map<String, Object> result = remoteMarcarCertificadoRevisadoProcedure.execute(certificadoId, connectedUserId);
        }

        enviaEmailsModeradoresCertificadosByListaIdsCertificados(listaCertificadosIds, connectedUserId);
    }

    private static void enviaEmailsModeradoresCertificadosByListaIdsCertificados(List<String> listaIdsCertificados, Long connectedUserId)
            throws UnauthorizedUserException, RegistroNoEncontradoException
    {
        Integer numAsistentes = listaIdsCertificados.size();

        Certificado certificado = Certificado.getCertificadoById(Long.parseLong(listaIdsCertificados.get(0)), connectedUserId);

        String mensaje = componerTextoEmailCertificadosPendientesDeModerar(numAsistentes, certificado.getDescripcionCursoCA());

        TipoCertificado tipoCertificado = TipoCertificado.getTipoCertificadoById(certificado.getTipoCertificado().getId());
        List<String> listaEmailsModeradores = tipoCertificado.extraeListaDeEmailsDeModeradores();

        String titulo = "[IGLU@CEO] Nous certificats pendents d'aprovació";
        Email.enviaEmail(listaEmailsModeradores, titulo, mensaje);
    }

    public static void deniegaCertificadosByListaIds(List<String> listaCertificadosIds, String mensaje, Long connectedUserId)
            throws GeneralCEOException, RegistroNoEncontradoException
    {
        for (String certificadoIdString : listaCertificadosIds)
        {
            Long certificadoId = Long.parseLong(certificadoIdString);
            RemoteMarcarCertificadoModeradoProcedure remoteMarcarCertificadoModeradoProcedure = new RemoteMarcarCertificadoModeradoProcedure();
            remoteMarcarCertificadoModeradoProcedure.init();
            Map<String, Object> result = remoteMarcarCertificadoModeradoProcedure.execute(certificadoId, connectedUserId, false, mensaje);
        }

        String titulo = "[IGLU@CEO] Certificats no aprovats";
        Integer numMensajes = listaCertificadosIds.size();

        Certificado certificado = Certificado.getDatosCertificadoById(Long.parseLong(listaCertificadosIds.get(0)));

        String emailMessage = componerTextoEmailCertificadosEmitidos(numMensajes, certificado.getDescripcionCursoCA(), false);

        TipoCertificado tipoCertificado = TipoCertificado.getTipoCertificadoById(certificado.getTipoCertificado().getId());
        List<String> listaEmailsEmisores = tipoCertificado.extraeListaDeEmailsDeEmisores();
        Email.enviaEmail(listaEmailsEmisores, titulo, emailMessage);
    }

    @Transactional
    public void insertCertificado(Long tipoCertificadoId, Long connectedUserId)
            throws UnauthorizedUserException, ErrorInsercionCertificadoException, RegistroNoEncontradoException, GeneralCEOException, IOException
    {
        compruebaPerfilUsuario(connectedUserId, TipoPermiso.REVISAR);

        compruebaAccesoUsuarioATipoCertificado(connectedUserId, tipoCertificadoId);
        insert();

        String titulo = "[IGLU@CEO] Nous certificats pendents de revisió";
        String mensaje = MessageFormat.format("S''ha emés un nou certificat amb el títol: ''{0}'' per al següent usuari:\r\n\r\n", this.getDescripcionCursoCA());

        if (this.getDestinatarioId() != null)
        {
            Persona destinatario = personaDAO.getPersonaById(this.getDestinatarioId());
            mensaje = MessageFormat.format("{0}\r\n{1}", mensaje,
                    destinatario.getNombreCompleto());
        }
        else
        {
            mensaje = MessageFormat.format("{0}\r\n{1}", mensaje, this.getNombreDestinatario());
        }

        TipoCertificado tipoCertificado = TipoCertificado.getTipoCertificadoById(tipoCertificadoId);
        List<String> listaEmailsEmisores = tipoCertificado.extraeListaDeEmailsDeEmisores();
        Email.enviaEmail(listaEmailsEmisores, titulo, mensaje);
    }

    private static void compruebaAccesoUsuarioATipoCertificado(Long connectedUserId, Long tipoCertificadoId)
            throws UnauthorizedUserException
    {
        if (!apaDAO.hasPerfil("CEO", "ADMIN", connectedUserId))
        {
            Boolean tienePermiso = TipoCertificadoPersona.compruebaPermisoPersonaACertificado(connectedUserId, tipoCertificadoId);

            if (tienePermiso == false)
            {
                throw new UnauthorizedUserException();
            }
        }
    }

    private static void compruebaPerfilUsuario(Long connectedUserId, TipoPermiso tipoPermiso)
            throws UnauthorizedUserException
    {
        switch (tipoPermiso)
        {
            case MODERAR:
                if (!(apaDAO.hasPerfil("CEO", "ADMIN", connectedUserId) || apaDAO.hasPerfil("CEO", "MODERADOR", connectedUserId)))
                {
                    throw new UnauthorizedUserException();
                }
                break;
            case REVISAR:
                if (!(apaDAO.hasPerfil("CEO", "ADMIN", connectedUserId) || apaDAO.hasPerfil("CEO", "USUARIO", connectedUserId)))
                {
                    throw new UnauthorizedUserException();
                }
        }
    }


    private String sustituyeEtiquetas(String texto)
    {
        String newTexto = texto;

        Map<String, String> mapaSustituciones = new HashMap<String, String>();

        mapaSustituciones.put("<b>", "<fo:inline font-weight=\"bold\">");
        mapaSustituciones.put("</b>", "</fo:inline>");
        mapaSustituciones.put("<i>", "<fo:inline font-style=\"italic\">");
        mapaSustituciones.put("</i>", "</fo:inline>");
        mapaSustituciones.put("<u>", "<fo:inline font-style=\"underscore\">");
        mapaSustituciones.put("</u>", "</fo:inline>");
        mapaSustituciones.put("\r\n", "<fo:block margin-bottom=\"0.25cm\"></fo:block>");
        mapaSustituciones.put("\n", "<fo:block margin-bottom=\"0.25cm\"></fo:block>");

        for (Map.Entry<String, String> entry : mapaSustituciones.entrySet())
        {
            String key = entry.getKey();
            String value = entry.getValue();

            newTexto = newTexto.replace(key, value);
        }

        return newTexto;
    }

    @Transient
    public String getTextoConEtiquetasFop(String idiomaId) throws GeneralCEOException
    {
        String texto = getTextoCa();

        if ("ES".equals(idiomaId))
        {
            texto = getTextoEs();
        }

        if ("EN".equals(idiomaId))
        {
            texto = getTextoUk();
        }

        if (texto == null)
        {
            throw new GeneralCEOException("El certificat en aquest idioma no es pot imprimir perquè no té cap text");
        }

        return sustituyeEtiquetas(texto);
    }

    public static void deleteCertificadosByListaIds(List<Long> listaCertificadosIds, Long connectedUserId)
            throws UnauthorizedUserException
    {
        if (apaDAO.hasPerfil("CEO", "ADMIN", connectedUserId))
        {
            certificadoDAO.deleteCertificadosByListaIds(listaCertificadosIds);
        }
        else
        {
            List<Long> listaCertificadosABorrar = new ArrayList<>();
            for (Long certificadoId : listaCertificadosIds)
            {
                Certificado certificado = Certificado.getCertificadoById(certificadoId, connectedUserId);

                if (certificado.getModeracionAceptada() == null && listaCertificadosIds.contains(certificado.getId()))
                {
                    listaCertificadosABorrar.add(certificado.getId());
                }
            }

            if (listaCertificadosABorrar.size() > 0)
            {
                certificadoDAO.deleteCertificadosByListaIds(listaCertificadosABorrar);
            }
        }
    }

    private static List<Long> extraeListaTiposCertificadoId(List<String> listaCertificadosIds, Long connectedUserId)
            throws UnauthorizedUserException
    {
        List<Long> listaTiposCertificadosIds = new ArrayList<Long>();

        for (String certificadoId : listaCertificadosIds)
        {
            Certificado certificado = Certificado.getCertificadoById(Long.parseLong(certificadoId), connectedUserId);
            if (!listaTiposCertificadosIds.contains(certificado.getTipoCertificado().getId()))
            {
                listaTiposCertificadosIds.add(certificado.getTipoCertificado().getId());
            }
        }
        return listaTiposCertificadosIds;
    }

    public static List<Certificado> getCertificadosByListaIdsOrderByApellido(List<Long> listaIds)
    {
        List<Certificado> certificados = certificadoDAO.getCertificadosByListaIds(listaIds);
        return ordenaPorApellidos(certificados);
    }

    private static List<Certificado> ordenaPorApellidos(List<Certificado> certificados)
    {
        Collections.sort(certificados, new Comparator<Certificado>()
        {
            @Override
            public int compare(Certificado c1, Certificado c2)
            {
                if (c1.getDestinatarioId() == null || c2.getDestinatarioId() == null)
                {
                    return 0;
                }
                Persona destinatario1 = personaDAO.getPersonaById(c1.getDestinatarioId());
                Persona destinatario2 = personaDAO.getPersonaById(c2.getDestinatarioId());
                return destinatario1.getApellido1().compareTo(destinatario2.getApellido1());
            }
        });

        return certificados;
    }

    public String getAnversoByIdiomaId(String idiomaId)
    {
        String anverso = getTextoAnversoCA();

        if ("ES".equals(idiomaId))
        {
            anverso = getTextoAnversoES();
        }

        if ("EN".equals(idiomaId))
        {
            anverso = getTextoAnversoUK();
        }

        return anverso != null ? sustituyeEtiquetas(anverso) : null;
    }

    public String getDescripcionCursoES()
    {
        return descripcionCursoES;
    }

    public void setDescripcionCursoES(String descripcionCursoES)
    {
        this.descripcionCursoES = descripcionCursoES;
    }

    public String getDescripcionCursoUK()
    {
        return descripcionCursoUK;
    }

    public void setDescripcionCursoUK(String descripcionCursoUK)
    {
        this.descripcionCursoUK = descripcionCursoUK;
    }

    public String getDescripcionCursoByIdiomaId(String idiomaId)
    {
        String descripcion = getDescripcionCursoCA();

        if ("ES".equals(idiomaId))
        {
            descripcion = getDescripcionCursoES();
        }

        if ("EN".equals(idiomaId))
        {
            descripcion = getDescripcionCursoUK();
        }

        return descripcion;
    }

    public String getReferenciaFirma()
    {
        return referenciaFirma;
    }

    public void setReferenciaFirma(String referenciaFirma)
    {
        this.referenciaFirma = referenciaFirma;
    }

    public Long getSecretarioId()
    {
        return secretarioId;
    }

    public void setSecretarioId(Long secretarioId)
    {
        this.secretarioId = secretarioId;
    }

    public static List<Integer> getAnyosCursos(Long connectedUserId)
    {
        return certificadoDAO.getAnyosCursos(connectedUserId);
    }

    @Override
    public int compareTo(Certificado cer)
    {
        String desc1 = this.getDescripcionCursoCA();
        if (desc1 == null)
        {
            desc1 = "";
        }
        String desc2 = cer.getDescripcionCursoCA();
        if (desc2 == null)
        {
            desc2 = "";
        }
        return desc1.compareTo(desc2);
    }


    public static List<String> getCursos(Long tipoCertificadoId, String descripcionCurso, Long anyo, EstadoCertificado estadoCertificado, Long connectedUserId)
    {
        Map<String, Object> filtros = new HashMap<>();
        filtros.put("tipoCertificadoId", tipoCertificadoId);
        filtros.put("descripcionCurso", descripcionCurso);
        filtros.put("anyo", anyo);
        filtros.put("estado", estadoCertificado);

        if (!apaDAO.hasPerfil("CEO", "ADMIN", connectedUserId))
        {
            filtros.put("connectedUserId", connectedUserId);
        }

        return certificadoDAO.getCursos(filtros);
    }

    public UbicacionLogica getUbicacionModeracion()
    {
        return ubicacionLogicaDAO.getUbicacionModeracion(this.getModeradorPerId());
    }

    public String getNombreCompletoDestinatario()
    {
        if (destinatarioId != null)
        {
            Persona destinatario = personaDAO.getPersonaById(destinatarioId);

            if (destinatario != null)
            {
                return destinatario.getNombreCompleto();
            }
            else
            {
                return "";
            }
        }

        return nombreDestinatario;
    }

    public String getIdentificacionDestinatario()
    {
        if (destinatarioId != null)
        {
            Persona destinatario = personaDAO.getPersonaById(destinatarioId);

            if (destinatario != null)
            {
                return destinatario.getIdentificacion();
            }
            else
            {
                return "";
            }
        }

        return getIdentificacionDestinatario();

    }
}
