package es.uji.apps.ceo.services;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import es.uji.apps.ceo.model.FirmaNotificacion;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("firma")
public class FirmaNotificacionService extends CoreBaseService
{
    @GET
    @Path("{id}")
    @Produces(MediaType.TEXT_XML)
    public List<UIEntity> getImage(@PathParam("id") String certificadoId)
    {
        List<FirmaNotificacion> listaFirmaNotificacion = FirmaNotificacion
                .getFirmaNotificacionesByCertificadoId(certificadoId);

        List<UIEntity> listaUI = UIEntity.toUI(listaFirmaNotificacion);

        return listaUI;
    }


    @GET
    @Path("sinnotificar")
    @Produces(MediaType.TEXT_XML)
    public List<UIEntity> getFirmasNotificacionSinFechaEntrada()
    {
        List<FirmaNotificacion> listaFirmaNotificacion = FirmaNotificacion
                .getFirmaNotificacionSinFechaEntrada();

        List<UIEntity> listaUI = UIEntity.toUI(listaFirmaNotificacion);

        return listaUI;
    }
}
