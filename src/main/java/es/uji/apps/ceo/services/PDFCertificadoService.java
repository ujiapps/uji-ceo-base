package es.uji.apps.ceo.services;

import java.text.DateFormatSymbols;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import es.uji.apps.ceo.GeneralCEOException;
import es.uji.apps.ceo.IdiomaNoValidoException;
import es.uji.apps.ceo.model.Certificado;
import es.uji.apps.ceo.model.SecretarioGeneral;
import es.uji.apps.ceo.model.TipoCertificado;
import es.uji.apps.ceo.model.UbicacionLogica;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;
import es.uji.commons.web.template.PDFTemplate;
import es.uji.commons.web.template.Template;

@Path("pdf")
public class PDFCertificadoService extends CoreBaseService
{
    @GET
    @Produces("application/pdf")
    public Template generateCertificate(
            @QueryParam("listaCertificadosIds") String listaCertificadosIds,
            @QueryParam("idiomaId") String idiomaId) throws UnauthorizedUserException,
            IdiomaNoValidoException, RegistroNoEncontradoException, GeneralCEOException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        Calendar calendario = Calendar.getInstance(new Locale(idiomaId));

        List<String> listaIdiomas = new ArrayList<String>();
        listaIdiomas.add("CA");
        listaIdiomas.add("ES");
        listaIdiomas.add("EN");

        if (!listaIdiomas.contains(idiomaId))
        {
            throw new IdiomaNoValidoException();
        }

        List<Long> listaIds = new ArrayList<Long>();
        for (String id : listaCertificadosIds.split(","))
        {
            listaIds.add(Long.parseLong(id));
        }
        List<Certificado> listaCertificados = Certificado.getCertificadosByListaIdsOrderByApellido(listaIds);
        TipoCertificado tipoCertificado = TipoCertificado.getTipoCertificadoById(listaCertificados.get(0).getTipoCertificado().getId());

        SimpleDateFormat sdf = new SimpleDateFormat(("dd/MM/yyyy"));
        SimpleDateFormat sdfFechaIngles = new SimpleDateFormat(("yyyy/MM/dd"));
        SimpleDateFormat sdfHora = new SimpleDateFormat(("HH.mm"));

        List<Map<String, String>> listaInfoTextos = new ArrayList<Map<String, String>>();

        for (Certificado certificado : listaCertificados)
        {
            Map<String, String> mapaCertificado = new HashMap<String, String>();
            String texto = certificado.getTextoConEtiquetasFop(idiomaId);
            String anverso = certificado.getAnversoByIdiomaId(idiomaId);
            mapaCertificado.put("idioma", idiomaId);
            mapaCertificado.put("texto", texto);
            mapaCertificado.put("referencia", certificado.getReferenciaFirma());
            mapaCertificado.put("anverso", anverso);
            mapaCertificado.put("secretarioId", certificado.getSecretarioId().toString());
            SecretarioGeneral secretarioGeneral = SecretarioGeneral.getSecretarioGeneralById(certificado.getSecretarioId());
            mapaCertificado.put("secretarioNombre", secretarioGeneral.getNombre());
            mapaCertificado.put("secretarioDNI", secretarioGeneral.getDni());
            mapaCertificado.put("secretarioFirmaBase64", secretarioGeneral.getFirmaBASE64());


            if (idiomaId.equals("CA"))
            {
                mapaCertificado.put("secretarioCargo", secretarioGeneral.getCargoCA());
                mapaCertificado.put("secretarioPrefacio", secretarioGeneral.getPrefacioCA());
            }
            else if (idiomaId.equals("ES"))
            {
                mapaCertificado.put("secretarioCargo", secretarioGeneral.getCargoES());
                mapaCertificado.put("secretarioPrefacio", secretarioGeneral.getPrefacioES());
            }
            else if (idiomaId.equals("EN"))
            {
                mapaCertificado.put("secretarioCargo", "");
                mapaCertificado.put("secretarioPrefacio", "");
                mapaCertificado.put("fecha", "");
            }
            else
            {
                mapaCertificado.put("secretarioCargo", secretarioGeneral.getCargoCA());
                mapaCertificado.put("secretarioPrefacio", secretarioGeneral.getPrefacioCA());
            }


            mapaCertificado.put("titulo", certificado.getDescripcionCursoByIdiomaId(idiomaId));
            if (certificado.getFechaModeracion() != null)
            {
                mapaCertificado.put("fechaModeracion", sdf.format(certificado.getFechaModeracion()));
                mapaCertificado.put("horaModeracion", sdfHora.format(certificado.getFechaModeracion()));

                String conector;
                String cargo;

                if (idiomaId.equals("CA"))
                {
                    conector = tipoCertificado.getCargoConectorCa();
                    cargo = tipoCertificado.getCargoTextoCa();
                }
                else if (idiomaId.equals("ES"))
                {
                    conector = tipoCertificado.getCargoConectorEs();
                    cargo = tipoCertificado.getCargoTextoEs();

                }
                else if (idiomaId.equals("EN"))
                {
                    conector = tipoCertificado.getCargoConectorUk();
                    cargo = tipoCertificado.getCargoTextoUk();
                }
                else
                {
                    conector = tipoCertificado.getCargoConectorCa();
                    cargo = tipoCertificado.getCargoTextoCa();
                }

                mapaCertificado.put("conector", conector);
                mapaCertificado.put("cargo", cargo);
            }

            if (certificado.getFechaInicioCurso() != null)
            {
                mapaCertificado.put("fechaCertificado", sdf.format(certificado.getFechaCertificado()));
            }
            listaInfoTextos.add(mapaCertificado);
        }

        Date fechaCertificado = listaCertificados.get(0).getFechaCertificado();
        calendario.setTime(fechaCertificado);
        Template template = new PDFTemplate("ceo/certificado", new Locale(idiomaId), "ceo");
        template.put("listaInfoTextos", listaInfoTextos);
        template.put("logoContentType", tipoCertificado.getContentType());
        if (tipoCertificado.getLogoBASE64() != null)
            template.put("logoBase64", tipoCertificado.getLogoBASE64());

        template.put("tipoCertificadoId", tipoCertificado.getId());
        template.put("fecha", formateaFecha(idiomaId, calendario));

        return template;
    }

    @GET
    @Path("preview")
    @Produces("application/pdf")
    public Template generatePreviewTipoCertificado(
            @QueryParam("tipoCertificadoId") String tipoCertificadoId,
            @QueryParam("idiomaId") String idiomaId) throws UnauthorizedUserException,
            IdiomaNoValidoException, RegistroNoEncontradoException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        Calendar calendario = Calendar.getInstance(new Locale(idiomaId));

        List<String> listaIdiomas = new ArrayList<String>();
        listaIdiomas.add("CA");
        listaIdiomas.add("ES");
        listaIdiomas.add("EN");

        if (!listaIdiomas.contains(idiomaId))
        {
            throw new IdiomaNoValidoException();
        }

        List<Long> listaIds = new ArrayList<Long>();
        listaIds.add(ParamUtils.parseLong(tipoCertificadoId));

        TipoCertificado tipoCertificado = TipoCertificado.getTipoCertificadoById(ParamUtils.parseLong(tipoCertificadoId));

        List<Map<String, String>> listaInfoTextos = new ArrayList<Map<String, String>>();

        Map<String, String> mapaCertificado = new HashMap<String, String>();
        String texto = tipoCertificado.getTextoConEtiquetasFop(idiomaId);
        mapaCertificado.put("texto", texto);
        mapaCertificado.put("secretarioId", "58618");
        SecretarioGeneral secretarioGeneral = SecretarioGeneral.getSecretarioGeneralById(58618L);
        mapaCertificado.put("secretarioNombre", secretarioGeneral.getNombre());
        mapaCertificado.put("secretarioDNI", secretarioGeneral.getDni());
        mapaCertificado.put("secretarioFirmaBase64", secretarioGeneral.getFirmaBASE64());


        if (idiomaId.equals("CA"))
        {
            mapaCertificado.put("secretarioCargo", secretarioGeneral.getCargoCA());
        }
        else if (idiomaId.equals("ES"))
        {
            mapaCertificado.put("secretarioCargo", secretarioGeneral.getCargoES());
        }
        else
        {
            mapaCertificado.put("secretarioCargo", secretarioGeneral.getCargoEN());
        }

        if (idiomaId.equals("CA"))
        {
            mapaCertificado.put("secretarioPrefacio", secretarioGeneral.getPrefacioCA());
        }
        else if (idiomaId.equals("ES"))
        {
            mapaCertificado.put("secretarioPrefacio", secretarioGeneral.getPrefacioES());
        }
        else
        {
            mapaCertificado.put("secretarioPrefacio", secretarioGeneral.getPrefacioEN());
        }
        mapaCertificado.put("fechaCertificado", "");

        listaInfoTextos.add(mapaCertificado);

        Template template = new PDFTemplate("ceo/certificado", new Locale(idiomaId), "ceo");
        template.put("listaInfoTextos", listaInfoTextos);
        template.put("logoContentType", tipoCertificado.getContentType());
        template.put("logoBase64", tipoCertificado.getLogoBASE64());
        template.put("tipoCertificadoId", tipoCertificado.getId());
        template.put("fecha", formateaFecha(idiomaId, calendario));

        return template;
    }

    private String formateaFecha(String idiomaId, Calendar calendario)
    {
        String formatMask = "{0} de {1} de {2,number,####}";

        if ("EN".equals(idiomaId))
        {
            formatMask = "{0} {1}, {2,number,####}";
        }
        else if ("CA".equals(idiomaId))
        {
            String mes = getNombreMes(calendario.get(Calendar.MONTH), idiomaId);
            if (Pattern.matches("^[aeiouAEIOU].+", mes))
            {
                formatMask = "{0} d''{1} de {2,number,####}";
            }
            else
            {
                formatMask = "{0} de {1} de {2,number,####}";
            }
        }

        return MessageFormat.format(formatMask, calendario.get(Calendar.DAY_OF_MONTH),
                getNombreMes(calendario.get(Calendar.MONTH), idiomaId),
                calendario.get(Calendar.YEAR));
    }

    private String getNombreMes(Integer numeroMes, String idiomaId)
    {
        DateFormatSymbols symbols = new DateFormatSymbols(new Locale(idiomaId));
        String[] monthNames = symbols.getMonths();
        return monthNames[numeroMes];
    }
}
