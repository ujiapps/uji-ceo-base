package es.uji.apps.ceo.services;

import es.uji.commons.rest.UIEntity;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("test")
public class TestService {
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public UIEntity getTest() {
        UIEntity entity = new UIEntity();
        entity.put("version", "07/09/2022 12:13");
        return entity;
    }
}
