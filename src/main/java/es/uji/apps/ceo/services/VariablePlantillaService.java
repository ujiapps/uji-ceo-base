package es.uji.apps.ceo.services;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import es.uji.apps.ceo.model.VariablePlantilla;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;

@Path("variable-plantilla")
public class VariablePlantillaService extends CoreBaseService
{
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getVariblesPlantillas()
    {
        VariablePlantilla[] variablePlantillas = VariablePlantilla.values();

        List<UIEntity> entities = new ArrayList<UIEntity>();

        for (VariablePlantilla variablePlantilla : variablePlantillas)
        {
            UIEntity entity = new UIEntity();
            
            entity.put("descripcion", variablePlantilla.getDescripcion());
            entity.put("variable", variablePlantilla.getVariable());
            
            entities.add(entity);
        }

        return entities;
    }
}
