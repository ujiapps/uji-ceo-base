package es.uji.apps.ceo.services;

import com.mysema.query.Tuple;
import com.sun.jersey.multipart.BodyPart;
import com.sun.jersey.multipart.FormDataMultiPart;
import es.uji.apps.ceo.RegistroSinCampoObligatorio;
import es.uji.apps.ceo.model.*;
import es.uji.commons.rest.*;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroDuplicadoException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

@Path("tipocertificado")
public class TipoCertificadoService extends CoreBaseService {
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTiposCertificado() throws UnauthorizedUserException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<Tuple> lista = TipoCertificado.getTiposCertificado(connectedUserId);

        List<UIEntity> listaUI = new ArrayList<UIEntity>();
        for (Tuple tipoCertificado : lista) {
            UIEntity entity = new UIEntity();
            entity.setBaseClass("TipoCertificado");
            entity.put("id", tipoCertificado.get(QTipoCertificado.tipoCertificado.id));
            entity.put("descripcionCa", tipoCertificado.get(QTipoCertificado.tipoCertificado.descripcionCa));
            entity.put("descripcionEs", tipoCertificado.get(QTipoCertificado.tipoCertificado.descripcionEs));
            entity.put("descripcionUk", tipoCertificado.get(QTipoCertificado.tipoCertificado.descripcionUk));
            entity.put("emisionManual", tipoCertificado.get(QTipoCertificado.tipoCertificado.emisionManual));
            listaUI.add(entity);
        }
        return listaUI;
    }

    @GET
    @Path("manual")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTiposCertificadoManual() throws UnauthorizedUserException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<Tuple> lista = TipoCertificado.getTiposCertificadoEmisionManual(connectedUserId);

        List<UIEntity> listaUI = new ArrayList<UIEntity>();
        for (Tuple tipoCertificado : lista) {
            UIEntity entity = new UIEntity();
            entity.setBaseClass("TipoCertificado");
            entity.put("id", tipoCertificado.get(QTipoCertificado.tipoCertificado.id));
            entity.put("descripcionCa", tipoCertificado.get(QTipoCertificado.tipoCertificado.descripcionCa));
            entity.put("descripcionEs", tipoCertificado.get(QTipoCertificado.tipoCertificado.descripcionEs));
            entity.put("descripcionUk", tipoCertificado.get(QTipoCertificado.tipoCertificado.descripcionUk));
            entity.put("plantillaCa", tipoCertificado.get(QTipoCertificado.tipoCertificado.plantillaCa));
            entity.put("plantillaEs", tipoCertificado.get(QTipoCertificado.tipoCertificado.plantillaEs));
            entity.put("plantillaUk", tipoCertificado.get(QTipoCertificado.tipoCertificado.plantillaUk));

            entity.put("emisionManual", tipoCertificado.get(QTipoCertificado.tipoCertificado.emisionManual));
            listaUI.add(entity);
        }
        return listaUI;
    }

    @GET
    @Path("moderacion")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTiposCertificadoModeracion() throws UnauthorizedUserException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<TipoCertificado> lista = TipoCertificado.getTiposCertificadoModeracion(connectedUserId);
        List<UIEntity> listaUI = UIEntity.toUI(lista);

        return listaUI;
    }

    @GET
    @Path("revision")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTiposCertificadoRevision() throws UnauthorizedUserException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<TipoCertificado> lista = TipoCertificado.getTiposCertificadoRevision(connectedUserId);
        List<UIEntity> listaUI = UIEntity.toUI(lista);

        return listaUI;
    }


    @GET
    @Path("{tipoCertificadoId}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTipoCertificadoById(
            @PathParam("tipoCertificadoId") String tipoCertificadoId)
            throws UnauthorizedUserException, RegistroNoEncontradoException, NumberFormatException {

        Long connectedUserId = AccessManager.getConnectedUserId(request);

        TipoCertificado tipoCertificado = TipoCertificado.getTipoCertificadoById(Long.parseLong(tipoCertificadoId));

        UIEntity entity = UIEntity.toUI(tipoCertificado);
        Persona persona = Persona.getPersonaById(tipoCertificado.getCargoPerId());
        entity.put("cargoNombre", persona.getNombreCompleto());
        return Collections.singletonList(entity);
    }

    @GET
    @Path("{tipoCertificadoId}/imagen/raw")
    public Response getImagenTipoCertificadoById(
            @PathParam("tipoCertificadoId") String tipoCertificadoId)
            throws UnauthorizedUserException, RegistroNoEncontradoException, NumberFormatException {
        TipoCertificado tipoCertificado = TipoCertificado.getTipoCertificadoById(Long.parseLong(tipoCertificadoId));

        byte[] documento = tipoCertificado.getLogo();
        String contentType = tipoCertificado.getContentType();

        return Response.ok(documento).header("Content-Length", documento.length).type(contentType).build();
    }

    @GET
    @Path("{tipoCertificadoId}/imagen")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getTieneImagenTipoCertificadoById(
            @PathParam("tipoCertificadoId") String tipoCertificadoId)
            throws UnauthorizedUserException, RegistroNoEncontradoException, NumberFormatException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        TipoCertificado tipoCertificado = TipoCertificado.getTipoCertificadoById(Long
                .parseLong(tipoCertificadoId));

        byte[] documento = tipoCertificado.getLogo();
        String contentType = tipoCertificado.getContentType();

        List<UIEntity> listaUI = new ArrayList<UIEntity>();
        if (contentType != null && documento.length > 0) {
            UIEntity entity = new UIEntity();
            entity.setBaseClass("TipoCertificado");
            entity.put("id", tipoCertificado.getId());
            entity.put("contentType", tipoCertificado.getContentType());
            listaUI.add(entity);
        }
        return listaUI;
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> insertTipoCertificado(MultivaluedMap<String, String> map) 
            throws UnauthorizedUserException, RegistroSinCampoObligatorio {
        Long userId = AccessManager.getConnectedUserId(request);

        String descripcionCa = map.getFirst("descripcionCa");
        String descripcionEs = map.getFirst("descripcionEs");
        String descripcionUk = map.getFirst("descripcionUk");
        String emisionManual = map.getFirst("emisionManual");
        String cargoConectorCa = map.getFirst("cargoConectorCa");
        String cargoTextoCa = map.getFirst("cargoTextoCa");
        String cargoConectorEs = map.getFirst("cargoConectorEs");
        String cargoTextoEs = map.getFirst("cargoTextoEs");
        String cargoConectorUk = map.getFirst("cargoConectorUk");
        String cargoTextoUk = map.getFirst("cargoTextoUk");
        Long cargoPerId = Long.parseLong(map.getFirst("cargoPerId"));

        if (descripcionCa == null) {
            return Collections.singletonList(new UIEntity());
        }

        ParamUtils.checkNotNull(descripcionCa);
        ParamUtils.checkNotNull(cargoConectorCa);
        ParamUtils.checkNotNull(cargoConectorEs);
        ParamUtils.checkNotNull(cargoTextoCa);
        ParamUtils.checkNotNull(cargoTextoEs);

        if ((descripcionCa.trim().length() == 0) ||
            (cargoConectorCa.trim().length() == 0) ||
            (cargoConectorEs.trim().length() == 0) ||
            (cargoTextoCa.trim().length() == 0) ||
            (cargoTextoEs.trim().length() == 0)
           )
        {
            throw new RegistroSinCampoObligatorio();
        }

        TipoCertificado tipoCertificado = new TipoCertificado();
        tipoCertificado.setDescripcionCa(descripcionCa);
        tipoCertificado.setDescripcionEs(descripcionEs);
        tipoCertificado.setDescripcionUk(descripcionUk);
        tipoCertificado.setCargoConectorCa(cargoConectorCa);
        tipoCertificado.setCargoConectorEs(cargoConectorEs);
        tipoCertificado.setCargoConectorUk(cargoConectorUk);
        tipoCertificado.setCargoTextoCa(cargoTextoCa);
        tipoCertificado.setCargoTextoEs(cargoTextoEs);
        tipoCertificado.setCargoTextoUk(cargoTextoUk);
        tipoCertificado.setCargoPerId(cargoPerId);

        if (emisionManual!= null && emisionManual.equals("on")) {
            tipoCertificado.setEmisionManual(1L);
        } else {
            tipoCertificado.setEmisionManual(0L);
        }

        tipoCertificado = tipoCertificado.insert(userId);

        return Collections.singletonList(UIEntity.toUI(tipoCertificado));
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> updateTipoCertificado(@PathParam("id") Long id, MultivaluedMap<String, String> map)
            throws UnauthorizedUserException, RegistroSinCampoObligatorio {
        Long userId = AccessManager.getConnectedUserId(request);

        String descripcionCa = map.getFirst("descripcionCa");
        String descripcionEs = map.getFirst("descripcionEs");
        String descripcionUk = map.getFirst("descripcionUk");
        String emisionManual = map.getFirst("emisionManual");
        String cargoConectorCa = map.getFirst("cargoConectorCa");
        String cargoTextoCa = map.getFirst("cargoTextoCa");
        String cargoConectorEs = map.getFirst("cargoConectorEs");
        String cargoTextoEs = map.getFirst("cargoTextoEs");
        String cargoConectorUk = map.getFirst("cargoConectorUk");
        String cargoTextoUk = map.getFirst("cargoTextoUk");
        Long cargoPerId = Long.parseLong(map.getFirst("cargoPerId"));

        if (descripcionCa == null) {
            return Collections.singletonList(new UIEntity());
        }

        ParamUtils.checkNotNull(descripcionCa);
        ParamUtils.checkNotNull(cargoConectorCa);
        ParamUtils.checkNotNull(cargoConectorEs);
        ParamUtils.checkNotNull(cargoTextoCa);
        ParamUtils.checkNotNull(cargoTextoEs);

        if ((descripcionCa.trim().length() == 0) ||
                (cargoConectorCa.trim().length() == 0) ||
                (cargoConectorEs.trim().length() == 0) ||
                (cargoTextoCa.trim().length() == 0) ||
                (cargoTextoEs.trim().length() == 0)
        )
        {
            throw new RegistroSinCampoObligatorio();
        }

        TipoCertificado tipoCertificado = new TipoCertificado();
        tipoCertificado.setId(id);
        tipoCertificado.setDescripcionCa(descripcionCa);
        tipoCertificado.setDescripcionEs(descripcionEs);
        tipoCertificado.setDescripcionUk(descripcionUk);
        tipoCertificado.setCargoConectorCa(cargoConectorCa);
        tipoCertificado.setCargoConectorEs(cargoConectorEs);
        tipoCertificado.setCargoConectorUk(cargoConectorUk);
        tipoCertificado.setCargoTextoCa(cargoTextoCa);
        tipoCertificado.setCargoTextoEs(cargoTextoEs);
        tipoCertificado.setCargoTextoUk(cargoTextoUk);
        tipoCertificado.setCargoPerId(cargoPerId);

        if (emisionManual!= null && emisionManual.equals("on")) {
            tipoCertificado.setEmisionManual(1L);
        } else {
            tipoCertificado.setEmisionManual(0L);
        }

        tipoCertificado.update(userId);

        return Collections.singletonList(UIEntity.toUI(tipoCertificado));
    }

    private String getParamString(FormDataMultiPart multiPart, String param) {
        try {
            return multiPart.getField(param).getValue();
        } catch (NullPointerException e) {
            return "";
        }
    }

    @POST
    @Path("{tipoCertificadoId}/plantillas")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> updatePlantillasTipoCertificado(
            @PathParam("tipoCertificadoId") String tipoCertificadoId, FormDataMultiPart multiPart)
            throws UnauthorizedUserException, IOException, RegistroNoEncontradoException,
            NumberFormatException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        String plantillaCa = getParamString(multiPart, "plantillaCa");
        String plantillaEs = getParamString(multiPart, "plantillaEs");
        String plantillaUk = getParamString(multiPart, "plantillaUk");

        TipoCertificado tipoCertificado = TipoCertificado.getTipoCertificadoById(Long.parseLong(tipoCertificadoId));

        tipoCertificado.updatePlantillas(plantillaCa, plantillaEs, plantillaUk, connectedUserId);

        return Collections.singletonList(UIEntity.toUI(tipoCertificado));
    }

    @POST
    @Path("{tipoCertificadoId}/responsable")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> updateResponsableTipoCertificado(@PathParam("tipoCertificadoId") String tipoCertificadoId, MultivaluedMap<String, String> map)
            throws RegistroNoEncontradoException, NumberFormatException {

        Long connectedUserId = AccessManager.getConnectedUserId(request);

        Long cargoPersonaId = Long.parseLong(map.getFirst("cargoPerId"));
        String cargoConectorCa = map.getFirst("cargoConectorCa");
        String cargoConectorEs = map.getFirst("cargoConectorEs");
        String cargoConectorUk = map.getFirst("cargoConectorUk");
        String cargoTextoCa = map.getFirst("cargoTextoCa");
        String cargoTextoEs = map.getFirst("cargoTextoEs");
        String cargoTextoUk = map.getFirst("cargoTextoUk");

        TipoCertificado tipoCertificado = TipoCertificado.getTipoCertificadoById(Long.parseLong(tipoCertificadoId));

        tipoCertificado.updateResponsable(cargoPersonaId, cargoConectorCa, cargoTextoCa, cargoConectorEs, cargoTextoEs, cargoConectorUk, cargoTextoUk, connectedUserId);

        return Collections.singletonList(UIEntity.toUI(tipoCertificado));
    }

    @POST
    @Path("{tipoCertificadoId}/logo")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> updateLogoTipoCertificado(
            @PathParam("tipoCertificadoId") String tipoCertificadoId, FormDataMultiPart multiPart)
            throws UnauthorizedUserException, IOException, RegistroNoEncontradoException,
            NumberFormatException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        byte[] imagen = null;
        String mimeType = null;

        if (multiPart.getField("imagen") != null) {
            imagen = StreamUtils.inputStreamToByteArray(multiPart.getField("imagen").getEntityAs(
                    InputStream.class));

            if (imagen.length > 0) {
                for (BodyPart bodyPart : multiPart.getBodyParts()) {
                    mimeType = bodyPart.getHeaders().getFirst("Content-Type");
                    if (mimeType != null && !mimeType.isEmpty()) {
                        break;
                    }
                }
            }
        }

        TipoCertificado tipoCertificado = TipoCertificado.getTipoCertificadoById(Long
                .parseLong(tipoCertificadoId));

        if (imagen.length > 0) {
            tipoCertificado.updateLogo(imagen, mimeType, connectedUserId);
        }

        return Collections.singletonList(UIEntity.toUI(tipoCertificado));
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void deleteTipoCertificado(@PathParam("id") String id) throws RegistroConHijosException {
        Long userId = AccessManager.getConnectedUserId(request);

        TipoCertificado.delete(Long.parseLong(id), userId);
    }

    @GET
    @Path("{id}/ubicacionlogica")
    @Produces(MediaType.APPLICATION_JSON)

    public List<UIEntity> getPermisosUbicacionLogica(@PathParam("id") String tipoCertificadoId)
            throws UnauthorizedUserException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<UbicacionLogica> ubicacionesLogicas = null;

        if (tipoCertificadoId != null && !tipoCertificadoId.isEmpty()) {
            ubicacionesLogicas = Permiso.getUbicacionesLogicasConPermisoPorTipoCertificadoId(
                    Long.parseLong(tipoCertificadoId), connectedUserId);
        }

        return UIEntity.toUI(ubicacionesLogicas);
    }

    @GET
    @Path("{id}/permisopersona")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getPermisosPersona(@PathParam("id") String tipoCertificadoId)
            throws UnauthorizedUserException {
        Long userId = AccessManager.getConnectedUserId(request);
        List<Persona> personas = null;

        if (tipoCertificadoId != null && !tipoCertificadoId.isEmpty()) {
            personas = Permiso.getPersonasConPermisoPorTipoCertificadoId(
                    Long.parseLong(tipoCertificadoId), userId);
        }

        List<UIEntity> personasUI = new ArrayList<UIEntity>();
        for (Persona persona : personas) {
            UIEntity personaUI = UIEntity.toUI(persona);
            personaUI.put("nombreCompleto", persona.getNombreCompleto());
            personasUI.add(personaUI);
        }
        return personasUI;
    }

    @POST
    @Path("{id}/permisopersona")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> setPermisosPersona(@PathParam("id") String tipoCertificadoId,
                                             UIEntity uiEntity) throws RegistroDuplicadoException, UnauthorizedUserException,
            RegistroNoEncontradoException, NumberFormatException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        UIEntity entity = new UIEntity();

        Long personaId = Long.parseLong(uiEntity.get("id"));

        if (uiEntity != null && uiEntity.get("id") != null && tipoCertificadoId != null
                && !tipoCertificadoId.isEmpty()) {
            Persona persona = Persona.getPersonaById(personaId);
            TipoCertificado tipoCertificado = TipoCertificado.getTipoCertificadoById(Long
                    .parseLong(tipoCertificadoId));

            if (Permiso.personaTienePermisoATipoCertificado(personaId,
                    Long.parseLong(tipoCertificadoId))) {
                throw new RegistroDuplicadoException();
            }

            Permiso permiso = new Permiso();
            permiso.setPersona(persona);
            permiso.setTipoCertificado(tipoCertificado);
            permiso.insertTipoCertificado(connectedUserId);

            entity = UIEntity.toUI(persona);
            entity.put("nombreCompleto", persona.getNombreCompleto());
        }

        return Collections.singletonList(entity);
    }

    @POST
    @Path("{id}/ubicacionlogica")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> setPermisosUbicacionLogica(@PathParam("id") String tipoCertificadoId,
                                                     UIEntity uiEntity) throws RegistroDuplicadoException, UnauthorizedUserException,
            RegistroNoEncontradoException, NumberFormatException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        UIEntity entity = new UIEntity();

        Long ubicacionLogicaId = Long.parseLong(uiEntity.get("id"));

        if (uiEntity != null && uiEntity.get("id") != null && tipoCertificadoId != null
                && !tipoCertificadoId.isEmpty()) {
            UbicacionLogica ubicacionLogica = UbicacionLogica.getUbicacionLogicaById(
                    ubicacionLogicaId, connectedUserId);
            TipoCertificado tipoCertificado = TipoCertificado.getTipoCertificadoById(Long
                    .parseLong(tipoCertificadoId));

            if (Permiso.ubicacionLogicaTienePermisoATipoCertificado(ubicacionLogicaId,
                    Long.parseLong(tipoCertificadoId))) {
                throw new RegistroDuplicadoException();
            }

            Permiso permiso = new Permiso();
            permiso.setUbicacionLogica(ubicacionLogica);
            permiso.setTipoCertificado(tipoCertificado);
            permiso.insertUbicacionLogica(connectedUserId);

            entity = UIEntity.toUI(ubicacionLogica);
        }

        return Collections.singletonList(entity);
    }

    @DELETE
    @Path("{id}/permisopersona/{idPersona}")
    public void deletePermisosPersona(@PathParam("id") String tipoCertificadoId,
                                      @PathParam("idPersona") String personaId) throws UnauthorizedUserException {
        Long userId = AccessManager.getConnectedUserId(request);
        if (tipoCertificadoId != null && !tipoCertificadoId.isEmpty() && personaId != null
                && !personaId.isEmpty()) {
            Permiso.deletePersonaConPermisoPorTipoCertificadoId(Long.parseLong(personaId),
                    Long.parseLong(tipoCertificadoId), userId);
        }
    }

    @DELETE
    @Path("{id}/imagen")
    public void deleteImagenTipoCertificado(@PathParam("id") String tipoCertificadoId,
                                            @PathParam("idPersona") String personaId) throws UnauthorizedUserException, RegistroNoEncontradoException, NumberFormatException {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        TipoCertificado tipoCertificado = TipoCertificado.getTipoCertificadoById(Long
                .parseLong(tipoCertificadoId));
        tipoCertificado.borraImagen(connectedUserId);
    }

    @DELETE
    @Path("{id}/ubicacionlogica/{idUbicacionLogica}")
    public void deletePermisosUbicacionLogica(@PathParam("id") String tipoCertificadoId,
                                              @PathParam("idUbicacionLogica") String ubicacionLogicaId)
            throws UnauthorizedUserException {
        Long userId = AccessManager.getConnectedUserId(request);

        if (tipoCertificadoId != null && !tipoCertificadoId.isEmpty() && ubicacionLogicaId != null
                && !ubicacionLogicaId.isEmpty()) {
            Permiso.deleteUbicacionLogicaConPermisoPorTipoCertificadoId(
                    Long.parseLong(ubicacionLogicaId), Long.parseLong(tipoCertificadoId), userId);
        }
    }

}
