package es.uji.apps.ceo.services;

import com.mysema.query.Tuple;
import es.uji.apps.ceo.*;
import es.uji.apps.ceo.model.Certificado;
import es.uji.apps.ceo.model.Persona;
import es.uji.apps.ceo.model.QCertificado;
import es.uji.apps.ceo.model.TipoCertificado;
import es.uji.apps.ceo.model.TipoUsuarioEnum;
import es.uji.apps.ceo.model.domains.EstadoCertificado;
import es.uji.commons.messaging.client.MessageNotSentException;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.rest.exceptions.RegistroConHijosException;
import es.uji.commons.rest.exceptions.RegistroNoEncontradoException;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

@Path("certificado")
public class CertificadoService extends CoreBaseService
{
    private SimpleDateFormat formatter;

    public CertificadoService()
    {
        formatter = new SimpleDateFormat("dd/MM/yyyy");
    }

        public Boolean isModeracionAceptada(Integer moderacionAceptada)
    {
        if (moderacionAceptada == null)
        {
            return null;
        }

        return moderacionAceptada == 1;
    }

    private UIEntity certificadoTOUISimplificado(Certificado certificado)
    {
        UIEntity ui = new UIEntity();
        ui.setBaseClass("Certificado");

        ui.put("moderacionAceptada", isModeracionAceptada(certificado.getModeracionAceptada()));
        ui.put("tipoCertificado", certificado.getTipoCertificado().getDescripcionCa());
        ui.put("descripcionCa", certificado.getDescripcionCursoCA());
        ui.put("id", certificado.getId());
        ui.put("moderadorPerId", certificado.getModeradorPerId());

        if (certificado.getModeracionMotivoRechazo() != null
                && !certificado.getModeracionMotivoRechazo().isEmpty())
        {
            ui.put("moderacionMotivoRechazo", certificado.getModeracionMotivoRechazo());
        }

        if (certificado.getReferenciaFirma() != null && !certificado.getReferenciaFirma().isEmpty())
        {
            ui.put("referenciaFirma", certificado.getReferenciaFirma());
        }

        ui.put("mailDestinatario", certificado.getMailDestinatario());
        if (certificado.getDestinatarioId() != null)
        {
            ui.put("destinatarioId", certificado.getDestinatarioId());
            ui.put("nombreDestinatario", certificado.getNombreCompletoDestinatario());
            ui.put("dniDestinatario", certificado.getIdentificacionDestinatario());
        }
        else
        {
            ui.put("nombreDestinatario", certificado.getNombreDestinatario());
            ui.put("dniDestinatario", certificado.getDniDestinatario());
        }

        ui.put("fechaCertificado", certificado.getFechaCertificado());
        ui.put("fechaRevision", certificado.getFechaRevision());
        ui.put("fechaRecogida", certificado.getFechaRecogida());
        ui.put("fechaModeracion", certificado.getFechaModeracion());
        ui.put("fechaCreacion", certificado.getFechaCreacion());
        ui.put("fechaInicioCurso", certificado.getFechaInicioCurso());

        return ui;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getCertificados(@QueryParam("tipoCertificadoId") Long tipoCertificadoId,
            @QueryParam("descripcionCurso") String descripcionCurso, @QueryParam("anyo") Long anyo,
            @QueryParam("estado") String estadoCertificado) throws UnauthorizedUserException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        ArrayList<UIEntity> listaUIEntity = new ArrayList<UIEntity>();
        List<Certificado> certificados = new ArrayList<Certificado>();

        certificados = Certificado.getCertificados(tipoCertificadoId, descripcionCurso, anyo,
                EstadoCertificado.getEstadoByDescripcion(estadoCertificado), connectedUserId);

        for (Certificado certificado : certificados)
        {
            UIEntity uiEntity = certificadoTOUISimplificado(certificado);
            listaUIEntity.add(uiEntity);
        }

        return listaUIEntity;
    }

    private List<UIEntity> creaListaCertificadosConDescripcionYEdicion(
            List<Certificado> listaCertificados)
    {
        List<UIEntity> listaUI = new ArrayList<>();

        for (Certificado certificado : listaCertificados)
        {
            UIEntity ui = UIEntity.toUI(certificado);
            String descripcion = certificado.getDescripcionCursoCA();

            if (certificado.getFechaInicioCurso() != null)
            {
                descripcion = descripcion + " (" + getYear(certificado.getFechaInicioCurso()) + ")";
            }
            ui.put("descripcionYEdicion", descripcion);
            listaUI.add(ui);
        }
        return listaUI;
    }

    @GET
    @Path("descripcionCursosYAnverso/{tipoCertificadoId}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> descripcionCursosYAnversosCertificadosByTipoCertificado(
            @PathParam("tipoCertificadoId") Long tipoCertificadoId,
            @QueryParam("descripcionCurso") String descripcionCurso, @QueryParam("anyo") Long anyo,
            @QueryParam("estado") String estadoCertificado) throws UnauthorizedUserException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<Certificado> listaCertificados = Certificado.getCertificados(tipoCertificadoId,
                descripcionCurso, anyo, EstadoCertificado.getEstadoByDescripcion(estadoCertificado),
                connectedUserId);

        List<Certificado> listaResultado = new ArrayList<Certificado>();
        List<String> listaDescripciones = new ArrayList<String>();

        Collections.sort(listaCertificados);

        for (Certificado certificado : listaCertificados)
        {
            if (certificado.getDescripcionCursoCA() != null
                    && !certificado.getDescripcionCursoCA().isEmpty()
                    && !listaDescripciones.contains(certificado.getDescripcionCursoCA()))
            {
                listaResultado.add(certificado);
                listaDescripciones.add(certificado.getDescripcionCursoCA());
            }
        }

        return creaListaCertificadosConDescripcionYEdicion(listaResultado);
    }

    @GET
    @Path("anyos")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> anyoCursos() throws UnauthorizedUserException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<Integer> listaAnyos = Certificado.getAnyosCursos(connectedUserId);

        Collections.sort(listaAnyos, Collections.reverseOrder());

        List<UIEntity> listaAnyosUI = new ArrayList<UIEntity>();
        Integer counter = 1;
        for (Integer anyo : listaAnyos)
        {
            UIEntity entity = new UIEntity();
            entity.setBaseClass("Anyo");
            entity.put("anyo", anyo);
            entity.put("id", anyo);
            listaAnyosUI.add(entity);
        }

        return listaAnyosUI;
    }

    @GET
    @Path("descripcionCursos")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> descripcionCursos(@QueryParam("tipoCertificadoId") Long tipoCertificadoId,
            @QueryParam("descripcionCurso") String descripcionCurso, @QueryParam("anyo") Long anyo,
            @QueryParam("estado") String estadoCertificado) throws UnauthorizedUserException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<String> listaCertificados = Certificado.getCursos(tipoCertificadoId, descripcionCurso, anyo, EstadoCertificado.getEstadoByDescripcion(estadoCertificado), connectedUserId);
        List<UIEntity> listaDescripcionesUI = extraeDescripcionCursos(listaCertificados);

        return listaDescripcionesUI;
    }

    private String getYear(Date date)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        Integer year = cal.get(Calendar.YEAR);
        return year.toString();
    }

    private List<UIEntity> extraeDescripcionCursos(List<String> listaCertificados)
    {
        AtomicInteger index = new AtomicInteger(1);
        return listaCertificados.stream().map(descripcion -> {
            UIEntity entity = new UIEntity();
            entity.setBaseClass("Certificado");
            entity.put("id", index.getAndIncrement());
            entity.put("descripcionCurso", descripcion);
            return entity;
        }).collect(Collectors.toList());
    }

    @GET
    @Path("pendientes/moderar")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> certificadosPendientesModerarByTipoCertificado(
            @QueryParam("tipoCertificadoId") Long tipoCertificadoId,
            @QueryParam("descripcionCurso") String descripcionCurso, @QueryParam("anyo") Long anyo)
            throws UnauthorizedUserException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<Certificado> listaCertificados = new ArrayList<Certificado>();
        listaCertificados = Certificado.getCertificados(tipoCertificadoId, descripcionCurso, anyo,
                EstadoCertificado.PENDIENTE_MODERAR, connectedUserId);

        ArrayList<UIEntity> listaUIEntity = new ArrayList<UIEntity>();

        for (Certificado certificado : listaCertificados)
        {
            UIEntity uiEntity = certificadoTOUISimplificado(certificado);
            listaUIEntity.add(uiEntity);
        }

        return listaUIEntity;
    }

    @GET
    @Path("pendientes/revisar")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> certificadosPendientesRevisarByTipoCertificado(
            @QueryParam("tipoCertificadoId") Long tipoCertificadoId,
            @QueryParam("descripcionCurso") String descripcionCurso, @QueryParam("anyo") Long anyo)
            throws UnauthorizedUserException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<Certificado> listaCertificados = Certificado.getCertificados(tipoCertificadoId, descripcionCurso, anyo,
                EstadoCertificado.PENDIENTE_REVISAR, connectedUserId);

        ArrayList<UIEntity> listaUIEntity = new ArrayList<UIEntity>();

        for (Certificado certificado : listaCertificados)
        {
            UIEntity uiEntity = certificadoTOUISimplificado(certificado);
            listaUIEntity.add(uiEntity);
        }

        return listaUIEntity;
    }

    @PUT
    @Path("{certificadoId}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void updateCertificado(@PathParam("certificadoId") String certificadoId, UIEntity entity)
            throws ModificacionFechaRecogidaDenegadaException, UnauthorizedUserException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        Date fechaRecogida = getDate(entity.get("fechaRecogida"));

        Certificado certificado = Certificado
                .getCertificadoById(ParamUtils.parseLong(certificadoId), connectedUserId);

        if (isModeracionAceptada(certificado.getModeracionAceptada()))
        {
            certificado.setFechaRecogida(fechaRecogida);
            Certificado.update(certificado);
        }
        else
        {
            throw new ModificacionFechaRecogidaDenegadaException();
        }
    }

    @POST
    @Path("pendientes/moderar")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public ResponseMessage moderarCertificadosPendientes(MultivaluedMap<String, String> map)
            throws ModeracionDeCertificadoDenegadaException, UnauthorizedUserException,
            RegistroNoEncontradoException, GeneralCEOException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<String> listaCertificadosIds = map.get("listaCertificadosIds");
        String moderar = map.getFirst("moderar");

        if (new Boolean(moderar))
        {
            Certificado.apruebaCertificadosByListaIds(listaCertificadosIds, connectedUserId);
        }
        else
        {
            String mensaje = map.getFirst("mensaje");
            Certificado.deniegaCertificadosByListaIds(listaCertificadosIds, mensaje,
                    connectedUserId);
        }

        return new ResponseMessage(true);
    }

    @POST
    @Path("pendientes/revisar")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public void revisarCertificadosPendientes(MultivaluedMap<String, String> map)
            throws ModeracionDeCertificadoDenegadaException, UnauthorizedUserException,
            RegistroNoEncontradoException, GeneralCEOException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        List<String> listaCertificadosIds = map.get("listaCertificadosIds");

        Certificado.apruebaRevisionCertificadosByListaIds(listaCertificadosIds, connectedUserId);

        return;
    }

    @POST
    @Produces(MediaType.APPLICATION_XML)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public ResponseMessage insertCertificado(MultivaluedMap<String, String> map)
            throws UnauthorizedUserException, NumberFormatException, MessageNotSentException,
            ErrorInsercionCertificadoException, RegistroNoEncontradoException, GeneralCEOException,
            IOException
    {
        String tipoCertificadoId = map.getFirst("tipoCertificado");
        String tipoUsuario = map.getFirst("tipoUsuario");
        String fechaCertificadoStr = map.getFirst("fechaCertificado");
        String fechaInicioCertificadoStr = map.getFirst("fechaInicioCertificado");
        String textoCa = map.getFirst("textoCa");
        String textoEs = map.getFirst("textoEs");
        String textoUk = map.getFirst("textoUk");
        String textoAnversoCA = map.getFirst("textoAnversoCA");
        String textoAnversoES = map.getFirst("textoAnversoES");
        String textoAnversoUK = map.getFirst("textoAnversoUK");
        String dniDestinatario = "";
        String emailDestinatario = "";
        String destinatarioId = "";
        String nombreDestinatario = "";
        String descripcionCurso = "";

        String descripcionCursoCA = map.getFirst("descripcionCursoCA");
        String descripcionCursoES = map.getFirst("descripcionCursoES");
        String descripcionCursoUK = map.getFirst("descripcionCursoUK");

        if (descripcionCursoUK.isEmpty())
        {
            descripcionCursoUK = descripcionCursoCA;
        }

        if (textoAnversoUK.isEmpty())
        {
            textoAnversoUK = textoAnversoCA;
        }

        ParamUtils.checkNotNull(tipoCertificadoId, tipoUsuario, fechaCertificadoStr);

        if (TipoUsuarioEnum.INTERNO.toString().equals(tipoUsuario))
        {
            destinatarioId = map.getFirst("DestinatarioUsuarioInterno");
            ParamUtils.checkNotNull(destinatarioId);
        }
        else
        {
            nombreDestinatario = map.getFirst("DestinatarioUsuarioExterno");
            dniDestinatario = map.getFirst("DNIUsuarioExterno");
            emailDestinatario = map.getFirst("EMailUsuarioExterno");

            ParamUtils.checkNotNull(nombreDestinatario, dniDestinatario, emailDestinatario);
        }

        Certificado certificado = new Certificado();

        Long connectedUserId = AccessManager.getConnectedUserId(request);

        certificado.setUsuario(Persona.getPersonaById(connectedUserId));
        if (!destinatarioId.equals(""))
        {
            Long destinatarioUserId = ParamUtils.parseLong(destinatarioId);
            certificado.setDestinatarioId(destinatarioUserId);
        }
        certificado.setDniDestinatario(dniDestinatario);
        certificado.setMailDestinatario(emailDestinatario);
        certificado.setDescripcionCursoCA(descripcionCursoCA);
        certificado.setDescripcionCursoES(descripcionCursoES);
        certificado.setDescripcionCursoUK(descripcionCursoUK);
        certificado.setNombreDestinatario(nombreDestinatario);
        certificado.setTipoCertificado(TipoCertificado.getTipoCertificadoById(ParamUtils.parseLong(tipoCertificadoId)));
        certificado.setFechaCreacion(new Date());
        certificado.setFechaCertificado(getDate(fechaCertificadoStr));
        certificado.setFechaInicioCurso(getDate(fechaInicioCertificadoStr));
        certificado.setTextoCa(textoCa);
        certificado.setTextoEs(textoEs);
        certificado.setTextoUk(textoUk);
        certificado.setTextoAnversoCA(textoAnversoCA);
        certificado.setTextoAnversoES(textoAnversoES);
        certificado.setTextoAnversoUK(textoAnversoUK);

        certificado.insertCertificado(Long.parseLong(tipoCertificadoId), connectedUserId);

        return new ResponseMessage(true);
    }

    public Date getDate(String value)
    {
        try
        {
            return formatter.parse(value);
        }
        catch (Exception e)
        {
            return null;
        }
    }

    private List<Long> convierteArrayStringsALongs(List<String> listaStrings)
    {
        List<Long> listaEnteros = new ArrayList<>();

        for (String id : listaStrings)
        {
            listaEnteros.add(Long.parseLong(id));
        }

        return listaEnteros;
    }

    @POST
    @Path("pendientes/borrar")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public void deleteCertificados(@PathParam("tipoCertificadoId") String tipoCertificadoId,
            MultivaluedMap<String, String> map)
            throws RegistroConHijosException, ModeracionDeCertificadoDenegadaException,
            UnauthorizedUserException, BorradoDeCertificadoDenegadaException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);

        List<String> listaCertificadosIds = map.get("listaCertificadosIds");

        Certificado.deleteCertificadosByListaIds(convierteArrayStringsALongs(listaCertificadosIds),
                connectedUserId);
    }
}
