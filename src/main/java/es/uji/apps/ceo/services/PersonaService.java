package es.uji.apps.ceo.services;

import java.util.Collections;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import es.uji.apps.ceo.model.Persona;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.UIEntity;
import es.uji.commons.sso.AccessManager;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;

@Path("persona")
public class PersonaService extends CoreBaseService
{
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<UIEntity> getPersonas(@QueryParam("dni") String dni) throws UnauthorizedUserException
    {
        Long connectedUserId = AccessManager.getConnectedUserId(request);
        return Collections.singletonList(UIEntity.toUI(Persona.getPersonaByDNI(dni, connectedUserId)));
    }
}
