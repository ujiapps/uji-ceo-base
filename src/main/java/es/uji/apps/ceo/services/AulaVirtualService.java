package es.uji.apps.ceo.services;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.core.InjectParam;
import com.sun.jersey.multipart.FormDataMultiPart;
import es.uji.apps.ceo.SesionNoValidaException;
import es.uji.apps.ceo.dao.CertificadoAulaVirtualDAO;
import es.uji.apps.ceo.model.CertificadoAulaVirtual;
import es.uji.commons.rest.CoreBaseService;
import es.uji.commons.rest.ParamUtils;
import es.uji.commons.rest.ResponseMessage;
import es.uji.commons.sso.exceptions.UnauthorizedUserException;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.Date;

@Component
@Path("aulavirtual")
public class AulaVirtualService extends CoreBaseService
{
    @Value("${uji.aulavirtual.authToken}")
    private String authToken;

    @Value("${uji.aulavirtual.url}")
    private String url;

    protected static CertificadoAulaVirtualDAO certificadoAulaVirtualDAO;

    @InjectParam
    SessionManager sessionManager;

    @Autowired
    protected void setCertificadoAulaVirtualDAO(CertificadoAulaVirtualDAO certificadoAulaVirtualDAO)
    {
        AulaVirtualService.certificadoAulaVirtualDAO = certificadoAulaVirtualDAO;
    }

    @GET
    public ResponseMessage getCursos(@QueryParam("perId") Long perId,
            @QueryParam("session") String session, @QueryParam("connectedUserId") Long connectedUserId)
                    throws UnauthorizedUserException, JSONException, SesionNoValidaException
    {

        ParamUtils.checkNotNull(perId, session, connectedUserId);
        Client client = Client.create();

        sessionManager.checkSession(session, connectedUserId);

        WebResource resource = client.resource(url);

        FormDataMultiPart formDataMultiPart = new FormDataMultiPart();

        formDataMultiPart.field("wstoken", authToken);
        formDataMultiPart.field("wsfunction", "uji_get_teacher_courses_for_certificate");
        formDataMultiPart.field("perid", perId.toString());
        formDataMultiPart.field("moodlewsrestformat", "json");

        String jsonResponse = resource.type(MediaType.MULTIPART_FORM_DATA).post(String.class,
                formDataMultiPart);

        JSONArray jsonCourses = new JSONArray(jsonResponse);

        String cursosString = createCursosStringFromJSON(jsonCourses);

        Long id = insertaCertificcadoAulavirtual(perId, connectedUserId, cursosString);

        return new ResponseMessage(true, id.toString());

    }

    private Long insertaCertificcadoAulavirtual(Long perId, Long connectedUserId,
            String cursosString)
    {
        CertificadoAulaVirtual certificadoAulaVirtual = new CertificadoAulaVirtual();

        certificadoAulaVirtual.setDatos(cursosString);
        certificadoAulaVirtual.setPerIdCertificado(perId);
        certificadoAulaVirtual.setPerIdSolicitante(connectedUserId);
        certificadoAulaVirtual.setFecha(new Date());

        certificadoAulaVirtual = certificadoAulaVirtualDAO.insert(certificadoAulaVirtual);

        return certificadoAulaVirtual.getId();
    }

    private String createCursosStringFromJSON(JSONArray jsonCourses) throws JSONException
    {
        String cursosString = "";

        for (int i = 0; i < jsonCourses.length(); i++)
        {
            JSONObject jsonCourse = jsonCourses.getJSONObject(i);
            cursosString += jsonCourse.get("acadcourse") + "###" + jsonCourse.get("course_fullname")
                    + "@@@";
        }

        return cursosString.substring(0, cursosString.length() - 3);
    }

}
