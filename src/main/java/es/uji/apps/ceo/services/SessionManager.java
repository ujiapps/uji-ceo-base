package es.uji.apps.ceo.services;

import es.uji.apps.ceo.dao.SessionDAO;
import es.uji.apps.ceo.SesionNoValidaException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SessionManager
{
    @Autowired
    SessionDAO sessionDAO;

    public void checkSession(String session, Long connectedUserId)
            throws SesionNoValidaException
    {
        if (!sessionDAO.checkSesionActivaEujier(session, connectedUserId))
        {
            throw new SesionNoValidaException();
        }
    }
}