package es.uji.apps.ceo;

import es.uji.commons.rest.exceptions.CoreBaseException;

@SuppressWarnings("serial")
public class BorradoDeCertificadoDenegadaException extends CoreBaseException
{
    public BorradoDeCertificadoDenegadaException()
    {

        super("No pots borrar els certificats sel·leccionats");
    }
    
    public BorradoDeCertificadoDenegadaException(String message)
    {
        super(message);
    }
}
