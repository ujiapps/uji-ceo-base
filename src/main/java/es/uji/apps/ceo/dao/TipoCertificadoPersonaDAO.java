package es.uji.apps.ceo.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.ceo.model.QTipoCertificadoPersona;
import es.uji.apps.ceo.model.TipoCertificadoPersona;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TipoCertificadoPersonaDAO extends BaseDAODatabaseImpl
{

    public List<TipoCertificadoPersona> getPermisoPorUsuarioYTipoCertificado(Long connectedUserId,
            Long tipoCertificadoId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QTipoCertificadoPersona qTipoCertificadoPersona = QTipoCertificadoPersona.tipoCertificadoPersona;

        query.from(qTipoCertificadoPersona).where(qTipoCertificadoPersona.tipoCertificadoId
                .eq(tipoCertificadoId).and(qTipoCertificadoPersona.perId.eq(connectedUserId)));

        return query.list(qTipoCertificadoPersona);
    }

    public List<TipoCertificadoPersona> getListaTipoCertificadoPersonaByTipoCertificadoId(
            Long tipoCertificadoId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QTipoCertificadoPersona qTipoCertificadoPersona = QTipoCertificadoPersona.tipoCertificadoPersona;

        query.from(qTipoCertificadoPersona)
                .where(qTipoCertificadoPersona.tipoCertificadoId.eq(tipoCertificadoId));

        return query.list(qTipoCertificadoPersona);
    }

    public TipoCertificadoPersona getByUserIdAndTipoCertificadoId(Long connectedUserId,
            Long tipoCertificadoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QTipoCertificadoPersona qTipoCertificadoPersona = QTipoCertificadoPersona.tipoCertificadoPersona;

        query.from(qTipoCertificadoPersona).where(qTipoCertificadoPersona.tipoCertificadoId
                .eq(tipoCertificadoId).and(qTipoCertificadoPersona.perId.eq(connectedUserId)));

        return query.uniqueResult(qTipoCertificadoPersona);
    }
}