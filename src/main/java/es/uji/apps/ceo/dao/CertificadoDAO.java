package es.uji.apps.ceo.dao;

import com.mysema.query.BooleanBuilder;
import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.QTuple;
import es.uji.apps.ceo.model.Certificado;
import es.uji.apps.ceo.model.Persona;
import es.uji.apps.ceo.model.QCertificado;
import es.uji.apps.ceo.model.QPersona;
import es.uji.apps.ceo.model.QTipoCertificadoModeracion;
import es.uji.apps.ceo.model.QTipoCertificadoPersona;
import es.uji.apps.ceo.model.domains.EstadoCertificado;
import es.uji.apps.ceo.model.domains.TipoPermiso;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
public class CertificadoDAO extends BaseDAODatabaseImpl
{
    private static final int DENEGADO = 0;
    private static final int ACEPTADO = 1;

    private JPAQuery construyeQuery(Map<String, Object> queryParams)
    {

        JPAQuery query = new JPAQuery(entityManager);
        BooleanBuilder condicionesWhere = new BooleanBuilder();

        QCertificado qCertificado = QCertificado.certificado;
        QTipoCertificadoPersona qTipoCertificadoPersona = QTipoCertificadoPersona.tipoCertificadoPersona;
        QTipoCertificadoModeracion qTipoCertificadoModeracion = QTipoCertificadoModeracion.tipoCertificadoModeracion;

        if (queryParams.get("connectedUserId") != null)
        {
            Long connectedUserId = (Long) queryParams.get("connectedUserId");
            if (queryParams.get("estado") != null
                    && queryParams.get("estado").equals(EstadoCertificado.PENDIENTE_MODERAR))
            {
                query.from(qCertificado, qTipoCertificadoModeracion);
                condicionesWhere.and(qTipoCertificadoModeracion.tipoCertificadoId
                        .eq(qCertificado.tipoCertificado.id));
                condicionesWhere.and(qTipoCertificadoModeracion.perId.eq(connectedUserId));
            }
            else
            {
                query.from(qCertificado, qTipoCertificadoPersona);
                condicionesWhere.and(qTipoCertificadoPersona.tipoCertificadoId
                        .eq(qCertificado.tipoCertificado.id));
                condicionesWhere.and(qTipoCertificadoPersona.perId.eq(connectedUserId));
            }
        }
        else
        {
            query.from(qCertificado);
        }

        filtraTipoCertificado(queryParams, condicionesWhere, qCertificado);
        filtraAnyo(queryParams, condicionesWhere, qCertificado);
        filtraDescripcionCurso(queryParams, condicionesWhere, qCertificado);
        filtraPendientesModerar(queryParams, condicionesWhere, qCertificado);
        filtraPendientesRevisar(queryParams, condicionesWhere, qCertificado);
        filtraCertificadosPorPermisos(queryParams, condicionesWhere, qCertificado,
                qTipoCertificadoModeracion, qTipoCertificadoPersona);

        if (condicionesWhere.hasValue())
        {
            query.where(condicionesWhere);
        }

        return query;
    }

    private void filtraPendientesRevisar(Map<String, Object> queryParams,
            BooleanBuilder condicionesWhere, QCertificado qCertificado)
    {
        if (queryParams.get("estado") != null
                && queryParams.get("estado").equals(EstadoCertificado.PENDIENTE_REVISAR))
        {
            condicionesWhere.and(qCertificado.fechaRevision.isNull());
        }
    }

    private void filtraCertificadosPorPermisos(Map<String, Object> queryParams,
            BooleanBuilder condicionesWhere, QCertificado qCertificado,
            QTipoCertificadoModeracion qTipoCertificadoModeracion,
            QTipoCertificadoPersona qTipoCertificadoPersona)
    {
        if (queryParams.get("connectedUserId") != null)
        {
            Long connectedUserId = (Long) queryParams.get("connectedUserId");

            if (queryParams.get("estado") != null
                    && queryParams.get("estado").equals(EstadoCertificado.PENDIENTE_MODERAR))
            {
                condicionesWhere.and(qTipoCertificadoModeracion.perId.eq(connectedUserId));
                condicionesWhere.and(qCertificado.tipoCertificado.id
                        .eq(qTipoCertificadoModeracion.tipoCertificadoId));
            }
            else
            {
                condicionesWhere.and(qTipoCertificadoPersona.perId.eq(connectedUserId));
                condicionesWhere.and(qCertificado.tipoCertificado.id
                        .eq(qTipoCertificadoPersona.tipoCertificadoId));
            }
        }

    }

    private void filtraPendientesModerar(Map<String, Object> queryParams,
            BooleanBuilder condicionesWhere, QCertificado qCertificado)
    {
        if (queryParams.get("estado") != null
                && queryParams.get("estado").equals(EstadoCertificado.PENDIENTE_MODERAR))
        {
            condicionesWhere.and(qCertificado.fechaRevision.isNotNull());
            condicionesWhere.and(qCertificado.moderacionAceptada.isNull());
        }
    }

    private void filtraTipoCertificado(Map<String, Object> queryParams,
            BooleanBuilder condicionesWhere, QCertificado qCertificado)
    {
        if (queryParams.get("tipoCertificadoId") != null)
        {
            Long tipoCertificadoId = (Long) queryParams.get("tipoCertificadoId");
            condicionesWhere.and(qCertificado.tipoCertificado.id.eq(tipoCertificadoId));
        }
    }

    private void filtraAnyo(Map<String, Object> queryParams, BooleanBuilder condicionesWhere,
            QCertificado qCertificado)
    {
        if (queryParams.get("anyo") != null)
        {
            Long anyo = (Long) queryParams.get("anyo");
            condicionesWhere.and(qCertificado.fechaInicioCurso.year().eq(anyo.intValue()));
        }
    }

    private void filtraDescripcionCurso(Map<String, Object> queryParams,
            BooleanBuilder condicionesWhere, QCertificado qCertificado)
    {
        if (queryParams.get("descripcionCurso") != null
                && !queryParams.get("descripcionCurso").toString().isEmpty())
        {
            String descripcionCurso = (String) queryParams.get("descripcionCurso");
            condicionesWhere.and(qCertificado.descripcionCursoCA.eq(descripcionCurso));
        }
    }

    public List<Certificado> getCertificados(Map<String, Object> queryParams)
    {
        JPAQuery query = construyeQuery(queryParams);
        QCertificado qCertificado = QCertificado.certificado;

        //return query.list(qCertificado);
        return query.list(QCertificado.create(qCertificado.id,
                qCertificado.dniDestinatario, qCertificado.fechaCertificado,
                qCertificado.fechaCreacion, qCertificado.fechaInicioCurso, qCertificado.fechaRecogida,
                qCertificado.mailDestinatario, qCertificado.nombreDestinatario, qCertificado.fechaModeracion,
                qCertificado.fechaRevision, qCertificado.moderadorPerId, qCertificado.revisorPerId,
                qCertificado.moderacionAceptada,
                qCertificado.moderacionMotivoRechazo, qCertificado.descripcionCursoCA, qCertificado.descripcionCursoES,
                qCertificado.descripcionCursoUK, qCertificado.referenciaFirma, qCertificado.secretarioId, qCertificado.usuario,
                qCertificado.destinatarioId, qCertificado.tipoCertificado
        ));
    }

    public List<Certificado> getCertificadoByUserIdAndId(Long connectedUserId, Long certificadoId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QCertificado qCertificado = QCertificado.certificado;
        QTipoCertificadoPersona qTipoCertificadoPersona = QTipoCertificadoPersona.tipoCertificadoPersona;

        query.from(qCertificado, qTipoCertificadoPersona)
                .where(qTipoCertificadoPersona.tipoCertificadoId.eq(qCertificado.tipoCertificado.id)
                        .and(qTipoCertificadoPersona.perId.eq(connectedUserId))
                        .and(qCertificado.id.eq(certificadoId)));

        return query.list(qCertificado);
    }

    public List<Certificado> getCertificadosByListaIds(List<Long> listaIds)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCertificado qCertificado = QCertificado.certificado;

        query.from(qCertificado).where(qCertificado.id.in(listaIds));
        return query.list(qCertificado);
    }

    public List<Tuple> getDescripcionesCursos()
    {
        JPAQuery query = new JPAQuery(entityManager);

        QCertificado qCertificado = QCertificado.certificado;

        query.from(qCertificado).orderBy(qCertificado.descripcionCursoCA.desc());

        return query.distinct().list(new QTuple(qCertificado.descripcionCursoCA));
    }

    public List<Tuple> getDescripcionesCursosByUserId(Long connectedUserId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCertificado qCertificado = QCertificado.certificado;
        QTipoCertificadoPersona qTipoCertificadoPersona = QTipoCertificadoPersona.tipoCertificadoPersona;

        query.from(qCertificado, qTipoCertificadoPersona)
                .where(qCertificado.tipoCertificado.id.eq(qTipoCertificadoPersona.tipoCertificadoId)
                        .and(qTipoCertificadoPersona.perId.eq(connectedUserId)));

        return query.distinct().list(new QTuple(qCertificado.descripcionCursoCA));
    }

    private Date getYearDate(Long year)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.MONTH, 1);
        calendar.set(Calendar.YEAR, year.intValue());
        return calendar.getTime();
    }

    public List<Integer> getAnyosCursos(Long connectedUserId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QCertificado qCertificado = QCertificado.certificado;
        return query.from(qCertificado).where(qCertificado.fechaInicioCurso.isNotNull()).distinct().list(qCertificado.fechaInicioCurso.year());
    }

    @Transactional
    public void delete(Long certificadoId)
    {
        QCertificado qCertificado = QCertificado.certificado;
        JPADeleteClause deleteClause = new JPADeleteClause(entityManager, qCertificado);
        deleteClause.where(qCertificado.id.eq(certificadoId)).execute();
    }

    @Transactional
    public void deleteCertificadosByListaIds(List<Long> listaCertificadosIds)
    {
        QCertificado qCertificado = QCertificado.certificado;
        JPADeleteClause deleteClause = new JPADeleteClause(entityManager, qCertificado);

        deleteClause.where(qCertificado.id.in(listaCertificadosIds)
                .and(qCertificado.moderacionAceptada.isNull())).execute();
    }

    public List<String> getCursos(Map<String, Object> queryParams)
    {
        JPAQuery query = construyeQuery(queryParams);
        QCertificado qCertificado = QCertificado.certificado;
        return query.distinct().orderBy(qCertificado.descripcionCursoCA.trim().asc()).list(qCertificado.descripcionCursoCA.trim());
    }
}
