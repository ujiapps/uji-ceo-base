package es.uji.apps.ceo.dao;

import java.util.ArrayList;
import java.util.List;

import es.uji.commons.rest.json.lookup.LookupItem;
import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.ceo.model.QContratacion;
import es.uji.apps.ceo.model.QUbicacionLogica;
import es.uji.apps.ceo.model.UbicacionLogica;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class UbicacionLogicaDAODatabaseImpl extends BaseDAODatabaseImpl implements UbicacionLogicaDAO
{

    @Override
    public List<LookupItem> search(String cadena)
    {
        JPAQuery query = new JPAQuery(entityManager);
        QUbicacionLogica ubicacionLogica = QUbicacionLogica.ubicacionLogica;
        List<LookupItem> result = new ArrayList<LookupItem>();

        query.from(ubicacionLogica).where(
                ubicacionLogica.nombre.lower().like("%" + cadena.toLowerCase() + "%"));

        for (UbicacionLogica ubicacionlogicaEncontrada : query.list(ubicacionLogica))
        {
            LookupItem lookupItem = new LookupItem();

            lookupItem.setId(String.valueOf(ubicacionlogicaEncontrada.getId()));
            lookupItem.setNombre(ubicacionlogicaEncontrada.getNombre());

            result.add(lookupItem);
        }

        return result;
    }

    public UbicacionLogica getUbicacionModeracion(Long moderadorPerId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QUbicacionLogica qUbicacionLogica = QUbicacionLogica.ubicacionLogica;
        QContratacion qContratacion = QContratacion.contratacion;

        query.from(qUbicacionLogica).join(qUbicacionLogica.contrataciones, qContratacion)
                .where(qContratacion.personaId.eq(moderadorPerId));

        return query.uniqueResult(qUbicacionLogica);
    }
}
