package es.uji.apps.ceo.dao;

import com.mysema.query.BooleanBuilder;
import com.mysema.query.Tuple;
import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;
import com.mysema.query.types.QTuple;
import com.mysema.query.types.expr.ArrayExpression;
import es.uji.apps.ceo.model.QCertificado;
import es.uji.apps.ceo.model.QPermiso;
import es.uji.apps.ceo.model.QTipoCertificado;
import es.uji.apps.ceo.model.QTipoCertificadoModeracion;
import es.uji.apps.ceo.model.QTipoCertificadoPersona;
import es.uji.apps.ceo.model.TipoCertificado;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.db.LookupDAO;
import es.uji.commons.rest.json.lookup.LookupItem;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository("tipoCertificadoDAO")
public class TipoCertificadoDAO extends BaseDAODatabaseImpl
{
    public List<Tuple> getTiposCertificado(Long userIdAFiltrar)
    {
        QTipoCertificado qTipoCertificado = QTipoCertificado.tipoCertificado;

        JPAQuery query = componerQuery(userIdAFiltrar);
        BooleanBuilder condiciones = filtrarQueryPorPersona(userIdAFiltrar);

        query.where(condiciones).orderBy(qTipoCertificado.descripcionCa.asc());

        return query.list(new QTuple(qTipoCertificado.id,
                qTipoCertificado.descripcionCa,
                 qTipoCertificado.descripcionEs, qTipoCertificado.descripcionUk
                , qTipoCertificado.emisionManual));
    }

    private BooleanBuilder filtrarQueryPorPersona(Long userIdAFiltrar)
    {
        BooleanBuilder condiciones = new BooleanBuilder();
        QTipoCertificadoPersona qTipoCertificadoPersona = QTipoCertificadoPersona.tipoCertificadoPersona;
        if (userIdAFiltrar != null)
        {
            condiciones.and(qTipoCertificadoPersona.perId.eq(userIdAFiltrar));
        }

        return condiciones;
    }

    private JPAQuery componerQuery(Long userIdAFiltrar)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QTipoCertificado qTipoCertificado = QTipoCertificado.tipoCertificado;
        QTipoCertificadoPersona qTipoCertificadoPersona = QTipoCertificadoPersona.tipoCertificadoPersona;

        if (userIdAFiltrar == null)
        {
            query.from(qTipoCertificado);
        }
        else
        {
            query.from(qTipoCertificado, qTipoCertificadoPersona)
                    .where(qTipoCertificado.id.eq(qTipoCertificadoPersona.tipoCertificadoId));
        }

        return query;
    }

    public List<Tuple> getTiposCertificadoConPermiso(Long userId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QTipoCertificado qTipoCertificado = QTipoCertificado.tipoCertificado;
        QTipoCertificadoPersona qTipoCertificadoPersona = QTipoCertificadoPersona.tipoCertificadoPersona;

        query.from(qTipoCertificado, qTipoCertificadoPersona)
                .where(qTipoCertificadoPersona.perId.eq(userId)
                        .and(qTipoCertificadoPersona.tipoCertificadoId.eq(qTipoCertificado.id)))
                .orderBy(qTipoCertificado.descripcionCa.asc());
        ;

        return query.list(new QTuple(qTipoCertificado.id, qTipoCertificado.descripcionCa,
                qTipoCertificado.descripcionEs, qTipoCertificado.descripcionUk,
                qTipoCertificado.plantillaCa, qTipoCertificado.plantillaEs,
                qTipoCertificado.plantillaUk));
    }

    public List<TipoCertificado> getTiposCertificadoModeracionConPermiso(Long userId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        List<TipoCertificado> resultadoListaTiposCertificados = new ArrayList<TipoCertificado>();

        QTipoCertificado qTiposCertificado = QTipoCertificado.tipoCertificado;
        QCertificado qCertificado = QCertificado.certificado;
        QTipoCertificadoModeracion qTipoCertificadoModeracion = QTipoCertificadoModeracion.tipoCertificadoModeracion;

        query.from(qTiposCertificado, qTipoCertificadoModeracion)
                .innerJoin(qTiposCertificado.certificadosTiposCertificados, qCertificado)
                .where(qTiposCertificado.id.eq(qTipoCertificadoModeracion.tipoCertificadoId)
                        .and(qCertificado.moderacionAceptada.isNull())
                        .and(qCertificado.fechaRevision.isNotNull())
                        .and(qTipoCertificadoModeracion.perId.eq(userId)));

        List<Tuple> listaTipoCertificados = query.distinct().list(qTiposCertificado.id,
                qTiposCertificado.descripcionCa);

        for (Tuple elementoAux : listaTipoCertificados)
        {
            TipoCertificado tipoCertificado = new TipoCertificado();
            tipoCertificado.setId(elementoAux.get(qTiposCertificado.id));
            tipoCertificado.setDescripcionCa(elementoAux.get(qTiposCertificado.descripcionCa));
            resultadoListaTiposCertificados.add(tipoCertificado);
        }

        return resultadoListaTiposCertificados;
    }

    public List<TipoCertificado> getTiposCertificadoRevisionConPermiso(Long userId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        List<TipoCertificado> resultadoListaTiposCertificados = new ArrayList<TipoCertificado>();

        QTipoCertificado qTiposCertificado = QTipoCertificado.tipoCertificado;
        QCertificado qCertificado = QCertificado.certificado;
        QTipoCertificadoPersona qTipoCertificadoPersona = QTipoCertificadoPersona.tipoCertificadoPersona;

        query.from(qTiposCertificado, qTipoCertificadoPersona)
                .innerJoin(qTiposCertificado.certificadosTiposCertificados, qCertificado)
                .where(qTiposCertificado.id.eq(qTipoCertificadoPersona.tipoCertificadoId)
                        .and(qCertificado.fechaRevision.isNull())
                        .and(qTipoCertificadoPersona.perId.eq(userId)));

        List<Tuple> listaTipoCertificados = query.distinct().list(qTiposCertificado.id,
                qTiposCertificado.descripcionCa);

        for (Tuple elementoAux : listaTipoCertificados)
        {
            TipoCertificado tipoCertificado = new TipoCertificado();
            tipoCertificado.setId(elementoAux.get(qTiposCertificado.id));
            tipoCertificado.setDescripcionCa(elementoAux.get(qTiposCertificado.descripcionCa));
            resultadoListaTiposCertificados.add(tipoCertificado);
        }

        return resultadoListaTiposCertificados;
    }

    public Long getNumPermisosByTipoCertificado(Long tipoCertificadoId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QPermiso permiso = QPermiso.permiso;

        query.from(permiso).where(permiso.tipoCertificado.id.eq(tipoCertificadoId));

        return (long) query.list(permiso).size();
    }

    public Long getNumTextosByTipoCertificado(Long tipoCertificadoId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QCertificado qCertificado = QCertificado.certificado;
        query.from(qCertificado).where(qCertificado.tipoCertificado.id.eq(tipoCertificadoId));
        return (Long) new Long(query.list(qCertificado).size());
    }

    @Transactional
    public void delete(Long tipoCertificadoId)
    {
        QTipoCertificado qTipoCertificado = QTipoCertificado.tipoCertificado;
        JPADeleteClause deleteClause = new JPADeleteClause(entityManager, qTipoCertificado);
        deleteClause.where(qTipoCertificado.id.eq(tipoCertificadoId)).execute();
    }

    public List<TipoCertificado> getCertificadoByIdAndUserId(Long tipoCertificadoId,
            Long connectedUserId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QTipoCertificado qTiposCertificado = QTipoCertificado.tipoCertificado;
        QTipoCertificadoPersona qTipoCertificadoPersona = QTipoCertificadoPersona.tipoCertificadoPersona;

        query.from(qTiposCertificado, qTipoCertificadoPersona)
                .where(qTiposCertificado.id.eq(tipoCertificadoId).and(qTipoCertificadoPersona.perId
                        .eq(connectedUserId)
                        .and(qTipoCertificadoPersona.tipoCertificadoId.eq(qTiposCertificado.id))));

        return query.list(qTiposCertificado);
    }

    public List<TipoCertificado> getTiposCertificadoModeracion()
    {
        JPAQuery query = new JPAQuery(entityManager);
        List<TipoCertificado> resultadoListaTiposCertificados = new ArrayList<TipoCertificado>();

        QTipoCertificado qTiposCertificado = QTipoCertificado.tipoCertificado;
        QCertificado qCertificado = QCertificado.certificado;
        QTipoCertificadoModeracion qTipoCertificadoModeracion = QTipoCertificadoModeracion.tipoCertificadoModeracion;

        query.from(qTiposCertificado, qTipoCertificadoModeracion)
                .innerJoin(qTiposCertificado.certificadosTiposCertificados, qCertificado)
                .where(qTiposCertificado.id.eq(qTipoCertificadoModeracion.tipoCertificadoId)
                        .and(qCertificado.moderacionAceptada.isNull())
                        .and(qCertificado.fechaRevision.isNotNull()));

        List<Tuple> listaTipoCertificados = query.distinct().list(qTiposCertificado.id,
                qTiposCertificado.descripcionCa);

        for (Tuple elementoAux : listaTipoCertificados)
        {
            TipoCertificado tipoCertificado = new TipoCertificado();
            tipoCertificado.setId(elementoAux.get(qTiposCertificado.id));
            tipoCertificado.setDescripcionCa(elementoAux.get(qTiposCertificado.descripcionCa));
            resultadoListaTiposCertificados.add(tipoCertificado);
        }

        return resultadoListaTiposCertificados;
    }

    public List<TipoCertificado> getTiposCertificadoRevision()
    {
        JPAQuery query = new JPAQuery(entityManager);

        QTipoCertificado qTiposCertificado = QTipoCertificado.tipoCertificado;
        QCertificado qCertificado = QCertificado.certificado;

        query.from(qTiposCertificado)
                .join(qTiposCertificado.certificadosTiposCertificados, qCertificado)
                .where(qCertificado.fechaRevision.isNull());

        return query.list(qTiposCertificado);

    }

    public List<Tuple> getTiposCertificadoEmisionManual(Long userIdAFiltrar) {
        QTipoCertificado qTipoCertificado = QTipoCertificado.tipoCertificado;

        JPAQuery query = componerQuery(userIdAFiltrar);
        BooleanBuilder condiciones = filtrarQueryPorPersona(userIdAFiltrar);
        condiciones.and(qTipoCertificado.emisionManual.eq(new Long(1)));

        query.where(condiciones).orderBy(qTipoCertificado.descripcionCa.asc());

        return query.list(new QTuple(qTipoCertificado.id, qTipoCertificado.descripcionCa,
                qTipoCertificado.descripcionEs, qTipoCertificado.descripcionUk,
                qTipoCertificado.plantillaCa, qTipoCertificado.plantillaEs,
                qTipoCertificado.plantillaUk, qTipoCertificado.emisionManual));
    }
}