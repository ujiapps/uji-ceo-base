package es.uji.apps.ceo.dao;

import java.util.List;

import es.uji.apps.ceo.model.Persona;
import es.uji.commons.db.BaseDAO;
import es.uji.commons.db.LookupDAO;
import es.uji.commons.rest.json.lookup.LookupItem;

public interface PersonaDAO extends BaseDAO, LookupDAO<LookupItem> {

    List<LookupItem> search(String cadena);
    List<Persona> getPesonaByDNI(String dni);

    Persona getPersonaById(Long destinatarioId);
}