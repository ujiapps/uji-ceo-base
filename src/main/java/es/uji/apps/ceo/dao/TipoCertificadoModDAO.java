package es.uji.apps.ceo.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.ceo.model.QTipoCertificadoModeracion;
import es.uji.apps.ceo.model.TipoCertificadoModeracion;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class TipoCertificadoModDAO extends BaseDAODatabaseImpl
{

    public List<TipoCertificadoModeracion> getListaTipoCertificadoModeracionByTipoCertificadoId(
            Long tipoCertificadoId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QTipoCertificadoModeracion qTipoCertificadoModeracion = QTipoCertificadoModeracion.tipoCertificadoModeracion;

        query.from(qTipoCertificadoModeracion).where(
                qTipoCertificadoModeracion.tipoCertificadoId.eq(tipoCertificadoId));

        return query.list(qTipoCertificadoModeracion);
    }

    public List<TipoCertificadoModeracion> getPermisoPorUsuarioYTipoCertificado(
            Long connectedUserId, Long tipoCertificadoId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QTipoCertificadoModeracion qTipoCertificadoModeracion = QTipoCertificadoModeracion.tipoCertificadoModeracion;

        query.from(qTipoCertificadoModeracion).where(
                qTipoCertificadoModeracion.tipoCertificadoId.eq(tipoCertificadoId).and(
                        qTipoCertificadoModeracion.perId.eq(connectedUserId)));

        return query.list(qTipoCertificadoModeracion);
    }

}
