package es.uji.apps.ceo.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.ceo.model.QSessionActivaEujier;
import es.uji.commons.db.BaseDAODatabaseImpl;
import org.springframework.stereotype.Repository;

@Repository
public class SessionDAO extends BaseDAODatabaseImpl
{
    private QSessionActivaEujier sessionActivaEujier = QSessionActivaEujier.sessionActivaEujier;

    public Boolean checkSesionActivaEujier(String session, Long connectedUserId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        query.from(sessionActivaEujier)
                .where(sessionActivaEujier.id.eq(session)
                        .and(sessionActivaEujier.personaId.eq(connectedUserId)));

        if (query.list(sessionActivaEujier).size() > 0)
        {
            return true;
        }

        return false;
    }
}
