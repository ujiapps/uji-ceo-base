package es.uji.apps.ceo.dao;

public enum ErroresAPI
{
    ERROR_REGISTRANDO_CERTIFICADO(20001, "No es pot registrar el certificat"), ERROR_CERTIFICADO_YA_EXISTE(
            20002, "El certificat ja existeix"), ERROR_PERMISOS_ACCESO(20003,
            "No tens accés a aquesta operació"), ERROR_CERTIFICADO_INEXISTENTE(20004,
            "El certificat no existeix"), ERROR_VALOR_RETORNO(6503, "El certificat no s'ha registrat correctament");

    private int userExceptionCode;
    private String exceptionMessage;

    private ErroresAPI(int userExceptionCode, String exceptionMessage)
    {
        this.userExceptionCode = userExceptionCode;
        this.exceptionMessage = exceptionMessage;
    }

    public int getUserExceptionCode()
    {
        return userExceptionCode;
    }

    public String getExceptionMessage()
    {
        return exceptionMessage;
    }

    public static ErroresAPI getErrorById(int userExceptionCode)
    {
        for (ErroresAPI error : ErroresAPI.values())
        {
            if (error.getUserExceptionCode() == userExceptionCode)
            {
                return error;
            }
        }

        return null;
    }
}
