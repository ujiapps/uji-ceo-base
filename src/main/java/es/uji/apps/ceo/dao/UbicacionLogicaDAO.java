package es.uji.apps.ceo.dao;

import java.util.List;

import es.uji.apps.ceo.model.UbicacionLogica;
import es.uji.commons.db.BaseDAO;
import es.uji.commons.db.LookupDAO;
import es.uji.commons.rest.json.lookup.LookupItem;

public interface UbicacionLogicaDAO extends BaseDAO, LookupDAO<LookupItem> {

    List<LookupItem> search(String cadena);
    UbicacionLogica getUbicacionModeracion(Long moderadorPerId);
}