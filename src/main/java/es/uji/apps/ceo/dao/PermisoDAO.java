package es.uji.apps.ceo.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mysema.query.jpa.impl.JPADeleteClause;
import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.ceo.model.*;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class PermisoDAO extends BaseDAODatabaseImpl
{

    public List<Persona> getPersonasConPermisoPorTipoCertificadoId(Long tipoCertificadoId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QPersona persona = QPersona.persona;
        QPermiso permiso = QPermiso.permiso;

        query.from(persona).join(persona.permisosUsuarios, permiso)
                .where(permiso.tipoCertificado.id.eq(tipoCertificadoId));

        return query.list(persona);
    }

    @Transactional
    public void deletePersonaConPermisoPorTipoCertificadoId(Long personaId, Long tipoCertificadoId)
    {
        QPermiso permiso = QPermiso.permiso;

        JPADeleteClause deleteClause = new JPADeleteClause(entityManager, permiso);

        deleteClause.where(permiso.persona.id.eq(personaId).and(permiso.tipoCertificado.id.eq(tipoCertificadoId))).execute();
    }

    public List<UbicacionLogica> getUbicacionesConPermisoPorTipoCertificadoId(Long tipoCertificadoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        
        QUbicacionLogica ubicacionLogica = QUbicacionLogica.ubicacionLogica;
        QPermiso permiso = QPermiso.permiso;
        
        query.from(ubicacionLogica).join(ubicacionLogica.permisosUbicacionesLogicas, permiso)
                .where(permiso.tipoCertificado.id.eq(tipoCertificadoId));
        
        return query.list(ubicacionLogica);
    }

    @Transactional
    public void deleteUbicacionLogicaConPermisoPorTipoCertificadoId(Long ubicacionLogicaId,
            Long tipoCertificadoId)
    {
        QPermiso permiso = QPermiso.permiso;
        
        JPADeleteClause deleteClause = new JPADeleteClause(entityManager, permiso);
        
        deleteClause.where(permiso.ubicacionLogica.id.eq(ubicacionLogicaId)).execute();
    }

}