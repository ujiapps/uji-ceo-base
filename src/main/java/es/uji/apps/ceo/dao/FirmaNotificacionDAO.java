package es.uji.apps.ceo.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mysema.query.jpa.impl.JPAQuery;

import es.uji.apps.ceo.model.FirmaNotificacion;
import es.uji.apps.ceo.model.QFirmaNotificacion;
import es.uji.commons.db.BaseDAODatabaseImpl;

@Repository
public class FirmaNotificacionDAO extends BaseDAODatabaseImpl
{
    public List<FirmaNotificacion> getFirmaNotificacionesByCertificadoId(String certificadoId)
    {
        JPAQuery query = new JPAQuery(entityManager);
        
        QFirmaNotificacion qFirma = QFirmaNotificacion.firmaNotificacion;

        query.from(qFirma).where(qFirma.referenciaInterna.eq(certificadoId)).limit(1);

        return query.list(qFirma);
    }

    public List<FirmaNotificacion> getFirmaNotificacionSinFechaEntrada()
    {
        JPAQuery query = new JPAQuery(entityManager);

        QFirmaNotificacion qFirma = QFirmaNotificacion.firmaNotificacion;

        query.from(qFirma).where(qFirma.fechaEntradaNotificacion.isNull());

        return query.list(qFirma);
    }
}