package es.uji.apps.ceo.dao;

import com.mysema.query.jpa.impl.JPAQuery;
import es.uji.apps.ceo.model.Persona;
import es.uji.apps.ceo.model.QPersona;
import es.uji.commons.db.BaseDAODatabaseImpl;
import es.uji.commons.rest.StringUtils;
import es.uji.commons.rest.json.lookup.LookupItem;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class PersonaDAODatabaseImpl extends BaseDAODatabaseImpl implements PersonaDAO
{

    public List<LookupItem> search(String cadena)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QPersona persona = QPersona.persona;
        String cadenaLimpia = StringUtils.limpiaAcentos(cadena);
        Long queryStringLong = 0L;

        List<LookupItem> result = new ArrayList<LookupItem>();

        if (cadena != null && !cadena.isEmpty())
        {
            query.from(persona).where(
                    persona.nombreBusqueda.lower().like("%" + cadenaLimpia.toLowerCase() + "%"));

            List<Persona> listaPersonas = query.list(persona);

            try
            {
                queryStringLong = Long.parseLong(cadena);
            }
            catch (Exception e)
            {
            }

            for (Persona personaEncontrada : listaPersonas)
            {
                LookupItem lookupItem = new LookupItem();
                lookupItem.setId(String.valueOf(personaEncontrada.getId()));
                lookupItem.setNombre(personaEncontrada.getNombreCompleto());

                lookupItem.addExtraParam("DNI", personaEncontrada.getIdentificacion().trim());

                if (personaEncontrada.getMail() != null)
                {
                    lookupItem.addExtraParam("EMail", personaEncontrada.getMail().trim());
                }

                result.add(lookupItem);
            }
        }

        return result;
    }

    public List<Persona> getPesonaByDNI(String dni)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QPersona persona = QPersona.persona;

        query.from(persona).where(persona.identificacion.lower().eq(dni.toLowerCase()));

        List<Persona> resultado = query.list(persona);

        return resultado;
    }

    @Override
    public Persona getPersonaById(Long destinatarioId)
    {
        JPAQuery query = new JPAQuery(entityManager);

        QPersona persona = QPersona.persona;

        query.from(persona).where(persona.id.eq(destinatarioId));

        List<Persona> resultado = query.list(persona);

        if (resultado.size() > 0)
        {
            return resultado.get(0);
        } else {
            return null;
        }

    }
}
