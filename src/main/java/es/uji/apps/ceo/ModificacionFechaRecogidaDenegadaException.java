package es.uji.apps.ceo;

import es.uji.commons.rest.exceptions.CoreBaseException;

@SuppressWarnings("serial")
public class ModificacionFechaRecogidaDenegadaException extends CoreBaseException
{
    public ModificacionFechaRecogidaDenegadaException()
    {
        super("No pots canviar la data de recollida perque el certificat encara no ha sigut moderat");
    }
    
    public ModificacionFechaRecogidaDenegadaException(String message)
    {
        super(message);
    }
}