package es.uji.apps.ceo;

import es.uji.commons.rest.exceptions.CoreBaseException;

@SuppressWarnings("serial")
public class ErrorInsercionCertificadoException extends CoreBaseException
{
    public ErrorInsercionCertificadoException()
    {
        super("No ha sigut possible enregistrar el certificat.");
    }
    
    public ErrorInsercionCertificadoException(String message)
    {
        super(message);
    }
}