package es.uji.apps.ceo;

import es.uji.commons.rest.exceptions.CoreBaseException;

@SuppressWarnings("serial")
public class GeneralCEOException extends CoreBaseException
{
    public GeneralCEOException()
    {
        super("S'ha produït un error en l'operació");
    }
    
    public GeneralCEOException(String message)
    {
        super(message);
    }
}