package es.uji.apps.ceo;

import es.uji.commons.rest.exceptions.CoreBaseException;

@SuppressWarnings("serial")
public class IdiomaNoValidoException extends CoreBaseException
{
    public IdiomaNoValidoException()
    {
        super("L'idioma introduït no és correcte.");
    }
    
    public IdiomaNoValidoException(String message)
    {
        super(message);
    }
}