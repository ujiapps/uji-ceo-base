package es.uji.apps.ceo;

import es.uji.commons.rest.exceptions.CoreBaseException;

@SuppressWarnings("serial")
public class RegistroSinCampoObligatorio extends CoreBaseException
{
    public RegistroSinCampoObligatorio()
    {
        super("La descripció en català és obligatòria");
    }
    
    public RegistroSinCampoObligatorio(String message)
    {
        super(message);
    }
}