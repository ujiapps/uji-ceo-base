package es.uji.apps.ceo;

public class SesionNoValidaException extends Throwable
{
    public SesionNoValidaException()
    {
        super("La sesión no es válida");
    }

    public SesionNoValidaException(String message)
    {
        super(message);
    }
}