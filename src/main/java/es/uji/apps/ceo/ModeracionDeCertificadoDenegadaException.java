package es.uji.apps.ceo;

import es.uji.commons.rest.exceptions.CoreBaseException;

@SuppressWarnings("serial")
public class ModeracionDeCertificadoDenegadaException extends CoreBaseException
{
    public ModeracionDeCertificadoDenegadaException()
    {
        super("No pots moderar el certificat sel·leccionat");
    }
    
    public ModeracionDeCertificadoDenegadaException(String message)
    {
        super(message);
    }
}