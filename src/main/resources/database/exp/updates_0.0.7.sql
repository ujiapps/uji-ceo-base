DROP VIEW UJI_SEU.SEU_EXT_PER_PERSONAS;

/* Formatted on 04/12/2012 12:37:22 (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW UJI_SEU.SEU_EXT_PER_PERSONAS
(
   ID,
   IDENTIFICACION,
   NOMBRE,
   MAIL
)
AS
   SELECT   DISTINCT
            p.id,
            p.identificacion,
            p.nombre || ' ' || p.apellido1 || ' ' || p.apellido2 nombre,
            busca_cuenta (p.id) mail
     FROM   per_personas p, grh_exp_personal ep, grh_contrataciones c
    WHERE       p.id = ep.per_id
            AND ep.n_expediente = c.nreg_exp
            AND c.cper_act_id = 'PAS'
            AND TRUNC (SYSDATE) BETWEEN c.fecha_inicio
                                    AND  NVL (fecha_fin, SYSDATE);
                                    
                                    

                                    
DROP VIEW UJI_SEU.SEU_EXT_ULOGICAS;

/* Formatted on 04/12/2012 12:38:21 (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW UJI_SEU.SEU_EXT_ULOGICAS (ID, NOMBRE, MAIL)
AS
   SELECT   id, nombre, mail
     FROM   est_ubic_estructurales
    WHERE   tuest_id = 'SE' AND status = 'A';



    
DROP VIEW UJI_SEU.SEU_VW_TIPO_CERTIFICADO_MOD;

/* Formatted on 04/12/2012 12:38:29 (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW UJI_SEU.SEU_VW_TIPO_CERTIFICADO_MOD
(
   UBICACION_ID,
   PER_ID,
   TIPO_CERTIFICADO_ID
)
AS
     SELECT   UP.uest_id ubicacion_id, ep.per_id, pd.tipo_certificado_id
       FROM   grh_grh.grh_ulogica_plaza UP,
              grh_grh.grh_exp_personal ep,
              seu_perm_doc pd
      WHERE   UP.n_exp = ep.n_expediente AND UP.uest_id = pd.ulogica_id
   ORDER BY   1;
   
   
   
DROP VIEW UJI_SEU.SEU_VW_TIPOCERTIFICADO_PER;

/* Formatted on 04/12/2012 12:38:54 (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW UJI_SEU.SEU_VW_TIPOCERTIFICADO_PER
(
   TIPO_CERTIFICADO_ID,
   PER_ID
)
AS
   SELECT   DISTINCT tipo_certificado_id, ep.per_id per_id
     FROM   seu_perm_doc pd,
            grh_plazas p,
            grh_plazas_contrataciones pc,
            grh_contrataciones c,
            grh_exp_personal ep
    WHERE       pd.ulogica_id = p.uest_id
            AND p.id = pc.plz_id
            AND pc.ctnes_id = c.id
            AND c.nreg_exp = ep.n_expediente
            AND c.cper_act_id = 'PAS'
            AND TRUNC (SYSDATE) BETWEEN c.fecha_inicio
                                    AND  NVL (fecha_fin, SYSDATE)
   UNION
   SELECT   tipo_certificado_id, per_id
     FROM   seu_perm_doc
    WHERE   ulogica_id IS NULL
   ORDER BY   1, 2;

   