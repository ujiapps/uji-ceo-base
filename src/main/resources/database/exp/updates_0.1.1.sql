CREATE OR REPLACE package UJI_SEU.seu_certificados_api as

procedure add_certificado
(v_per_id_usuario in number
, v_per_id_destinatario in number
, v_mail_destinatario in varchar2
, v_dni_destinatario in varchar2
, v_nombre_destinatario in varchar2
, v_texto_certificado_ca in varchar2
, v_texto_certificado_es in varchar2
, v_texto_certificado_uk in varchar2
, v_fecha_creacion in date
, v_fecha_certificado in date
, v_tipo_certificado_id in number
, v_fecha_recogida in date
, v_descripcion_curso in varchar2
, v_texto_anverso in varchar2
, v_error out varchar2)
;

end seu_certificados_api;
/



CREATE OR REPLACE package body UJI_SEU.seu_certificados_api is

procedure add_certificado
(v_per_id_usuario in number
, v_per_id_destinatario in number
, v_mail_destinatario in varchar2
, v_dni_destinatario in varchar2
, v_nombre_destinatario in varchar2
, v_texto_certificado_ca in varchar2
, v_texto_certificado_es in varchar2
, v_texto_certificado_uk in varchar2
, v_fecha_creacion in date
, v_fecha_certificado in date
, v_tipo_certificado_id in number
, v_fecha_recogida in date
, v_descripcion_curso in varchar2
, v_texto_anverso in varchar2
, v_id_certificado out number
, v_error out varchar2)

is

begin
declare
 v_ya_existe_cert number;

begin
   
 select count(*) into v_ya_existe_cert
  from uji_seu.seu_certificados cer
  where cer.tipo_certificado_id = v_tipo_certificado_id 
  and cer.descripcion_curso = v_descripcion_curso
  and cer.destinatario_per_id = v_per_id_destinatario
  and cer.moderacion_aceptado <> '0';
 
  if v_ya_existe_cert = 0 then 
   select hibernate_sequence.nextval
   into v_id_certificado
   from dual;
  
   begin
    insert into
    uji_seu.seu_certificados
    (id
    , usuario_id
    , destinatario_per_id
    , mail_destinatario
    , dni_destinatario
    , nombre_destinatario
    , tipo_certificado_id
    , fecha_creacion
    , fecha_certificado
    , texto_ca
    , fecha_recogida
    , descripcion_curso
    , texto_anverso)
    values
    (v_id_certificado
    , v_per_id_usuario
    , v_per_id_destinatario
    , v_mail_destinatario
    , v_dni_destinatario
    , v_nombre_destinatario
    , v_tipo_certificado_id
    , v_fecha_creacion
    , v_fecha_certificado
    , v_texto_certificado_ca
    , v_fecha_recogida
    , v_descripcion_curso
    , v_texto_anverso);
    
   commit;
   exception
    when others then
     v_error:= 'No ha sigut possible enregistrar el certificat per a l''usuari '||v_nombre_destinatario||'.';
   end;
 else
  v_error:= 'Ja existeix un certificat d''aquestes característiques emés per a l''usuari '||v_nombre_destinatario||'.';
 end if;
end;

end add_certificado;

end seu_certificados_api;
