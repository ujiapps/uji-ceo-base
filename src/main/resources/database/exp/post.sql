---- Creación de usuarios 
drop table uji_seu.seu_ext_ulogicas;

create view seu_ext_ulogicas as
select id,nombre,mail from est_ulogicas
and tuest_id in ('VI','SE');

DROP TABLE UJI_SEU.SEU_EXT_PER_PERSONAS;

CREATE VIEW UJI_SEU.SEU_EXT_PER_PERSONAS AS
SELECT P.ID,P.IDENTIFICACION,P.NOMBRE,CUE.MAIL
FROM PER_VW_PERSONAS P, PER_VW_CUENTAS CUE
WHERE ID BETWEEN 7000 AND 10000
AND ID = CUE.CUE_PSV_PER_ID(+);


grant select on gri_per.per_rf_firmas to uji_seu

grant select on gri_per.per_rf_tipos_firmas to uji_seu

drop table uji_seu.seu_vw_firmas_notificaciones

create view  uji_seu.seu_vw_firmas_notificaciones as
select fir.id,fir.per_id,fecha,xml,per_id_solicitante,fir.nombre,referencia,ref_interna,contenido,(select fecha_firma from per_rf_firmas_per f where F.FIR_ID = fir.id) fecha_firma,
       NOTI.FECHA_ENTRADA F_ENTRADA_NOT, NOTI.CONTENIDO_TXT CONTENIDO_NOT,NOTI.FECHA_ACEPTA_RECHAZA, NOTI.MOTIVO_RECHAZO, NOTI.ESTADO
from gri_per.per_rf_firmas fir , gri_per.per_rf_tipos_firmas tip
where fir.tip_id = tip.id 
and   tip.id  = 12 and
      fir.ref_interna = referencia_int(+)



create view uji_seu.seu_vw_firmas_notificaciones as
select null id, null per_id_emisor, null fecha_emision, null
      per_id_solicitante, null nombre_solicitante, null referencia, null
      ref_interna, null contenido, null fecha_firma, null
      f_entrada_notificacion, null contenido_notificacion, null
      fecha_acepta_rechaza, null motivo_rechazo, null estado, null xml from
      dual