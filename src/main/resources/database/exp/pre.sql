---- Creación de usuarios 
create user UJI_seu identified by scrum2010 quota unlimited on datos; 
grant connect, resource, create view to UJI_seu;

create user UJI_seu_usr identified by scrum2010;
grant connect to UJI_seu_usr;

-- Secuencia para Hibernate 
create sequence UJI_seu.hibernate_sequence;


GRANT SELECT ON UJI_APA.APA_VW_PERSONAS_ITEMS TO UJI_seu;
GRANT SELECT ON UJI_APA.APA_VW_PERSONAS_ITEMS TO UJI_seu_usr;



grant select on UJI_APA.APA_APLICACIONES_EXTRAS to uji_seu;                                                                                                                                                                                                                 
grant select on UJI_APA.APA_ROLES to uji_seu;
grant select on UJI_APA.APA_APLICACIONES to uji_seu;
grant select on UJI_APA.APA_PERSONAS_CUENTAS to uji_seu;
grant select on UJI_APA.APA_APLICACIONES_MAPEOS to uji_seu;
grant select, insert  on APA_SESIONES to uji_seu;
