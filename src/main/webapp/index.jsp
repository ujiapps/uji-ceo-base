<%@page contentType="text/html; charset=utf-8"%>
<%@page import="es.uji.commons.sso.User"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Certificats Oficials</title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta content="text/html; charset=utf-8">

<link rel="stylesheet" type="text/css"
	href="//static.uji.es/js/extjs/ext-3.4.0/resources/css/ext-all.css" />
<link rel="stylesheet"
	href="//static.uji.es/js/extjs/ext-3.4.0/examples/ux/css/RowEditor.css" />
<link rel="stylesheet" type="text/css"
	href="//static.uji.es/js/extjs/ext-3.4.0/examples/ux/fileuploadfield/css/fileuploadfield.css" />
<link rel="stylesheet" type="text/css"
	href="//static.uji.es/js/extjs/uji-commons-extjs/css/icons.css" />
<link rel="stylesheet" type="text/css"
	href="css/dataviewer.css" />
<script type="text/javascript"
	src="//static.uji.es/js/extjs/ext-3.4.0/adapter/ext/ext-base-debug.js"></script>
<script type="text/javascript"
	src="//static.uji.es/js/extjs/ext-3.4.0/ext-all-debug.js"></script>
<script type="text/javascript"
	src="//static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/RowEditor/0.0.1/RowEditor.js"></script>
<script type="text/javascript"
	src="//static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/tree/XmlTreeLoader/0.0.1/XmlTreeLoader.js"></script>
<script type="text/javascript"
	src="//static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/TabCloseMenu/0.0.1/TabCloseMenu.js"></script>
<script type="text/javascript"
	src="//static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/uji/TabPanel/0.0.1/TabPanel.js"></script>
<script type="text/javascript"
	src="//static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/uji/ApplicationPanel/0.0.1/ApplicationPanel.js"></script>
<script type="text/javascript"
	src="//static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/uji/ApplicationViewport/0.0.1/ApplicationViewport.js"></script>
<script type="text/javascript"
	src="//static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/uji/form/LookupWindow/0.0.2/LookupWindow.js"></script>
<script type="text/javascript"
	src="//static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/uji/grid/RowEditor/0.0.1/RowEditor.js"></script>
<script type="text/javascript"
	src="//static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/uji/grid/GridPanel/0.0.1/GridPanel.js"></script>
<script type="text/javascript"
	src="//static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/uji/form/LookupComboBox/0.0.3/LookupComboBox.js"></script>
<script type="text/javascript"
	src="//static.uji.es/js/extjs/ext-3.4.0/examples/ux/fileuploadfield/FileUploadField.js"></script>
<script type="text/javascript"
	src="//static.uji.es/js/extjs/uji-commons-extjs/Ext/ux/uji/Util.js"></script>

<script type="text/javascript" src="js/dashboard.js"></script>
<script type="text/javascript" src="js/tiposCertificadoPersonas.js"></script>
<script type="text/javascript"
	src="js/tiposCertificadoUbicacionesLogicas.js"></script>
<script type="text/javascript" src="js/tiposCertificadoTabPlantillas.js"></script>
<script type="text/javascript" src="js/tiposCertificados.js"></script>
<script type="text/javascript" src="js/variablesPlantillaPanel.js"></script>
<script type="text/javascript" src="js/emisionCertificadosWizard.js"></script>
<script type="text/javascript" src="js/emisionCertificadosWizardTipoCertificado.js"></script>
<script type="text/javascript" src="js/emisionCertificadosWizardDescripcionYAnverso.js"></script>
<script type="text/javascript" src="js/emisionCertificadosWizardDestinatario.js"></script>
<script type="text/javascript" src="js/emisionCertificadosWizardPlantilla.js"></script>
<script type="text/javascript" src="js/emisionCertificadosWizardPrevisualizacion.js"></script>
<script type="text/javascript" src="js/emisionCertificados.js"></script>
<script type="text/javascript" src="js/revisionCertificados.js"></script>
<script type="text/javascript" src="js/moderacionCertificados.js"></script>
<script type="text/javascript" src="js/crearCertificadoWindow.js"></script>
<script type="text/javascript" src="js/detalleCertificado.js"></script>
<script type="text/javascript" src="js/application.js"></script>
<script type="text/javascript" src="js/tipoUsuarioEnum.js"></script>

<script type="text/javascript"
	src="//static.uji.es/js/extjs/uji-commons-extjs/Ext/patch/DataWriterDatePatchExtjs3-X.js"></script>

<%
	String login = ((User) session.getAttribute(User.SESSION_USER))
			.getName();
%>

<script type="text/javascript">
    Ext.BLANK_IMAGE_URL = '//static.uji.es/js/extjs/ext-3.4.0/resources/images/default/s.gif';

    var login = '<%=login%>';

    Ext.onReady(function()
    {
        Ext.QuickTips.init();

    Ext.Ajax.defaultHeaders = {
       'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'
    };

        new Ext.ux.uji.ApplicationViewport(
        {
            codigoAplicacion : 'CEO',
            tituloAplicacion : 'Certificats Oficials'
        });
    });
</script>
</head>
<body>
</body>
</html>
