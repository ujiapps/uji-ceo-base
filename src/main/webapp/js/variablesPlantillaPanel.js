Ext.ns('UJI.CEO');

UJI.CEO.VariablesPlantillaPanel = Ext.extend(Ext.Panel,
{
    tpl : new Ext.XTemplate('<tpl for=".">', '<span class="comodin-nombre">{values.data.variable}</span> <span class="comodin-descripcion">{values.data.descripcion}</span>', '<br/>', '</tpl>'),
    autoScroll : true,

    initComponent : function()
    {
        var config = {};

        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.CEO.VariablesPlantillaPanel.superclass.initComponent.call(this);

        this.buildStore();
    },

    buildStore : function()
    {
        var ref = this;

        this.store = new Ext.data.Store(
        {
            url : 'rest/variable-plantilla',
            restful : true,
            reader : new Ext.data.XmlReader(
            {
                record : 'RecordUI'
            }, [ 'descripcion', 'variable' ]),
            listeners :
            {
                load : function(store, records, options)
                {
                    ref.update(records);
                }
            }
        });
    },
    listeners :
    {
        render : function()
        {
            this.store.load();
        }
    }
});