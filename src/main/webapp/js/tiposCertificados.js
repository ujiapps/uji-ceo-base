Ext.ns('UJI.CEO');

UJI.CEO.TiposCertificados = Ext.extend(Ext.Panel,
{
    layout : 'vbox',
    title : 'Tipus de certificats',
    region : 'center',
    gridCertificados : {},
    storeGridTiposCertificados : {},
    panelTabsCertificados : {},
    certificadoId : 0,
    autoScroll : true,
    closable : true,
    tabTiposCertificadoPersonas : {},
    tiposCertificadoUbicacionesLogicas : {},
    tiposCertificadoTabPlantillas : {},
    layoutConfig :
    {
        align : 'stretch'
    },

    initComponent : function()
    {
        var config = {};

        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.CEO.TiposCertificados.superclass.initComponent.call(this);

        this.initUI();
        this.add(this.gridCertificados);
        this.add(this.panelTabsCertificados);
    },

    initUI : function()
    {
        this.buildTabTiposCertificadoPersonas();
        this.buildComboEstadoEmisionManual();
        this.buildTabTiposCertificadoUbicacionesLogicas();
        this.buildTiposCertificadoTabPlantillas();
        this.buildStoreGridTiposCertificados();
        this.buildGridCertificados();
        this.buildPanelTabsCertificados();
    },

    buildStoreGridTiposCertificados : function()
    {
        this.storeGridTiposCertificados = new Ext.data.Store(
        {
            restful : true,
            url : '/ceo/rest/tipocertificado',
            autoLoad : true,
            reader : new Ext.data.XmlReader(
            {
                record : 'TipoCertificado',
                id : 'id'
            }, [ 'id', 'descripcionCa', 'descripcionEs', 'descripcionUk', 'emisionManual' ]),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            })
        });
    },

    buildComboEstadoEmisionManual : function()
    {
        var ref = this;

        this.comboEmisionManual = new Ext.form.ComboBox(
        {
            mode : 'local',
            triggerAction : 'all',
            forceSelection : true,
            editable : false,
            displayField : 'emisionManual',
            store : [ [ 1, 'Sí'], [0, 'No']],
            field: 'emisionManual'
        });
    },

    buildGridCertificados : function()
    {
        var ref = this;

        Ext.util.Format.comboRenderer = function (combo) {
            return function (value) {
                var record = combo.findRecord(combo.valueField, value);
                return record ? record.get(combo.displayField) : combo.valueNotFoundText;
            };
        };

        var userColumns = [
        {
            header : 'Id',
            width : 15,
            hidden : true,
            dataIndex : 'id',
            sortable : true,
            editor : new Ext.form.TextField(
            {
                allowBlank : true,
                disabled : true
            })
        },
        {
            header : 'Descripció en Català',
            dataIndex : 'descripcionCa',
            sortable : true,
            editor : new Ext.form.TextField(
            {
                allowBlank : false
            })
        },
        {
            header : 'Descripció en Castellà',
            dataIndex : 'descripcionEs',
            sortable : true,
            editor : new Ext.form.TextField(
            {
                allowBlank : true
            })
        },
        {
            header : 'Descripció en Anglès',
            dataIndex : 'descripcionUk',
            sortable : true,
            editor : new Ext.form.TextField(
            {
                allowBlank : true
            })
        },
        {
            header : 'Emisió manual',
            width : 30,
            dataIndex : 'emisionManual',
            sortable : true,
            renderer : Ext.util.Format.comboRenderer(ref.comboEmisionManual),
            editor : ref.comboEmisionManual,

        } ];

        this.gridCertificados = new Ext.ux.uji.grid.GridPanel(
        {
            frame : true,
            store : this.storeGridTiposCertificados,
            loadMask : true,
            height : 300,
            autoScroll : true,
            sortable : true,
            viewConfig :
            {
                forceFit : true
            },
            columns : userColumns,
            listeners :
            {
                rowclick : function(grid, rowIndex, e)
                {
                    var mask = new Ext.LoadMask(Ext.getBody(),
                    {
                        msg : "Please wait..."
                    });
                    mask.show();

                    ref.panelTabsCertificados.enable();
                    this.getTopToolbar().get('delete').enable();
                    var record = grid.getStore().getAt(rowIndex);
                    var activeTab = ref.panelTabsCertificados.getActiveTab();
                    var tab = undefined;
                    if (activeTab.title == "Persones")
                    {
                        tab = ref.tabTiposCertificadoPersonas;
                    }
                    else if (activeTab.title == "Ubicacions Lògiques")
                    {
                        tab = ref.tiposCertificadoUbicacionesLogicas;

                    }
                    else
                    {
                        tab = ref.tiposCertificadoTabPlantillas;
                    }
                    tab.setTipoCertificadoId(record.get("id"));
                    tab.loadStore(mask);
                }
            }
        });
    },

    buildTabTiposCertificadoPersonas : function()
    {
        this.tabTiposCertificadoPersonas = new UJI.CEO.TiposCertificadoPersonas();
    },

    buildTabTiposCertificadoUbicacionesLogicas : function()
    {
        this.tiposCertificadoUbicacionesLogicas = new UJI.CEO.TiposCertificadoUbicacionesLogicas();
    },

    buildTiposCertificadoTabPlantillas : function()
    {
        this.tiposCertificadoTabPlantillas = new UJI.CEO.TiposCertificadoTabPlantillas();
    },

    buildPanelTabsCertificados : function()
    {
        var ref = this;
        this.panelTabsCertificados = new Ext.TabPanel(
        {
            activeTab : 0,
            disabled : true,
            flex : 1,
            deferredRender : true,
            items : [ this.tabTiposCertificadoPersonas, this.tiposCertificadoUbicacionesLogicas, this.tiposCertificadoTabPlantillas ],
            listeners :
            {
                tabchange : function(tabpanel, tab)
                {
                    var record = ref.gridCertificados.getSelectionModel().getSelected();
                    if (record)
                    {
                        tab.setTipoCertificadoId(record.get("id"));
                        tab.loadStore();
                    }
                }
            }
        });
    }
});