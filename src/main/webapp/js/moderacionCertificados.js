Ext.ns('UJI.CEO');

var RegistroNoSeleccionado = 0;

UJI.CEO.ModeracionCertificados = Ext.extend(Ext.Panel,
    {
        layout: 'vbox',
        title: 'Aprovació de certificats',
        autoScroll: true,
        closable: true,
        botonAprobar: {},
        botonRechazar: {},
        parentGridRowSelectedId: RegistroNoSeleccionado,
        gridCertificados: {},
        storeCertificados: {},
        filtroTipoCertificado: {},
        storeTipoCertificado: {},
        storeAnyoCurso: {},
        botonConfirmarRechazoCertificado: {},
        botonCancelarRechazoCertificado: {},
        barraDeEstado: {},
        textAreaMensajeRechazo: {},
        ventanaRechazoCertificado: {},
        etiquetaFiltroTipoCertificado: {},
        filtroDescripcionCurso: {},
        botonLimpiarFiltros: {},
        storeDescripcionCurso: {},
        etiquetaFiltroDescripcionCurso: {},

        layoutConfig:
            {
                align: 'stretch'
            },

        initComponent: function () {
            var config = {};

            Ext.apply(this, Ext.apply(this.initialConfig, config));
            UJI.CEO.ModeracionCertificados.superclass.initComponent.call(this);

            this.initUI();

            this.add(this.panelFiltros);
            this.add(this.gridCertificados);
        },

        initUI: function () {
            this.buildBotonLimpiarFiltros();
            this.buildEtiquetaFiltroTipoCertificado();
            this.buildTextAreaMensajeRechazo();
            this.buildBotonPrevisualizarPDFCertificado();
            this.buildBotonCancelarRechazoCertificado();
            this.buildBotonConfirmarRechazoCertificado();
            this.buildVentanaRechazoCertificado();
            this.buildBarraDeEstado();

            this.buildStoreAnyoCurso();

            this.buildEtiquetaFiltroDescripcionCurso();
            this.buildStoreDescripcionCurso();
            this.buildFiltroDescripcionCurso();

            this.buildBotonAprobar();
            this.buildBotonRechazar();

            this.buildStoreTipoCertificado();
            this.buildFiltroTipoCertificado();

            this.buildEtiquetaFiltroAnyoCurso();
            this.buildFiltroAnyoCurso();

            this.buildStoreCertificados();
            this.buildGridCertificados();
            this.buildPanelFiltros();

        },

        visualizarCertificado: function (idioma) {
            var ref = this;
            var arraySeleccionadoEmision = ref.gridCertificados.getSelectionModel().getSelections();

            if (arraySeleccionadoEmision.length >= 1) {
                var listaCertificadosIds = [];

                for (var index = 0; index < arraySeleccionadoEmision.length; index++) {
                    if (arraySeleccionadoEmision[index].id) {
                        listaCertificadosIds.push(arraySeleccionadoEmision[index].id);
                    }
                }

                idiomaId = idioma;
                window.open("/ceo/rest/pdf/?idiomaId=" + idiomaId + "&listaCertificadosIds=" + listaCertificadosIds);
            }
        },

        buildBotonPrevisualizarPDFCertificado: function () {
            var ref = this;

            this.botonPrevisualizarPDFCertificado = new Ext.Button(
                {
                    text: 'Visualitzar',
                    iconCls: 'printer',
                    disabled: true,
                    menu:
                        {
                            xtype: 'menu',
                            border: false,
                            plain: true,
                            items: [
                                {
                                    text: 'Català',
                                    handler: function () {
                                        var idioma = 'CA';
                                        ref.visualizarCertificado(idioma);
                                    }
                                },
                                {
                                    text: 'Castellà',
                                    handler: function () {
                                        var idioma = 'ES';
                                        ref.visualizarCertificado(idioma);
                                    }
                                },
                                {
                                    text: 'Anglès',
                                    handler: function () {
                                        var idioma = 'EN';
                                        ref.visualizarCertificado(idioma);
                                    }
                                }]
                        }
                });
        },


        buildBarraDeEstado: function () {
            this.barraDeEstado = new Ext.Toolbar.TextItem(
                {
                    id: 'num',
                    text: 'Cap certificat sel·leccionat'
                });
        },

        buildEtiquetaFiltroTipoCertificado: function () {
            this.etiquetaFiltroTipoCertificado = new Ext.form.Label(
                {
                    text: 'Tipus de certificat'
                });
        },

        buildPanelFiltros: function () {
            this.panelFiltros = new Ext.form.FormPanel(
                {
                    title: 'Filtres',
                    layout: 'table',
                    frame: true,
                    border: false,
                    padding: '5px 0px',
                    labelWidth: 65,
                    defaults:
                        {
                            bodyStyle: 'padding:0px 10px'
                        },
                    layoutConfig:
                        {
                            columns: 4
                        },
                    items: [
                        {
                            layout: 'form',
                            items: [this.filtroTipoCertificado]
                        },
                        {
                            layout: 'form',
                            items: [this.filtroDescripcionCurso]
                        },
                        {
                            layout: 'form',
                            items: [this.filtroAnyoCurso]
                        },
                        {
                            layout: 'form',
                            items: [this.botonLimpiarFiltros]
                        }]
                });
        },

        buildBotonAprobar: function () {
            var ref = this;
            this.botonAprobar = new Ext.Button(
                {
                    text: 'Aprovar',
                    iconCls: 'application-add',
                    handler: function (button, event) {
                        var registroSeleccionadoAprobacion = ref.gridCertificados.getSelectionModel().getSelected();
                        if (!registroSeleccionadoAprobacion) {
                            Ext.MessageBox.show(
                                {
                                    title: 'Aprovar certificat',
                                    msg: 'Cal seleccionar un/s certificat/s a aprovar',
                                    buttons: Ext.MessageBox.OK,
                                    icon: Ext.MessageBox.WARNING
                                });
                        }

                        var sm = ref.gridCertificados.getSelectionModel();
                        var seleccionados = sm.getSelections();
                        if (seleccionados.length == 0) {
                            return;
                        }

                        var listaCertificadosIds = [];

                        for (index in seleccionados) {
                            if (seleccionados[index].id) {
                                listaCertificadosIds.push(seleccionados[index].id);
                            }
                        }

                        var tipoCertificadoId = ref.filtroTipoCertificado.getValue();
                        var box = Ext.MessageBox.wait('Processant...');
                        Ext.Ajax.request(
                            {
                                url: '/ceo/rest/certificado/pendientes/moderar/',
                                method: 'POST',
                                params:
                                    {
                                        listaCertificadosIds: listaCertificadosIds,
                                        moderar: true
                                    },
                                success: function () {
                                    var tipoCertificadoId = ref.filtroTipoCertificado.getValue();
                                    var descripcionCurso = ref.filtroDescripcionCurso.getRawValue();

                                    ref.storeCertificados.load(
                                        {
                                            callback: function (records, operation, success) {
                                                if (records.length === 0) {
                                                    ref.filtroDescripcionCurso.clearValue();
                                                    ref.filtroTipoCertificado.clearValue();
                                                }
                                                box.hide();
                                            }
                                        });
                                }
                            });

                    }
                });
        },

        buildBotonLimpiarFiltros: function () {
            var ref = this;
            this.botonLimpiarFiltros = new Ext.Button(
                {
                    text: 'Netejar filtres',
                    handler: function (button, event) {
                        ref.filtroDescripcionCurso.reset();
                        ref.filtroTipoCertificado.reset();
                        ref.filtroAnyoCurso.reset();
                        ref.storeCertificados.load(
                            {
                                params: {}
                            });

                    }
                });
        },

        buildBotonConfirmarRechazoCertificado: function () {
            var ref = this;

            this.botonConfirmarRechazoCertificado = new Ext.Button(
                {
                    text: 'Rebutjar certificat',
                    iconCls: 'application-delete',
                    handler: function (button) {
                        var sm = ref.gridCertificados.getSelectionModel();
                        var seleccionados = sm.getSelections();
                        if (seleccionados.length == 0) {
                            return;
                        }

                        var mensaje = ref.textAreaMensajeRechazo.getValue();
                        var listaCertificadosIds = [];

                        for (index in seleccionados) {
                            if (seleccionados[index].id) {
                                listaCertificadosIds.push(seleccionados[index].id);
                            }
                        }

                        var tipoCertificadoId = ref.filtroTipoCertificado.getValue();
                        var box = Ext.MessageBox.wait('Processant...');
                        Ext.Ajax.request(
                            {
                                url: '/ceo/rest/certificado/pendientes/moderar/',
                                method: 'POST',
                                params:
                                    {
                                        listaCertificadosIds: listaCertificadosIds,
                                        mensaje: mensaje,
                                        moderar: false
                                    },
                                success: function () {
                                    var tipoCertificadoId = ref.filtroTipoCertificado.getValue();
                                    var descripcionCurso = ref.filtroDescripcionCurso.getRawValue();
                                    ref.storeCertificados.load(
                                        {
                                            callback: function (records, operation, success) {
                                                if (records.length === 0) {
                                                    ref.filtroDescripcionCurso.reset();
                                                    ref.filtroTipoCertificado.reset();
                                                }
                                                box.hide();
                                            }
                                        });
                                    ref.ventanaRechazoCertificado.hide();
                                }
                            });
                    }
                });
        },

        buildTextAreaMensajeRechazo: function () {
            this.textAreaMensajeRechazo = new Ext.form.TextArea(
                {
                    title: "Missatge de rebuig",
                    flex: 1,
                    width: 470
                });
        },

        buildVentanaRechazoCertificado: function () {
            var ref = this;
            this.ventanaRechazoCertificado = new Ext.Window(
                {
                    title: 'Rebutjar un certificat',
                    layout: 'fit',
                    modal: true,
                    record: false,
                    width: 500,
                    height: 300,
                    closeAction: 'hide',
                    closable: true,
                    items: [
                        {
                            xtype: 'panel',
                            padding: 5,
                            flex: 1,
                            layout: 'vbox',
                            items: [
                                {
                                    xtype: 'label',
                                    height: 20,
                                    'text': 'Introdueix un text explicatiu del motiu de rebuig del certificat:'
                                }, ref.textAreaMensajeRechazo]
                        }],
                    buttons: [ref.botonConfirmarRechazoCertificado, ref.botonCancelarRechazoCertificado]
                });
        },

        buildBotonCancelarRechazoCertificado: function () {

            var ref = this;

            this.botonCancelarRechazoCertificado = new Ext.Button(
                {
                    text: 'Cancel.lar',
                    handler: function (btn, evt) {
                        ref.ventanaRechazoCertificado.hide();
                    }
                });

        },

        buildBotonRechazar: function () {
            var ref = this;

            this.botonRechazar = new Ext.Button(
                {
                    text: 'Rebutjar',
                    iconCls: 'application-delete',
                    handler: function () {
                        var registroSeleccionadoRechazo = ref.gridCertificados.getSelectionModel().getSelected();
                        if (!registroSeleccionadoRechazo) {
                            Ext.MessageBox.show(
                                {
                                    title: 'Rebutjar certificat',
                                    msg: 'Cal seleccionar un/s certificat/s a rebutjar',
                                    buttons: Ext.MessageBox.OK,
                                    icon: Ext.MessageBox.WARNING
                                });
                        } else {
                            ref.ventanaRechazoCertificado.show();
                        }
                    }
                });
        },

        buildStoreTipoCertificado: function () {
            this.storeTipoCertificado = new Ext.data.Store(
                {
                    restful: true,
                    url: '/ceo/rest/tipocertificado/moderacion',
                    autoLoad: true,
                    reader: new Ext.data.XmlReader(
                        {
                            record: 'TipoCertificado',
                            id: 'id'
                        }, ['id', 'descripcionCa', 'descripcionEs', 'descripcionUk', 'plantillaCa', 'plantillaEs', 'plantillaUk'])
                });
        },

        buildFiltroTipoCertificado: function () {
            var ref = this;

            this.filtroTipoCertificado = new Ext.form.ComboBox(
                {
                    store: this.storeTipoCertificado,
                    displayField: 'descripcionCa',
                    emptyText: 'Selecciona un Tipus de certificat',
                    editable: false,
                    triggerAction: 'all',
                    fieldLabel: 'Tipus',
                    mode: 'remote',
                    width: 250,
                    listWidth: 400,
                    valueField: 'id',
                    name: 'filtroTipoCertificado',
                    listeners:
                        {
                            select: function (combo, record) {
                                var tipoCertificadoId = record.get('id');
                                var descripcionCurso = ref.filtroDescripcionCurso.getRawValue();
                                var anyo = ref.filtroAnyoCurso.getRawValue();
                                ref.storeCertificados.load(
                                    {
                                        params:
                                            {
                                                tipoCertificadoId: tipoCertificadoId,
                                                descripcionCurso: descripcionCurso,
                                                anyo: anyo
                                            }
                                    });
                            }
                        }
                });
        },

        buildStoreAnyoCurso: function () {
            var ref = this;
            this.storeAnyoCurso = new Ext.data.Store(
                {
                    restful: true,
                    url: '/ceo/rest/certificado/anyos/',
                    autoLoad: false,
                    reader: new Ext.data.XmlReader(
                        {
                            record: 'Anyo',
                            id: 'id'
                        }, ['id', 'anyo'])
                });
        },

        buildEtiquetaFiltroDescripcionCurso: function () {
            this.etiquetaFiltroDescripcionCurso = new Ext.form.Label(
                {
                    text: 'Descripció curs'
                });
        },

        buildStoreDescripcionCurso: function () {
            var ref = this;
            this.storeDescripcionCurso = new Ext.data.Store(
                {
                    restful: true,
                    url: '/ceo/rest/certificado/descripcionCursos',
                    baseParams: {
                        estado: "moderar"
                    },
                    autoLoad: true,
                    reader: new Ext.data.XmlReader(
                        {
                            record: 'Certificado',
                            id: 'id'
                        }, ['descripcionCurso']),
                    listeners:
                        {
                            beforeload: function () {
                                ref.storeDescripcionCurso.setBaseParam("tipoCertificadoId", ref.filtroTipoCertificado.getValue());
                                ref.storeDescripcionCurso.setBaseParam('anyo', ref.filtroAnyoCurso.getRawValue())
                            }
                        }
                });
        },

        buildFiltroDescripcionCurso: function () {
            var ref = this;

            this.filtroDescripcionCurso = new Ext.form.ComboBox(
                {
                    store: this.storeDescripcionCurso,
                    displayField: 'descripcionCurso',
                    emptyText: 'Selecciona una descrició de curs',
                    editable: false,
                    triggerAction: 'all',
                    fieldLabel: 'Descripcio',
                    mode: 'remote',
                    width: 250,
                    listWidth: 500,
                    valueField: 'descripcionCurso',
                    listeners:
                        {
                            select: function (combo, record) {
                                var descripcion = record.get('descripcionCurso');
                                var tipoCertificadoId = ref.filtroTipoCertificado.getValue();
                                var anyo = ref.filtroAnyoCurso.getRawValue();
                                ref.storeCertificados.load(
                                    {
                                        beforequery: function (qe) {
                                            delete qe.combo.lastQuery;
                                        },
                                        params:
                                            {
                                                tipoCertificadoId: tipoCertificadoId,
                                                descripcionCurso: descripcion,
                                                anyo: anyo
                                            },
                                        callback: function () {
                                            ref.gridCertificados.getView().refresh();
                                        }
                                    });
                            }
                        }
                });
        },

        buildEtiquetaFiltroAnyoCurso: function () {
            this.etiquetaFiltroAnyoCurso = new Ext.form.Label(
                {
                    text: 'Any'
                });
        },

        buildFiltroAnyoCurso: function () {
            var ref = this;

            this.filtroAnyoCurso = new Ext.form.ComboBox(
                {
                    store: this.storeAnyoCurso,
                    displayField: 'anyo',
                    emptyText: 'Selecciona un Any',
                    editable: false,
                    width: 150,
                    fieldLabel: 'Any',
                    triggerAction: 'all',
                    mode: 'remote',
                    valueField: 'id',
                    name: 'filtroAnyoCurso',
                    listeners:
                        {
                            select: function (combo, record) {
                                var descripcionCurso = ref.filtroDescripcionCurso.getRawValue();
                                var tipoCertificadoId = ref.filtroTipoCertificado.getValue();
                                var anyo = ref.filtroAnyoCurso.getRawValue();
                                ref.storeCertificados.load(
                                    {
                                        params:
                                            {
                                                tipoCertificadoId: tipoCertificadoId,
                                                descripcionCurso: descripcionCurso,
                                                anyo: anyo
                                            }
                                    });
                            }
                        }
                });
        },

        buildStoreCertificados: function () {
            this.storeCertificados = new Ext.data.Store(
                {
                    restful: true,
                    proxy: new Ext.data.HttpProxy(
                        {
                            url: '/ceo/rest/certificado/pendientes/moderar'
                        }),
                    autoLoad: true,
                    reader: new Ext.data.XmlReader(
                        {
                            record: 'Certificado',
                            id: 'id'
                        }, ['id', 'destinatarioId', 'dniDestinatario', 'descripcionCa', 'nombreDestinatario', 'mailDestinatario', 'descripcionCurso',
                            {
                                name: 'fechaCreacion',
                                type: 'date',
                                dateFormat: 'd/m/Y H:i:s'
                            },
                            {
                                name: 'fechaCertificado',
                                type: 'date',
                                dateFormat: 'd/m/Y H:i:s'
                            },
                            {
                                name: 'fechaRecogida',
                                type: 'date',
                                dateFormat: 'd/m/Y H:i:s'
                            }, 'usuarioId', 'tipoCertificadoId', 'textoCa', 'textoEs', 'textoUk', 'moderacionPerId', 'moderacionAceptado',
                            {
                                name: 'moderacionFecha',
                                type: 'date',
                                dateFormat: 'd/m/Y H:i:s'
                            }]),
                    writer: new Ext.data.XmlWriter(
                        {
                            xmlEncoding: 'UTF-8'
                        }, ['id', 'fechaRecogida'])
                });
        },

        buildGridCertificados: function () {
            var ref = this;
            var sm = new Ext.grid.CheckboxSelectionModel(
                {
                    handleMouseDown: Ext.emptyFn,
                    listeners:
                        {
                            selectionchange: function () {
                                var num = ref.gridCertificados.getSelectionModel().getCount();
                                if (num > 1) {
                                    ref.barraDeEstado.setText(num + ' certificats sel·leccionats');
                                } else if (num === 1) {
                                    ref.barraDeEstado.setText(num + ' certificat sel·leccionat');
                                } else {
                                    ref.barraDeEstado.setText('Cap certificat sel·leccionat');
                                }

                                filasSeleccionadas = sm.getSelections();

                                if (filasSeleccionadas.length === 0) {
                                    ref.botonPrevisualizarPDFCertificado.disable();
                                    ref.botonRechazar.disable();
                                    ref.botonAprobar.disable();
                                } else {
                                    ref.botonPrevisualizarPDFCertificado.enable();
                                    ref.botonRechazar.enable();
                                    ref.botonAprobar.enable();
                                }


                            }
                        }
                });

            var userColumnsGridCertificados = [sm,
                {
                    header: 'Id',
                    dataIndex: 'id',
                    sortable: true,
                    width: 20,
                    hidden: true
                },
                {
                    header: 'Destinatari',
                    dataIndex: 'destinatarioId',
                    sortable: true,
                    width: 20,
                    hidden: true
                },
                {
                    header: 'DNI Destinatari',
                    dataIndex: 'dniDestinatario',
                    sortable: true,
                    width: 20
                },
                {
                    header: 'Nom Destinatari',
                    dataIndex: 'nombreDestinatario',
                    sortable: true,
                    width: 40
                },
                {
                    header: 'Mail Destinatari',
                    dataIndex: 'mailDestinatario',
                    sortable: true,
                    width: 30
                },
                {
                    header: 'Descripció curs',
                    dataIndex: 'descripcionCa',
                    sortable: true,
                    width: 40
                },
                {
                    header: 'Data Certificat',
                    dataIndex: 'fechaCertificado',
                    sortable: true,
                    format: 'd/m/Y',
                    xtype: 'datecolumn',
                    width: 25
                }];

            this.gridCertificados = new Ext.grid.GridPanel(
                {
                    frame: true,
                    loadMask: true,
                    flex: 1,
                    sortable: true,
                    viewConfig:
                        {
                            forceFit: true
                        },
                    store: ref.storeCertificados,
                    columns: userColumnsGridCertificados,
                    sm: sm,
                    bbar: new Ext.Toolbar(
                        {
                            items: [this.barraDeEstado]
                        }),
                    tbar: [this.botonPrevisualizarPDFCertificado, '-', this.botonAprobar, '-', this.botonRechazar]
                });
        }
    });
