function initCEODashboard()
{
    var dashboardPanel = new Ext.Panel(
    {
        frame : true,
        padding : '10',
        title : 'Dashboard',
        layout :
        {
            type : 'vbox',
            align : 'stretch'
        },
        items : []
    });

    return dashboardPanel;
}