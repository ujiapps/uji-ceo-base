Ext.ns('UJI.CEO');
var RegistroNoSeleccionado = 0;

UJI.CEO.TiposCertificadoUbicacionesLogicas = Ext.extend(Ext.Panel,
{
    title : 'Ubicacions Lògiques',
    layout : 'vbox',
    layoutConfig :
    {
        align : 'stretch'
    },
    botonBorrarUbicacionLogica : {},
    botonAnyadirUbicacionLogica : {},
    storeCertificadoUbicacionesLogicas : {},
    gridUbicacionesLogicas : {},
    tipoCertificadoId : null,

    initComponent : function()
    {
        var config = {};

        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.CEO.TiposCertificadoUbicacionesLogicas.superclass.initComponent.call(this);

        this.initUI();

        this.add(this.gridUbicacionesLogicas);
    },
    initUI : function()
    {
        this.buildStoreCertificadoUbicacionesLogicas();
        this.buildBotonBorrarUbicacionLogica();
        this.buildBotonAnyadirUbicacionLogica();
        this.buildGridUbicacionesLogicas();
    },

    buildStoreCertificadoUbicacionesLogicas : function()
    {
        this.storeCertificadoUbicacionesLogicas = new Ext.data.Store(
        {
            restful : true,
            proxy : new Ext.data.HttpProxy(
            {
                url : '/ceo/rest/tipocertificado/' + this.tipoCertificadoId + '/ubicacionlogica'
            }),
            autoload : false,
            reader : new Ext.data.XmlReader(
            {
                record : 'UbicacionLogica',
                id : 'id'
            }, [ 'id', 'nombre', 'mail' ]),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            })
        });
    },

    buildBotonAnyadirUbicacionLogica : function()
    {
        var ref = this;
        this.addEvents('LookupWindowClickSelection');
        var window = new Ext.ux.uji.form.LookupWindow(
        {
            appPrefix : 'ceo',
            bean : 'ubicacionLogica'
        });

        window.on('LookoupWindowClickSeleccion', function(record)
        {
            var rec = new ref.storeCertificadoUbicacionesLogicas.recordType(
            {
                id : record.data.id
            });

            ref.storeCertificadoUbicacionesLogicas.proxy.setUrl('/ceo/rest/tipocertificado/' + ref.tipoCertificadoId + '/ubicacionlogica');

            ref.storeCertificadoUbicacionesLogicas.insert(0, rec);
        });

        this.botonAnyadirUbicacionLogica = new Ext.Button(
        {
            text : 'Afegir',
            iconCls : 'application-add',
            handler : function(boton)
            {
                if (ref.tipoCertificadoId)
                {
                    window.show();
                }
            }
        });
    },

    buildBotonBorrarUbicacionLogica : function()
    {
        var ref = this;
        this.botonBorrarUbicacionLogica = new Ext.Button(
        {
            text : 'Esborrar',
            iconCls : 'application-delete',
            handler : function(boton, evento)
            {
                if (ref.tipoCertificadoId)
                {
                    var registroSeleccionado = ref.gridUbicacionesLogicas.getSelectionModel().getSelected();
                    if (!registroSeleccionado)
                    {
                        Ext.MessageBox.show(
                        {
                            title : 'Error',
                            msg : "Cal seleccionar un registre a esborrar",
                            buttons : Ext.MessageBox.OK,
                            icon : Ext.MessageBox.ERROR
                        });
                    }
                    else
                    {
                        Ext.Msg.confirm("confirmació de la operació", "Realment dessitja esborrar aquest registre?", function(boton, texto)
                        {
                            if (boton == "yes")
                            {
                                ref.storeCertificadoUbicacionesLogicas.proxy.setUrl('/ceo/rest/tipocertificado/' + ref.tipoCertificadoId + '/ubicacionlogica');
                                ref.storeCertificadoUbicacionesLogicas.remove(registroSeleccionado);
                            }
                        });
                    }
                }
            }
        });
    },

    buildGridUbicacionesLogicas : function()
    {
        var ref = this;
        var colModel = new Ext.grid.ColumnModel(
        {
            defaults :
            {
                sortable : true
            },
            columns : [
            {
                header : "id",
                width : 25,
                dataIndex : 'id',
                hidden : true
            },
            {
                header : 'Nom',
                dataIndex : 'nombre'
            },
            {
                header : 'EMail',
                dataIndex : 'mail'
            } ]
        });

        this.gridUbicacionesLogicas = new Ext.grid.GridPanel(
        {
            frame : true,
            flex : 1,
            tbar :
            {
                items : [ ref.botonAnyadirUbicacionLogica, ref.botonBorrarUbicacionLogica ]
            },
            viewConfig :
            {
                forceFit : true
            },
            store : ref.storeCertificadoUbicacionesLogicas,
            sm : new Ext.grid.RowSelectionModel(
            {
                singleSelect : true
            }),
            colModel : colModel
        });
    },
    setTipoCertificadoId : function(tipoCertificadoId)
    {
        this.tipoCertificadoId = tipoCertificadoId;
    },

    loadStore : function(mask)
    {
        this.storeCertificadoUbicacionesLogicas.proxy.setUrl('/ceo/rest/tipocertificado/' + this.tipoCertificadoId + '/ubicacionlogica');
        this.storeCertificadoUbicacionesLogicas.load(
        {
            callback : function()
            {
                if (mask) {
                    mask.hide();
                }
            }
        });
    }
});