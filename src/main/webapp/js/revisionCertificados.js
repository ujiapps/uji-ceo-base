Ext.ns('UJI.CEO');

var RegistroNoSeleccionado = 0;

UJI.CEO.RevisionCertificados = Ext.extend(Ext.Panel,
    {
        layout: 'vbox',
        title: 'Revisió de certificats',
        autoScroll: true,
        closable: true,
        botonAceptar: {},
        botonRechazar: {},
        gridCertificados: {},
        storeCertificados: {},
        filtroTipoCertificado: {},
        storeTipoCertificado: {},
        botonConfirmarRechazoCertificado: {},
        botonCancelarRechazoCertificado: {},
        textAreaMensajeRechazo: {},
        ventanaRechazoCertificado: {},
        etiquetaFiltroTipoCertificado: {},
        filtroDescripcionCurso: {},
        storeDescripcionCurso: {},
        storeAnyoCurso: {},
        botonLimpiarFiltros: {},
        etiquetaFiltroDescripcionCurso: {},
        barraDeEstado: {},

        layoutConfig:
            {
                align: 'stretch'
            },

        initComponent: function () {
            var config = {};

            Ext.apply(this, Ext.apply(this.initialConfig, config));
            UJI.CEO.RevisionCertificados.superclass.initComponent.call(this);

            this.initUI();

            this.add(this.panelFiltros);
            this.add(this.gridCertificados);
        },

        initUI: function () {
            this.buildCheckboxSelectionModel();

            this.buildBotonLimpiarFiltros();
            this.buildBotonPrevisualizarPDFCertificado();
            this.buildEtiquetaFiltroTipoCertificado();
            this.buildEtiquetaFiltroDescripcionCurso();
            this.buildStoreDescripcionCurso();
            this.buildFiltroDescripcionCurso();
            this.buildBarraDeEstado();

            this.buildBotonAceptarCertificado();
            this.buildBotonEliminarCertificado();

            this.buildStoreAnyoCurso();
            this.buildStoreTipoCertificado();
            this.buildFiltroTipoCertificado();

            this.buildEtiquetaFiltroAnyoCurso();
            this.buildFiltroAnyoCurso();

            this.buildStoreCertificados();
            this.buildGridCertificados();
            this.buildPanelFiltros();
        },

        buildBarraDeEstado: function () {
            this.barraDeEstado = new Ext.Toolbar.TextItem(
                {
                    id: 'num',
                    text: 'Cap certificat sel·leccionat'
                });
        },

        buildEtiquetaFiltroTipoCertificado: function () {
            this.etiquetaFiltroTipoCertificado = new Ext.form.Label(
                {
                    text: 'Tipus de certificat'
                });
        },

        buildBotonPrevisualizarPDFCertificado: function () {
            var ref = this;

            this.botonPrevisualizarPDFCertificado = new Ext.Button(
                {
                    text: 'Visualitzar',
                    iconCls: 'printer',
                    disabled: true,
                    menu:
                        {
                            xtype: 'menu',
                            border: false,
                            plain: true,
                            items: [
                                {
                                    text: 'Català',
                                    handler: function () {
                                        var idioma = 'CA';
                                        ref.visualizarCertificado(idioma);
                                    }
                                },
                                {
                                    text: 'Castellà',
                                    handler: function () {
                                        var idioma = 'ES';
                                        ref.visualizarCertificado(idioma);
                                    }
                                },
                                {
                                    text: 'Anglès',
                                    handler: function () {
                                        var idioma = 'EN';
                                        ref.visualizarCertificado(idioma);
                                    }
                                }]
                        }
                });
        },

        buildEtiquetaFiltroAnyoCurso: function () {
            this.etiquetaFiltroAnyoCurso = new Ext.form.Label(
                {
                    text: 'Any'
                });
        },

        buildFiltroAnyoCurso: function () {
            var ref = this;

            this.filtroAnyoCurso = new Ext.form.ComboBox(
                {
                    store: this.storeAnyoCurso,
                    displayField: 'anyo',
                    emptyText: 'Selecciona un Any',
                    editable: false,
                    width: 120,
                    fieldLabel: 'Any',
                    listWidth: 120,
                    triggerAction: 'all',
                    mode: 'remote',
                    valueField: 'id',
                    name: 'filtroAnyoCurso',
                    listeners:
                        {
                            select: function (combo, record) {
                                var descripcionCurso = ref.filtroDescripcionCurso.getRawValue();
                                var tipoCertificadoId = ref.filtroTipoCertificado.getValue();
                                var anyo = ref.filtroAnyoCurso.getRawValue();
                                ref.storeCertificados.load(
                                    {
                                        params:
                                            {
                                                tipoCertificadoId: tipoCertificadoId,
                                                descripcionCurso: descripcionCurso,
                                                anyo: anyo
                                            }
                                    });
                            }
                        }
                });
        },

        buildPanelFiltros: function () {
            this.panelFiltros = new Ext.form.FormPanel(
                {
                    title: 'Filtres',
                    layout: 'table',
                    frame: true,
                    border: false,
                    padding: '5px 0px',
                    labelWidth: 65,
                    defaults:
                        {
                            bodyStyle: 'padding:0px 10px'
                        },
                    layoutConfig:
                        {
                            columns: 4
                        },
                    items: [
                        {
                            layout: 'form',
                            items: [this.filtroTipoCertificado]
                        },
                        {
                            layout: 'form',
                            items: [this.filtroDescripcionCurso]
                        },
                        {
                            layout: 'form',
                            items: [this.filtroAnyoCurso]
                        },
                        {
                            layout: 'form',
                            items: [this.botonLimpiarFiltros]
                        }]
                });
        },

        buildBotonLimpiarFiltros: function () {
            var ref = this;
            this.botonLimpiarFiltros = new Ext.Button(
                {
                    text: 'Netejar filtres',
                    handler: function (button, event) {
                        ref.filtroDescripcionCurso.reset();
                        ref.filtroTipoCertificado.reset();
                        ref.filtroAnyoCurso.reset();
                        ref.storeCertificados.load(
                            {
                                params: {}
                            });
                    }
                });
        },

        buildBotonAceptarCertificado: function () {
            var ref = this;
            this.botonAceptarCertificado = new Ext.Button(
                {
                    text: 'Acceptar i enviar a aprovació',
                    iconCls: 'application-add',
                    handler: function (button, event) {
                        var registroSeleccionadoAceptacion = ref.gridCertificados.getSelectionModel().getSelected();
                        if (!registroSeleccionadoAceptacion) {
                            Ext.MessageBox.show(
                                {
                                    title: 'Acceptar certificat',
                                    msg: 'Cal seleccionar un/s certificat/s a acceptar',
                                    buttons: Ext.MessageBox.OK,
                                    icon: Ext.MessageBox.WARNING
                                });
                        }

                        var sm = ref.gridCertificados.getSelectionModel();
                        var seleccionados = sm.getSelections();
                        if (seleccionados.length == 0) {
                            return;
                        }

                        var listaCertificadosIds = [];

                        for (index in seleccionados) {
                            if (seleccionados[index].id) {
                                listaCertificadosIds.push(seleccionados[index].id);
                            }
                        }

                        var tipoCertificadoId = ref.filtroTipoCertificado.getValue();
                        var box = Ext.MessageBox.wait('Processant...');

                        Ext.Ajax.request(
                            {
                                url: '/ceo/rest/certificado/pendientes/revisar/',
                                method: 'POST',
                                params:
                                    {
                                        listaCertificadosIds: listaCertificadosIds
                                    },
                                success: function () {
                                    var tipoCertificadoId = ref.filtroTipoCertificado.getValue();
                                    var descripcionCurso = ref.filtroDescripcionCurso.getRawValue();

                                    ref.storeCertificados.load(
                                        {
                                            params:
                                                {
                                                    tipoCertificadoId: tipoCertificadoId,
                                                    descripcionCurso: descripcionCurso
                                                },
                                            callback: function (records, operation, success) {
                                                if (records.length === 0) {
                                                    ref.filtroTipoCertificado.clearValue();
                                                    ref.filtroDescripcionCurso.clearValue();
                                                }
                                                box.hide();
                                            }
                                        });
                                }
                            });
                    }
                });
        },

        buildBotonEliminarCertificado: function () {
            var ref = this;

            this.botonEliminarCertificado = new Ext.Button(
                {
                    text: 'Esborrar',
                    iconCls: 'application-delete',
                    disabled: true,
                    handler: function (boton, evento) {
                        var sm = ref.gridCertificados.getSelectionModel();
                        var seleccionados = sm.getSelections();

                        if (!seleccionados) {
                            Ext.MessageBox.show(
                                {
                                    title: 'Esborrar certificat',
                                    msg: 'Cal seleccionar al menys un certificat a esborrar',
                                    buttons: Ext.MessageBox.OK,
                                    icon: Ext.MessageBox.WARNING
                                });
                        } else {
                            Ext.Msg.confirm("Corfirmació de la operació", "Realment dessitja esborrar aquest/s certificat/s?", function (button, text) {
                                if (button == "yes") {

                                    var listaCertificadosIds = [];

                                    for (index in seleccionados) {
                                        if (seleccionados[index].id) {
                                            listaCertificadosIds.push(seleccionados[index].id);
                                        }
                                    }

                                    var tipoCertificadoId = ref.filtroTipoCertificado.getValue();
                                    var box = Ext.MessageBox.wait('Processant...');
                                    Ext.Ajax.request(
                                        {
                                            url: '/ceo/rest/certificado/pendientes/borrar',
                                            method: 'POST',
                                            params:
                                                {
                                                    listaCertificadosIds: listaCertificadosIds,
                                                    moderar: true
                                                },
                                            success: function () {
                                                var tipoCertificadoId = ref.filtroTipoCertificado.getValue();
                                                var descripcionCurso = ref.filtroDescripcionCurso.getRawValue();
                                                ref.storeCertificados.load(
                                                    {
                                                        params:
                                                            {
                                                                tipoCertificadoId: tipoCertificadoId,
                                                                descripcionCurso: descripcionCurso
                                                            },
                                                        callback: function (records, operation, success) {
                                                            if (records.length === 0) {
                                                                ref.filtroDescripcionCurso.store.reload();
                                                                ref.filtroTipoCertificado.store.reload();
                                                            }
                                                            box.hide();
                                                        }
                                                    });
                                            }
                                        });
                                }
                            });
                        }
                    }
                });
        },

        buildStoreAnyoCurso: function () {
            var ref = this;
            this.storeAnyoCurso = new Ext.data.Store(
                {
                    restful: true,
                    url: '/ceo/rest/certificado/anyos/',
                    autoLoad: false,
                    reader: new Ext.data.XmlReader(
                        {
                            record: 'Anyo',
                            id: 'id'
                        }, ['id', 'anyo'])
                });
        },

        buildStoreTipoCertificado: function () {
            this.storeTipoCertificado = new Ext.data.Store(
                {
                    restful: true,
                    url: '/ceo/rest/tipocertificado/revision',
                    autoLoad: true,
                    reader: new Ext.data.XmlReader(
                        {
                            record: 'TipoCertificado',
                            id: 'id'
                        }, ['id', 'descripcionCa', 'descripcionEs', 'descripcionUk', 'plantillaCa', 'plantillaEs', 'plantillaUk'])
                });
        },

        buildFiltroTipoCertificado: function () {
            var ref = this;

            this.filtroTipoCertificado = new Ext.form.ComboBox(
                {
                    store: this.storeTipoCertificado,
                    displayField: 'descripcionCa',
                    emptyText: 'Selecciona un Tipus de certificat',
                    listWidth: 400,
                    editable: false,
                    fieldLabel: 'Tipus',
                    triggerAction: 'all',
                    mode: 'remote',
                    width: 250,
                    valueField: 'id',
                    name: 'filtroTipoCertificado',
                    listeners:
                        {
                            select: function (combo, record) {
                                var tipoCertificadoId = record.get('id');
                                var descripcionCurso = ref.filtroDescripcionCurso.getRawValue();
                                var anyo = ref.filtroAnyoCurso.getRawValue();
                                ref.storeCertificados.load(
                                    {
                                        params:
                                            {
                                                tipoCertificadoId: tipoCertificadoId,
                                                descripcionCurso: descripcionCurso,
                                                anyo: anyo
                                            }
                                    });
                            }
                        }
                });
        },

        buildEtiquetaFiltroDescripcionCurso: function () {
            this.etiquetaFiltroDescripcionCurso = new Ext.form.Label(
                {
                    text: 'Descripció curs'
                });
        },

        buildStoreDescripcionCurso: function () {
            var ref = this;
            this.storeDescripcionCurso = new Ext.data.Store(
                {
                    restful: true,
                    url: '/ceo/rest/certificado/descripcionCursos',
                    baseParams: {
                        estado: "revisar"
                    },
                    autoLoad: false,
                    reader: new Ext.data.XmlReader(
                        {
                            record: 'Certificado',
                            id: 'id'
                        }, ['id', 'textoAnverso', 'descripcionCurso']),
                    listeners:
                        {
                            beforeload: function () {
                                var tipoCertificadoId = ref.filtroTipoCertificado.getValue();
                                ref.storeDescripcionCurso.setBaseParam("tipoCertificadoId", ref.filtroTipoCertificado.getValue());
                                ref.storeDescripcionCurso.setBaseParam('anyo', ref.filtroAnyoCurso.getRawValue())
                            }
                        }
                });
        },

        buildFiltroDescripcionCurso: function () {
            var ref = this;

            this.filtroDescripcionCurso = new Ext.form.ComboBox(
                {
                    store: this.storeDescripcionCurso,
                    displayField: 'descripcionCurso',
                    emptyText: 'Selecciona una descrició de curs',
                    editable: false,
                    fieldLabel: 'Descripcio',
                    triggerAction: 'all',
                    mode: 'remote',
                    width: 250,
                    listWidth: 500,
                    valueField: 'id',
                    listeners:
                        {
                            beforequery: function (qe) {
                                delete qe.combo.lastQuery;
                            },
                            select: function (combo, record) {
                                var descripcion = record.get('descripcionCurso');
                                var tipoCertificadoId = ref.filtroTipoCertificado.getValue();
                                var anyo = ref.filtroAnyoCurso.getRawValue();
                                ref.storeCertificados.load(
                                    {
                                        params:
                                            {
                                                tipoCertificadoId: tipoCertificadoId,
                                                descripcionCurso: descripcion,
                                                anyo: anyo
                                            },
                                        callback: function () {
                                            ref.gridCertificados.getView().refresh();
                                        }
                                    });
                            }
                        }
                });
        },

        buildStoreCertificados: function () {
            this.storeCertificados = new Ext.data.Store(
                {
                    restful: true,
                    proxy: new Ext.data.HttpProxy(
                        {
                            url: '/ceo/rest/certificado/pendientes/revisar'
                        }),
                    autoLoad: true,
                    reader: new Ext.data.XmlReader(
                        {
                            record: 'Certificado',
                            id: 'id'
                        }, ['id', 'destinatarioId', 'dniDestinatario', 'nombreDestinatario', 'mailDestinatario', 'descripcionCurso',
                            {
                                name: 'fechaCreacion',
                                type: 'date',
                                dateFormat: 'd/m/Y H:i:s'
                            },
                            {
                                name: 'fechaCertificado',
                                type: 'date',
                                dateFormat: 'd/m/Y H:i:s'
                            },
                            {
                                name: 'fechaRecogida',
                                type: 'date',
                                dateFormat: 'd/m/Y H:i:s'
                            }, 'usuarioId', 'tipoCertificadoId', 'textoCa', 'textoEs', 'textoUk', 'moderacionPerId', 'moderacionAceptado',
                            {
                                name: 'moderacionFecha',
                                type: 'date',
                                dateFormat: 'd/m/Y H:i:s'
                            }]),
                    writer: new Ext.data.XmlWriter(
                        {
                            xmlEncoding: 'UTF-8'
                        }, ['id', 'fechaRecogida'])
                });
        },

        buildCheckboxSelectionModel: function () {
            var ref = this;
            this.checkboxSelectionModel = new Ext.grid.CheckboxSelectionModel(
                {
                    listeners:
                        {
                            selectionchange: function (sm) {

                                var num = ref.gridCertificados.getSelectionModel().getCount();
                                if (num > 1) {
                                    ref.barraDeEstado.setText(num + ' certificats sel·leccionats');
                                } else if (num === 1) {
                                    ref.barraDeEstado.setText(num + ' certificat sel·leccionat');
                                } else {
                                    ref.barraDeEstado.setText('Cap certificat sel·leccionat');
                                }

                                filasSeleccionadas = sm.getSelections();

                                if (filasSeleccionadas.length === 0) {
                                    ref.botonPrevisualizarPDFCertificado.disable();
                                    ref.botonEliminarCertificado.disable();
                                    ref.botonAceptarCertificado.disable();
                                } else {
                                    ref.botonPrevisualizarPDFCertificado.enable();
                                    ref.botonEliminarCertificado.enable();
                                    ref.botonAceptarCertificado.enable();
                                }

                            }
                        }
                });
        },

        visualizarCertificado: function (idioma) {
            var ref = this;
            var arraySeleccionadoEmision = ref.gridCertificados.getSelectionModel().getSelections();

            if (arraySeleccionadoEmision.length >= 1) {
                var listaCertificadosIds = [];

                for (var index = 0; index < arraySeleccionadoEmision.length; index++) {
                    if (arraySeleccionadoEmision[index].id) {
                        listaCertificadosIds.push(arraySeleccionadoEmision[index].id);
                    }
                }

                idiomaId = idioma;
                window.open("/ceo/rest/pdf/?idiomaId=" + idiomaId + "&listaCertificadosIds=" + listaCertificadosIds);
            }
        },

        buildGridCertificados: function () {
            var ref = this;
            var userColumnsGridCertificados = [this.checkboxSelectionModel,
                {
                    header: 'Id',
                    dataIndex: 'id',
                    sortable: true,
                    width: 20,
                    hidden: true
                },
                {
                    header: 'Destinatari',
                    dataIndex: 'destinatarioId',
                    sortable: true,
                    width: 20,
                    hidden: true
                },
                {
                    header: 'DNI Destinatari',
                    dataIndex: 'dniDestinatario',
                    sortable: true,
                    width: 30
                },
                {
                    header: 'Nom Destinatari',
                    dataIndex: 'nombreDestinatario',
                    sortable: true,
                    width: 40
                },
                {
                    header: 'Mail Destinatari',
                    dataIndex: 'mailDestinatario',
                    sortable: true,
                    width: 40
                },
                {
                    header: 'Data Creació',
                    dataIndex: 'fechaCreacion',
                    sortable: true,
                    format: 'd/m/Y',
                    xtype: 'datecolumn',
                    width: 25
                },
                {
                    header: 'Data Certificat',
                    dataIndex: 'fechaCertificado',
                    sortable: true,
                    format: 'd/m/Y',
                    xtype: 'datecolumn',
                    width: 25
                }];

            this.gridCertificados = new Ext.grid.GridPanel(
                {
                    frame: true,
                    loadMask: true,
                    flex: 1,
                    sortable: true,
                    viewConfig:
                        {
                            forceFit: true
                        },
                    store: ref.storeCertificados,
                    columns: userColumnsGridCertificados,
                    sm: this.checkboxSelectionModel,
                    bbar: new Ext.Toolbar(
                        {
                            items: [this.barraDeEstado]
                        }),
                    tbar: [this.botonPrevisualizarPDFCertificado, '-', this.botonAceptarCertificado, '-', this.botonEliminarCertificado]
                });
        }
    });