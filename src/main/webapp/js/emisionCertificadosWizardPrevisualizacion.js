Ext.ns('UJI.CEO');

UJI.CEO.EmisionCertificadosWizardPrevisualizacion = Ext.extend(Ext.Panel,
{
    fbar : [],
    border : false,
    texto : '',
    tipoCertificado : '',
    padding : 20,
    panelTextoCa : {},
    panelTextoEs : {},
    panelTextoUk : {},
    layout : 'fit',
    autoScroll : true,

    listeners :
    {
        activate : function()
        {
            var arrayEtiquetasCa = this.ownerCt.emisionCertificadosWizardPlantilla.panelComodinesCa.items.items;
            var arrayEtiquetasEs = this.ownerCt.emisionCertificadosWizardPlantilla.panelComodinesEs.items.items;
            var arrayEtiquetasUk = this.ownerCt.emisionCertificadosWizardPlantilla.panelComodinesUk.items.items;

            var textoPorProcesarCa = this.ownerCt.emisionCertificadosWizardPlantilla.plantillaCa.getValue();
            var textoPorProcesarEs = this.ownerCt.emisionCertificadosWizardPlantilla.plantillaEs.getValue();
            var textoPorProcesarUk = this.ownerCt.emisionCertificadosWizardPlantilla.plantillaUk.getValue();

            textoPorProcesarCa = this.remarcaEtiquetasNoRellenadasEnRojo(textoPorProcesarCa, arrayEtiquetasCa);
            textoPorProcesarEs = this.remarcaEtiquetasNoRellenadasEnRojo(textoPorProcesarEs, arrayEtiquetasEs);
            textoPorProcesarUk = this.remarcaEtiquetasNoRellenadasEnRojo(textoPorProcesarUk, arrayEtiquetasUk);

            this.panelTextoCa.update(this.procesaPlantillaConEtiquetas(textoPorProcesarCa, arrayEtiquetasCa));
            this.panelTextoEs.update(this.procesaPlantillaConEtiquetas(textoPorProcesarEs, arrayEtiquetasEs));
            this.panelTextoUk.update(this.procesaPlantillaConEtiquetas(textoPorProcesarUk, arrayEtiquetasUk));

        }
    },

    remarcaEtiquetasNoRellenadasEnRojo : function(textoPorProcesar, arrayEtiquetas)
    {
        for ( var i = 0; i < arrayEtiquetas.length; i++)
        {
            var tag = arrayEtiquetas[i].name;
            var valor = arrayEtiquetas[i].getValue();

            if (!valor)
            {
                textoPorProcesar = UJI.CEO.Application.replaceSpanRed(textoPorProcesar, tag);
            }
        }
        return textoPorProcesar;

    },

    procesaPlantillaConEtiquetas : function(textoPorProcesar, arrayEtiquetas)
    {
        for ( var i = 0; i < arrayEtiquetas.length; i++)
        {
            var tag = arrayEtiquetas[i].name;
            var valor = arrayEtiquetas[i].getValue();

            if (valor)
            {
                textoPorProcesar = UJI.CEO.Application.replaceAll(textoPorProcesar, tag, valor);
            }
        }
        return textoPorProcesar;
    },

    initComponent : function()
    {
        var config = {};

        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.CEO.EmisionCertificadosWizardPrevisualizacion.superclass.initComponent.call(this);

        this.initUI();

    },

    initUI : function()
    {
        this.buildButtonFinalizar();

        this.buildPanelTextoCa();
        this.buildPanelTextoEs();
        this.buildPanelTextoUk();

        this.add(
        {
            xtype : 'panel',
            layout : 'form',
            hideBorders : true,
            width : 700,
            padding : 10,
            autoScroll : true,
            items : [ this.panelTextoCa, this.panelTextoEs, this.panelTextoUk ]
        });

        this.getFooterToolbar().addFill();
        this.getFooterToolbar().addItem(this.buttonFinalizar);
    },

    buildPanelTextoCa : function()
    {
        this.panelTextoCa = new Ext.Panel(
        {
            title : "Previsualització en Català",
            padding : 10,
            margin : 10,
            height : 200,
            autoScroll : true
        });
    },

    buildPanelTextoEs : function()
    {
        this.panelTextoEs = new Ext.Panel(
        {
            title : 'Previsualización en Castellano',
            height : 200,
            padding : 10,
            margin : 10,
            autoScroll : true
        });
    },

    buildPanelTextoUk : function()
    {
        this.panelTextoUk = new Ext.Panel(
        {
            title : 'Preview in English',
            height : 200,
            padding : 10,
            margin : 10,
            autoScroll : true
        });
    },

    buildButtonFinalizar : function()
    {
        var ref = this;

        this.buttonFinalizar = new Ext.Button(
        {
            text : 'Finalitzar',
            handler : function()
            {
                ref.saveForm();
            }
        });
    },

    saveForm : function()
    {
        var ref = this;

        Ext.Msg.confirm('Emissió', '<b>Esteu segur/a de voler emetre el certificat ?</b><br/>Açò iniciarà el procediment de moderació.', function(btn, text)
        {
            if (btn == 'yes')
            {
                ref.submitForm();
            }
        });
    },

    submitForm : function()
    {
        var form = this.ownerCt.getForm();
        var ref = this;

        var arrayEtiquetasCa = this.ownerCt.emisionCertificadosWizardPlantilla.panelComodinesCa.items.items;
        var arrayEtiquetasEs = this.ownerCt.emisionCertificadosWizardPlantilla.panelComodinesEs.items.items;
        var arrayEtiquetasUk = this.ownerCt.emisionCertificadosWizardPlantilla.panelComodinesUk.items.items;

        var textoPorProcesarCa = this.ownerCt.emisionCertificadosWizardPlantilla.plantillaCa.getValue();
        var textoPorProcesarEs = this.ownerCt.emisionCertificadosWizardPlantilla.plantillaEs.getValue();
        var textoPorProcesarUk = this.ownerCt.emisionCertificadosWizardPlantilla.plantillaUk.getValue();

        var textoCa = this.procesaPlantillaConEtiquetas(textoPorProcesarCa, arrayEtiquetasCa);
        var textoEs = this.procesaPlantillaConEtiquetas(textoPorProcesarEs, arrayEtiquetasEs);
        var textoUk = this.procesaPlantillaConEtiquetas(textoPorProcesarUk, arrayEtiquetasUk);

        var descripcionCursoCA = this.ownerCt.emisionCertificadosWizardDescripcionYAnverso.inputFieldNuevaDescripcionCA.getValue();
        var descripcionCursoES = this.ownerCt.emisionCertificadosWizardDescripcionYAnverso.inputFieldNuevaDescripcionES.getValue();
        var descripcionCursoUK = this.ownerCt.emisionCertificadosWizardDescripcionYAnverso.inputFieldNuevaDescripcionUK.getValue();

        if (!descripcionCursoES) {
            descripcionCursoES = descripcionCursoCA;
        }

        if (!descripcionCursoUK) {
            descripcionCursoUK = descripcionCursoCA;
        }

        var textoAnversoCA = this.ownerCt.emisionCertificadosWizardDescripcionYAnverso.textAreaAnversoCA.getValue();
        var textoAnversoES = this.ownerCt.emisionCertificadosWizardDescripcionYAnverso.textAreaAnversoES.getValue();
        var textoAnversoUK = this.ownerCt.emisionCertificadosWizardDescripcionYAnverso.textAreaAnversoUK.getValue();

        var fechaCertificado = this.ownerCt.emisionCertificadosWizardPlantilla.dateField.getValue();
        var fechaInicioCertificado = this.ownerCt.emisionCertificadosWizardPlantilla.fechaInicioCertificadoField.getValue();

        var box = Ext.MessageBox.wait('Processant...');

        form.submit(
        {
            url : '/ceo/rest/certificado',
            method : 'post',
            clientValidation : false,
            params :
            {
                textoCa : textoCa,
                textoEs : textoEs,
                textoUk : textoUk,
                textoAnversoCA: textoAnversoCA,
                textoAnversoES: textoAnversoES,
                textoAnversoUK: textoAnversoUK,
                fechaCertificado: fechaCertificado.getDate() + "/" + ('0'+(fechaCertificado.getMonth()+1)).slice(-2) + "/" + fechaCertificado.getFullYear(),
                fechaInicioCertificado: fechaInicioCertificado.getDate() + "/" + ('0'+(fechaInicioCertificado.getMonth()+1)).slice(-2) + "/" + fechaInicioCertificado.getFullYear(),
                descripcionCursoCA: descripcionCursoCA,
                descripcionCursoES: descripcionCursoES,
                descripcionCursoUK: descripcionCursoUK,
                tipoCertificado : ref.tipoCertificado
            },
            success : function(form, action)
            {
                box.hide();
                ref.ownerCt.fireEvent("certificadoGuardado");
            },
            failure : function(form, action)
            {
                box.hide();
                Ext.MessageBox.show(
                {
                    title : 'Error',
                    msg : 'Errada al desar les dades...',
                    buttons : Ext.MessageBox.OK,
                    icon : Ext.MessageBox.ERROR
                });
            }
        });
    },

    checkMandatoryFields : function()
    {
        return true;
    },

    setTipoCertificado : function(tipoCertificado)
    {
        this.tipoCertificado = tipoCertificado;
    }
});