Ext.ns('UJI.CEO');

UJI.CEO.CrearCertificadoWindow = Ext.extend(Ext.Window,
{
    title : 'Creació Certificat',
    layout : 'fit',
    width : 760,
    height : 500,
    modal : true,
    resizable : true,
    wizard: {},

    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.CEO.CrearCertificadoWindow.superclass.initComponent.call(this);
        this.initUI();
    },

    initUI : function()
    {
        this.buildWizard();

        this.add(this.wizard);
    },

    buildWizard : function()
    {
        this.wizard = new UJI.CEO.EmisionCertificadosWizard();

        var ref = this;

        this.wizard.on("certificadoGuardado", function()
        {
            ref.fireEvent("certificadoGuardado");
        });
    }
});