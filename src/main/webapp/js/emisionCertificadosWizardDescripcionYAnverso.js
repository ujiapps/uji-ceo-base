Ext.ns('UJI.CEO');

UJI.CEO.EmisionCertificadosWizardDescripcionYAnverso = Ext.extend(Ext.Panel,
{
    layout : 'form',
    padding : 10,
    border : false,
    label : {},
    inputFieldNuevaDescripcionCA : {},
    inputFieldNuevaDescripcionES : {},
    inputFieldNuevaDescripcionUK : {},
    comboDescripcionesTiposCertificados : {},
    textAreaAnversoCA : {},
    textAreaAnversoES : {},
    textAreaAnversoUK : {},
    textPanelAnverso: {},
    tipoCertificadoId : null,
    storeDescripcionCurso : {},

    initComponent : function()
    {
        var config = {};

        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.CEO.EmisionCertificadosWizardDescripcionYAnverso.superclass.initComponent.call(this);

        this.initUI();
    },

    listeners :
    {
        activate : function()
        {
            this.storeDescripcionCurso.load();
        }
    },

    initUI : function()
    {
        this.buildLabel();
        this.buildStoreDescripcionCurso();
        this.buildComboDescripcionesTiposCertificados();
        this.buildInputFieldNuevaDescripcionCA();
        this.buildInputFieldNuevaDescripcionES();
        this.buildInputFieldNuevaDescripcionUK();
        this.buildTextAreaAnversoCA();
        this.buildTextAreaAnversoES();
        this.buildTextAreaAnversoUK();
        this.buildTabPanelDescripcionAnverso();
        this.add(this.label);
        this.add(new Ext.Spacer(
        {
            height : 30
        }));
        this.add(this.comboDescripcionesTiposCertificados);
        this.add(this.tabPanelDescripcionAnverso);
    },

    buildLabel : function()
    {
        this.label = new Ext.form.Label(
        {
            text : 'Completeu les dades de descripció del curs i premeu el botó "Següent"',
            margins :
            {
                bottom : '10'
            }
        });
    },

    buildStoreDescripcionCurso : function()
    {
        var ref = this;
        this.storeDescripcionCurso = new Ext.data.Store(
        {
            restful : true,
            url : '/ceo/rest/certificado/descripcionCursosYAnverso/',
            autoLoad : false,
            reader : new Ext.data.XmlReader(
            {
                record : 'Certificado',
                id : 'id'
            }, [ 'id', 'descripcionCursoCA', 'descripcionCursoES', 'descripcionCursoUK', 'textoAnversoCA', 'textoAnversoES', 'textoAnversoUK', 'descripcionYEdicion' ]),
            listeners :
            {
                beforeload : function()
                {
                    ref.storeDescripcionCurso.proxy.setUrl('/ceo/rest/certificado/descripcionCursosYAnverso/' + ref.tipoCertificadoId);
                }
            }

        });
    },

    buildComboDescripcionesTiposCertificados : function()
    {
        var ref = this;
        this.comboDescripcionesTiposCertificados = new Ext.form.ComboBox(
        {
            emptyText : 'Selecciona una descripció',
            name : 'descripcionCursoCA',
            width : 500,
            fieldLabel : 'Descripcions disponibles',
            forceSelection : true,
            editable : false,
            triggerAction : 'all',
            store : this.storeDescripcionCurso,
            valueField : 'id',
            displayField : 'descripcionYEdicion',
            allowBlank : false,
            listeners :
            {
                select : function(combo, record, index)
                {
                    var record = ref.storeDescripcionCurso.getAt(index);
                    ref.inputFieldNuevaDescripcionCA.setValue(record.get("descripcionCursoCA"));
                    ref.inputFieldNuevaDescripcionES.setValue(record.get("descripcionCursoES"));
                    ref.inputFieldNuevaDescripcionUK.setValue(record.get("descripcionCursoUK"));
                    ref.textAreaAnversoCA.setValue(record.get("textoAnversoCA"));
                    ref.textAreaAnversoES.setValue(record.get("textoAnversoES"));
                    ref.textAreaAnversoUK.setValue(record.get("textoAnversoUK"));
                }
            }
        });
    },

    buildInputFieldNuevaDescripcionCA : function()
    {
        this.inputFieldNuevaDescripcionCA = new Ext.form.TextField(
        {
            name : 'nuevaDescripcionCA',
            fieldLabel : 'Descripció',
            allowBlank : false,
            width : 500
        });
    },

    buildInputFieldNuevaDescripcionES : function()
    {
        this.inputFieldNuevaDescripcionES = new Ext.form.TextField(
            {
                name : 'nuevaDescripcionES',
                fieldLabel : 'Descripción',
                allowBlank : false,
                width : 500
            });
    },

    buildInputFieldNuevaDescripcionUK : function()
    {
        this.inputFieldNuevaDescripcionUK = new Ext.form.TextField(
            {
                name : 'nuevaDescripcionUK',
                fieldLabel : 'Description',
                allowBlank : false,
                width : 500
            });
    },

    buildTabPanelDescripcionAnverso: function()
    {
        this.tabPanelDescripcionAnverso = new Ext.TabPanel(
            {
                activeTab : 0,
                padding : 20,
                height: 400,
                items : [
                    {
                        xtype : 'panel',
                        layout : 'form',
                        title : 'Català',
                        padding : 10,
                        autoHeight : true,
                        items : [ this.inputFieldNuevaDescripcionCA, this.textAreaAnversoCA ]

                    },
                    {
                        xtype : 'panel',
                        layout : 'form',
                        title : 'Castellano',
                        padding : 10,
                        autoHeight : true,
                        items : [ this.inputFieldNuevaDescripcionES, this.textAreaAnversoES ]

                    },
                    {
                        xtype : 'panel',
                        layout : 'form',
                        title : 'English',
                        padding : 10,
                        autoHeight : true,
                        items : [ this.inputFieldNuevaDescripcionUK, this.textAreaAnversoUK ]

                    } ]
            });
    },

    buildTextAreaAnversoCA : function()
    {

        this.textAreaAnversoCA = new Ext.form.TextArea(
        {
            name : 'textoAnversoCA',
            fieldLabel : 'Text de l\'anvers',
            width : 500,
            height : 200
        });
    },

    buildTextAreaAnversoES : function()
    {

        this.textAreaAnversoES = new Ext.form.TextArea(
            {
                name : 'textoAnversoES',
                fieldLabel : 'Texto del anverso',
                width : 500,
                height : 200
            });
    },

    buildTextAreaAnversoUK : function()
    {

        this.textAreaAnversoUK = new Ext.form.TextArea(
            {
                name : 'textoAnversoUK',
                fieldLabel : 'Rear page text',
                width : 500,
                height : 200
            });
    },


    checkMandatoryFields : function()
    {
        return true;
    },

    setTipoCertificado : function(tipoCertificadoId)
    {
        this.tipoCertificadoId = tipoCertificadoId;
    },
    
    resetearValores: function() {
        this.inputFieldNuevaDescripcionCA.reset();
        this.inputFieldNuevaDescripcionES.reset();
        this.inputFieldNuevaDescripcionUK.reset();
        this.textAreaAnversoCA.reset();
        this.textAreaAnversoES.reset();
        this.textAreaAnversoUK.reset();
        this.comboDescripcionesTiposCertificados.reset();
    }
});