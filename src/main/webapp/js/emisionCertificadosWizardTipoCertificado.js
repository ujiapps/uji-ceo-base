Ext.ns('UJI.CEO');

UJI.CEO.EmisionCertificadosWizardTipoCertificado = Ext.extend(Ext.Panel,
{
    layout : 'form',
    padding : 10,
    border : false,

    initComponent : function()
    {
        var config = {};

        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.CEO.EmisionCertificadosWizardTipoCertificado.superclass.initComponent.call(this);

        this.initUI();
    },

    initUI : function()
    {
        this.buildStore();
        this.buildGrid();
        this.buildLabel();

        this.add(this.label);

        this.add(new Ext.Spacer(
        {
            height : 10
        }));

        this.add(this.grid);
    },

    buildLabel : function()
    {
        this.label = new Ext.form.Label(
        {
            text : 'Seleccioneu un tipus de certificat i premeu el botó de "Següent"',
            margins :
            {
                bottom : '10'
            }
        });
    },

    buildStore : function()
    {
        this.store = new Ext.data.Store(
        {
            restful : true,
            url : '/ceo/rest/tipocertificado/manual',
            autoLoad : true,
            reader : new Ext.data.XmlReader(
            {
                record : 'TipoCertificado',
                id : 'id'
            }, [ 'id', 'descripcionCa', 'descripcionEs', 'descripcionUk', 'plantillaCa', 'plantillaEs', 'plantillaUk', 'emisionManual' ]),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            })
        });
    },

    buildGrid : function()
    {
        var ref = this;

        var colModel = new Ext.grid.ColumnModel(
        {
            defaults :
            {
                sortable : true
            },
            columns : [
            {
                header : 'Id',
                width : 200,
                dataIndex : 'id',
                hidden : true
            },
            {
                header : 'Tipus de certificat',
                width : 440,
                dataIndex : 'descripcionCa',
                editor : new Ext.form.TextField(
                {
                    allowBlank : false
                })
            } ]
        });

        this.grid = new Ext.grid.GridPanel(
        {
            autoScroll : true,
            border : false,
            width : 722,
            height : 330,
            store : ref.store,
            colModel : colModel,
            sm : new Ext.grid.RowSelectionModel(
            {
                singleSelect : true,
                listeners :
                {
                    rowSelect : function(sm, index, record)
                    {
                        var plantillaCa = record.data.plantillaCa;
                        var plantillaEs = record.data.plantillaEs;
                        var plantillaUk = record.data.plantillaUk;
                        ref.ownerCt.emisionCertificadosWizardPlantilla.setTexto(plantillaCa, plantillaEs, plantillaUk);
                        ref.ownerCt.emisionCertificadosWizardPrevisualizacion.setTipoCertificado(record.id);
                        ref.ownerCt.emisionCertificadosWizardDescripcionYAnverso.setTipoCertificado(record.id);
                        ref.ownerCt.emisionCertificadosWizardDescripcionYAnverso.resetearValores();

                    }
                }
            }),
            viewConfig :
            {
                forceFit : true
            },
            listeners :
            {
                dblclick : function(e)
                {
                    var button = ref.ownerCt.adelanteButton;
                    button.handler.call(button.scope, button);
                }
            }
        });
    },

    checkMandatoryFields : function()
    {
        var tipoCertificado = this.grid.getSelectionModel().getSelected();

        if (tipoCertificado)
        {
            return true;
        }

        Ext.MessageBox.show(
        {
            title : 'Error',
            msg : 'És obligatori seleccionar un tipus de certificat',
            buttons : Ext.MessageBox.OK,
            icon : Ext.MessageBox.ERROR
        });
    }
});