UJI.CEO.Application =
{
    extractTags : function(texto)
    {
        var tags = texto.match(/\${[^}]+}/g);

        var result = [];

        if (tags)
        {
            for ( var i = 0; i < tags.length; i++)
            {
                if (result.indexOf(tags[i]) == -1)
                {
                    result.push(tags[i]);
                }
            }
        }

        return result;
    },

    replaceAll : function(text, search, newstring)
    {
        while (text.toString().indexOf(search) != -1)
        {
            text = text.toString().replace(search, newstring);
        }

        return text;
    },

    replaceSpanRed : function(text, searchTag)
    {
        var tagEscapado = searchTag.replace(/([.*+?^$|(){}\[\]])/mg, "\\$1");

        regexp = new RegExp(tagEscapado, "g");
        text = text.toString().replace(regexp,
                '<span style="color:red; font-weight: bold;">' + searchTag + "</span>");

        return text;
    },

    arrayIndexOf : function(items, item)
    {
        for ( var x = 0; x < items.length; x++)
        {
            if (items[x] == item)
            {
                return x;
            }
        }

        return -1;
    }
};