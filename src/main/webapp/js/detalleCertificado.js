Ext.ns('UJI.CEO');

UJI.CEO.detalleCertificado = Ext.extend(Ext.Panel,
{
    title : 'Detall Signatura i notificació',

    initComponent : function()
    {
        var config = {};
        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.CEO.detalleCertificado.superclass.initComponent.call(this);

        this.initUI();
        this.add(this.formFirma);
    },

    initUI : function()
    {
        this.buildFormFirma();
    },

    buildFormFirma : function()
    {
        this.formFirma = new Ext.form.FormPanel(
        {
            labelWidth : 85,
            frame : true,
            height: 290,

            reader : new Ext.data.XmlReader(
            {
                record : 'FirmaNotificacion'
            }, [ 'id', 'fechaEntradaNotificacion',
            {
                name : 'fecha',
                type : 'date',
                dateFormat : 'd/m/Y H:i:s'
            }, 'contenido', 'perId', 'xml', 'contenidoNotificacion', 'referencia', 'nombre',
                    'motivoRechazo', 'referenciaInterna', 'estado', 'perIdSolicitante',
                    {
                        name : 'fechaAceptaRechaza',
                        type : 'date',
                        dateFormat : 'd/m/Y H:i:s'
                    },
                    {
                        name : 'fechaFirma',
                        type : 'date',
                        dateFormat : 'd/m/Y H:i:s'
                    },
                    {
                        name : 'fechaEntradaNotificacion',
                        type : 'date',
                        dateFormat : 'd/m/Y H:i:s'
                    } ]),
            errorReader : new Ext.data.XmlReader(
            {
                record : 'responseMessage',
                success : 'success'
            }, [ 'success', 'msg' ]),
            items : [
            {
                xtype : 'fieldset',
                title : 'Signatura',
                flex: 1,
                layout : 'form',
                items : [
                {
                    fieldLabel : 'Entrada',
                    xtype : 'datefield',
                    name : 'fecha',
                    format : 'd/m/Y',
                    width : 100,
                    disabled : true
                },
                {
                    fieldLabel : 'Referència',
                    xtype : 'textfield',
                    name : 'referencia',
                    width : '90%',
                    disabled : true
                },
                {
                    fieldLabel : 'Signatura',
                    xtype : 'datefield',
                    name : 'fechaFirma',
                    format : 'd/m/Y',
                    width : 100,
                    disabled : true
                } ]
            },
            {
                xtype : 'fieldset',
                title : 'Notificació',
                layout : 'form',
                items : [
                {
                    fieldLabel : 'Entrada',
                    xtype : 'datefield',
                    name : 'fechaEntradaNotificacion',
                    format : 'd/m/Y',
                    width : 100,
                    disabled : true
                },
                {
                    fieldLabel : 'Acept/Rebuig',
                    xtype : 'datefield',
                    name : 'fechaAceptaRechaza',
                    format : 'd/m/Y',
                    width : 100,
                    disabled : true
                },
                {
                    fieldLabel : 'Motiu rebuig',
                    xtype : 'textfield',
                    name : 'motivoRechazo',
                    width : '90%',
                    disabled : true
                } ]
            } ]
        });
    },

    loadFirma : function(certificadoId)
    {
        var ref = this;

        ref.formFirma.getForm().load(
        {
            url : '/ceo/rest/firma/' + certificadoId,
            method : 'GET'
        });
    }
});