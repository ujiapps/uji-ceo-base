Ext.ns('UJI.CEO');

UJI.CEO.EmisionCertificados = Ext.extend(Ext.Panel,
{
    layout : 'vbox',
    title : 'Emissió de certificats',
    region : 'center',
    autoScroll : true,
    closable : true,

    botonImprimir : {},
    botonCrearCertificado : {},
    botonBorrarCertificado : {},
    gridCertificados : {},

    inputFechaRecogida : {},
    botonEditarFechaRecogida : {},
    ventanaFechaRecogida : {},
    formFechaRecogida : {},
    botonCancelar : {},
    botonGuardar : {},
    etiquetaFiltroTipoCertificado : {},
    storeTipoCertificado : {},
    filtroTipoCertificado : {},

    etiquetaFiltroDescripcionCurso : {},
    storeDescripcionCurso : {},
    storeAnyoCurso : {},
    filtroDescripcionCurso : {},
    botonLimpiarFiltros : {},
    checkboxSelectionModel : {},
    ventanaRechazoCertificado : {},
    botonMotivoRechazo : {},

    storeCertificados : {},
    panelCertificados : {},
    barraDeEstado : {},

    layoutConfig :
    {
        align : 'stretch'
    },

    initComponent : function()
    {
        var config = {};

        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.CEO.EmisionCertificados.superclass.initComponent.call(this);

        this.initUI();

        this.add(this.panelFiltros);
        this.add(this.gridCertificados);
        //this.add(this.panelCertificados);

    },

    initUI : function()
    {
        this.buildBotonLimpiarFiltros();
        this.buildInputFechaRecogida();
        this.buildBotonEditarFechaRecogida();
        this.buildBotonCancelar();
        this.buildBotonGuardar();
        this.buildFormFechaRecogida();
        this.buildVentanaFechaRecogida();
        this.buildBarraDeEstado();

        this.buildBotonImprimir();
        this.buildBotonCrearCertificado();
        this.buildBotonBorrarCertificado();

        this.buildEtiquetaFiltroDescripcionCurso();
        this.buildStoreDescripcionCurso();
        this.buildStoreAnyoCurso();
        this.buildFiltroDescripcionCurso();

        this.buildEtiquetaFiltroTipoCertificado();
        this.buildStoreTipoCertificado();
        this.buildFiltroTipoCertificado();

        this.buildEtiquetaFiltroAnyoCurso();
        this.buildFiltroAnyoCurso();

        this.buildBotonMotivoRechazo();
        this.buildVentanaRechazoCertificado();

        this.buildPanelDetalleFirma();
        this.buildPanelTextoCertificado();

        this.buildCheckboxSelectionModel();
        this.buildPanelCertificados();
        this.buildStoreCertificados();
        this.buildGridCertificados();
        this.buildPanelFiltros();

    },

    buildBarraDeEstado : function()
    {
        this.barraDeEstado = new Ext.Toolbar.TextItem(
        {
            id : 'num',
            text : 'Cap certificat sel·leccionat'
        });
    },

    imprimirCertificado : function(idioma)
    {
        var ref = this;
        var arraySeleccionadoEmision = ref.gridCertificados.getSelectionModel().getSelections();

        if (arraySeleccionadoEmision.length >= 1)
        {
            var flagModerado = true;
            for (var i = 0; i < arraySeleccionadoEmision.length; i++)
            {
                if (arraySeleccionadoEmision[i].get("moderacionAceptada") !== "true")
                {
                    flagModerado = false;
                    break;
                }
            }
            if (flagModerado === false)
            {
                Ext.MessageBox.show(
                {
                    title : 'Imprimir certificat',
                    msg : 'Algun certificat no es pot imprimir perque està pendent de moderar o ha sigut rebutjat',
                    buttons : Ext.MessageBox.OK,
                    icon : Ext.MessageBox.WARNING
                });
            }
            else
            {
                var listaCertificadosIds = [];

                for (var index = 0; index < arraySeleccionadoEmision.length; index++)
                {
                    if (arraySeleccionadoEmision[index].id)
                    {
                        listaCertificadosIds.push(arraySeleccionadoEmision[index].id);
                    }
                }

                idiomaId = idioma;
                window.open("/ceo/rest/pdf/?idiomaId=" + idiomaId + "&listaCertificadosIds=" + listaCertificadosIds);
            }
        }
    },

    buildBotonLimpiarFiltros : function()
    {
        var ref = this;
        this.botonLimpiarFiltros = new Ext.Button(
        {
            text : 'Netejar filtres',
            handler : function(button, event)
            {
                ref.filtroDescripcionCurso.reset();
                ref.filtroTipoCertificado.reset();
                ref.filtroAnyoCurso.reset();
                ref.storeCertificados.clearData();
                ref.gridCertificados.view.refresh();
            }
        });
    },

    buildBotonImprimir : function()
    {
        var ref = this;

        this.botonImprimir = new Ext.Button(
        {
            text : 'Imprimir',
            iconCls : 'printer',
            disabled : true,
            menu :
            {
                xtype : 'menu',
                border : false,
                plain : true,
                items : [
                {
                    text : 'Català',
                    handler : function()
                    {
                        var idioma = 'CA';
                        ref.imprimirCertificado(idioma);
                    }
                },
                {
                    text : 'Castellà',
                    handler : function()
                    {
                        var idioma = 'ES';
                        ref.imprimirCertificado(idioma);
                    }
                },
                {
                    text : 'Anglès',
                    handler : function()
                    {
                        var idioma = 'EN';
                        ref.imprimirCertificado(idioma);
                    }
                } ]
            }
        });
    },

    buildBotonCrearCertificado : function()
    {
        var ref = this;

        this.botonCrearCertificado = new Ext.Button(
        {
            text : 'Crear Nou',
            iconCls : 'vcard',
            handler : function()
            {
                var crearCertificadoWindow = new UJI.CEO.CrearCertificadoWindow();
                crearCertificadoWindow.show();

                crearCertificadoWindow.on("certificadoGuardado", function()
                {
                    crearCertificadoWindow.hide();
                    var tipoCertificadoSeleccionado = ref.filtroTipoCertificado.getValue();
                    if (tipoCertificadoSeleccionado != '')
                    {
                        ref.storeCertificados.load(
                        {
                            params :
                            {
                                tipoCertificadoId : tipoCertificadoSeleccionado
                            }
                        });
                    }
                });
            }
        });
    },

    buildBotonBorrarCertificado : function()
    {
        var ref = this;

        this.botonBorrarCertificado = new Ext.Button(
        {
            text : 'Esborrar',
            iconCls : 'application-delete',
            disabled : true,
            handler : function(boton, evento)
            {
                var sm = ref.gridCertificados.getSelectionModel();
                var seleccionados = sm.getSelections();

                if (!seleccionados)
                {
                    Ext.MessageBox.show(
                    {
                        title : 'Esborrar certificat',
                        msg : 'Cal seleccionar al menys un certificat a esborrar',
                        buttons : Ext.MessageBox.OK,
                        icon : Ext.MessageBox.WARNING
                    });
                }
                else
                {
                    for (var index = 0; index < seleccionados.length; index++)
                    {
                        if (seleccionados[index].data.moderacionAceptada)
                        {
                            Ext.MessageBox.show(
                            {
                                title : 'Esborrar Certificat',
                                msg : 'El/s certificat/s escollits no es poden esborrar, perquè algun ja ha sigut moderat',
                                buttons : Ext.MessageBox.OK,
                                icon : Ext.MessageBox.WARNING
                            });
                            return;
                        }
                    }

                    Ext.Msg.confirm("Corfirmació de la operació", "Realment dessitja esborrar aquest/s certificat/s?", function(button, text)
                    {
                        if (button == "yes")
                        {

                            var listaCertificadosIds = [];

                            for (index in seleccionados)
                            {
                                if (seleccionados[index].id)
                                {
                                    listaCertificadosIds.push(seleccionados[index].id);
                                }
                            }

                            var tipoCertificadoId = ref.filtroTipoCertificado.getValue();
                            Ext.Ajax.request(
                            {
                                url : '/ceo/rest/certificado/pendientes/tipoCertificado/' + tipoCertificadoId + '/borrar',
                                method : 'POST',
                                params :
                                {
                                    listaCertificadosIds : listaCertificadosIds,
                                    moderar : true
                                },
                                success : function()
                                {
                                    var tipoCertificadoId = ref.filtroTipoCertificado.getValue();
                                    var descripcionCurso = ref.filtroDescripcionCurso.getRawValue();
                                    ref.storeCertificados.load(
                                    {
                                        params :
                                        {
                                            tipoCertificadoId : tipoCertificadoId,
                                            descripcionCurso : descripcionCurso
                                        },
                                        callback : function(records, operation, success)
                                        {
                                            if (records.length === 0)
                                            {
                                                ref.filtroDescripcionCurso.reset();
                                                ref.filtroTipoCertificado.reset();
                                            }
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            }
        });
    },

    buildBotonMotivoRechazo : function()
    {
        var ref = this;

        this.botonMotivoRechazo = new Ext.Button(
        {
            text : 'Motiu de rebuig',
            iconCls : 'application-delete',
            disabled : true,
            handler : function()
            {
                var registroSeleccionadoRechazo = ref.gridCertificados.getSelectionModel().getSelected();
                if (!registroSeleccionadoRechazo)
                {
                    Ext.MessageBox.show(
                    {
                        title : 'Rebutjar certificat',
                        msg : 'Cal seleccionar un/s certificat/s a rebutjar',
                        buttons : Ext.MessageBox.OK,
                        icon : Ext.MessageBox.WARNING
                    });
                }
                else
                {
                    ref.ventanaRechazoCertificado.setMensaje(registroSeleccionadoRechazo.get('moderacionMotivoRechazo'));
                    ref.ventanaRechazoCertificado.show();
                }
            }
        });
    },

    buildVentanaRechazoCertificado : function()
    {
        var ref = this;
        this.ventanaRechazoCertificado = new Ext.Window(
        {
            title : 'Motiu de rebuig',
            layout : 'fit',
            modal : true,
            record : false,
            width : 500,
            height : 300,
            closeAction : 'hide',
            closable : true,
            items : [
            {
                xtype : 'panel',
                padding : 5,
                flex : 1,
                layout : 'vbox',
                items : [
                {
                    xtype : 'textarea',
                    title : "Missatge de rebuig",
                    flex : 1,
                    readOnly : true,
                    width : 470
                } ]
            } ],
            buttons : [
            {
                xtype : 'button',
                text : 'Acceptar',
                handler : function(btn, evt)
                {
                    ref.ventanaRechazoCertificado.hide();
                }
            } ],
            setMensaje : function(mensaje)
            {
                var cajaMensajeRechazo = this.findByType('textarea')[0];
                cajaMensajeRechazo.setValue(mensaje);
            }
        });
    },

    buildStoreTipoCertificado : function()
    {
        this.storeTipoCertificado = new Ext.data.Store(
        {
            restful : true,
            url : '/ceo/rest/tipocertificado',
            autoLoad : true,
            reader : new Ext.data.XmlReader(
            {
                record : 'TipoCertificado',
                id : 'id'
            }, [ 'id', 'descripcionCa', 'descripcionEs', 'descripcionUk' ])
        });
    },

    buildEtiquetaFiltroTipoCertificado : function()
    {
        this.etiquetaFiltroTipoCertificado = new Ext.form.Label(
        {
            text : 'Tipus de certificat'
        });
    },

    buildFiltroTipoCertificado : function()
    {
        var ref = this;

        this.filtroTipoCertificado = new Ext.form.ComboBox(
        {
            store : this.storeTipoCertificado,
            displayField : 'descripcionCa',
            emptyText : 'Selecciona un Tipus de certificat',
            fieldLabel : 'Tipus',
            editable : false,
            flex : 1,
            listWidth : 400,
            triggerAction : 'all',
            mode : 'local',
            valueField : 'id',
            name : 'filtroTipoCertificado',
            listeners :
            {
                select : function(field)
                {
                    var tipoCertificadoId = ref.filtroTipoCertificado.getValue();
                    var descripcionCurso = ref.filtroDescripcionCurso.getRawValue();
                    var anyo = ref.filtroAnyoCurso.getRawValue();
                    ref.storeCertificados.load(
                    {
                        params :
                        {
                            tipoCertificadoId : tipoCertificadoId,
                            descripcionCurso : descripcionCurso,
                            anyo : anyo
                        }
                    });
                }
            }
        });
    },

    buildStoreAnyoCurso : function()
    {
        var ref = this;
        this.storeAnyoCurso = new Ext.data.Store(
        {
            restful : true,
            url : '/ceo/rest/certificado/anyos/',
            autoLoad : false,
            reader : new Ext.data.XmlReader(
            {
                record : 'Anyo',
                id : 'id'
            }, [ 'id', 'anyo' ])
        });
    },

    buildStoreDescripcionCurso : function()
    {
        var ref = this;
        this.storeDescripcionCurso = new Ext.data.Store(
        {
            restful : true,
            url : '/ceo/rest/certificado/descripcionCursos/',
            autoLoad : false,
            listeners :
            {
                beforeLoad : function()
                {
                    ref.storeDescripcionCurso.setBaseParam('anyo', ref.filtroAnyoCurso.getRawValue())
                }
            },
            reader : new Ext.data.XmlReader(
            {
                record : 'Certificado',
                id : 'id'
            }, [ 'id', 'descripcionCurso' ])
        });
    },

    buildEtiquetaFiltroDescripcionCurso : function()
    {
        this.etiquetaFiltroDescripcionCurso = new Ext.form.Label(
        {
            text : 'Descripció curs'
        });
    },

    buildFiltroDescripcionCurso : function()
    {
        var ref = this;

        this.filtroDescripcionCurso = new Ext.form.ComboBox(
        {
            store : this.storeDescripcionCurso,
            displayField : 'descripcionCurso',
            emptyText : 'Selecciona un Curs',
            fieldLabel : 'Descripcio',
            editable : false,
            width : 150,
            listWidth : 500,
            triggerAction : 'all',
            mode : 'remote',
            valueField : 'id',
            name : 'filtroDescripcionCurso',
            listeners :
            {
                beforequery : function(qe)
                {
                    delete qe.combo.lastQuery;
                },
                select : function(combo, record)
                {
                    var descripcion = record.get("descripcionCurso");
                    var tipoCertificadoId = ref.filtroTipoCertificado.getValue();
                    var anyo = ref.filtroAnyoCurso.getRawValue();
                    ref.storeCertificados.load(
                    {
                        params :
                        {
                            tipoCertificadoId : tipoCertificadoId,
                            descripcionCurso : descripcion,
                            anyo : anyo
                        }
                    });
                }
            }
        });
    },

    buildEtiquetaFiltroAnyoCurso : function()
    {
        this.etiquetaFiltroAnyoCurso = new Ext.form.Label(
        {
            text : 'Any'
        });
    },

    buildFiltroAnyoCurso : function()
    {
        var ref = this;

        this.filtroAnyoCurso = new Ext.form.ComboBox(
        {
            store : this.storeAnyoCurso,
            displayField : 'anyo',
            emptyText : 'Selecciona un Any',
            editable : false,
            fieldLabel : 'Any',
            width : 150,
            triggerAction : 'all',
            mode : 'remote',
            valueField : 'id',
            name : 'filtroAnyoCurso',
            listeners :
            {
                select : function(combo, record)
                {
                    var descripcionCurso = ref.filtroDescripcionCurso.getRawValue();
                    var tipoCertificadoId = ref.filtroTipoCertificado.getValue();
                    var anyo = ref.filtroAnyoCurso.getRawValue();
                    ref.storeCertificados.load(
                    {
                        params :
                        {
                            tipoCertificadoId : tipoCertificadoId,
                            descripcionCurso : descripcionCurso,
                            anyo : anyo
                        }
                    });
                }
            }
        });
    },

    buildStoreCertificados : function()
    {
        this.storeCertificados = new Ext.data.Store(
        {
            restful : true,
            url : '/ceo/rest/certificado',
            autoLoad : false,
            reader : new Ext.data.XmlReader(
            {
                record : 'Certificado',
                id : 'id'
            }, [ 'id', 'destinatarioId', 'dniDestinatario', 'nombreDestinatario', 'mailDestinatario', 'moderacionAceptada', 'moderacionMotivoRechazo', 'referenciaFirma',
            {
                name : 'fechaCreacion',
                type : 'date',
                dateFormat : 'd/m/Y H:i:s'
            },
            {
                name : 'fechaRevision',
                type : 'date',
                dateFormat : 'd/m/Y H:i:s'
            },
            {
                name : 'fechaCertificado',
                type : 'date',
                dateFormat : 'd/m/Y H:i:s'
            },
            {
                name : 'fechaModeracion',
                type : 'date',
                dateFormat : 'd/m/Y H:i:s'
            },
            {
                name : 'fechaRecogida',
                type : 'date',
                dateFormat : 'd/m/Y H:i:s'
            }, 'tipoCertificado', 'textoCa', 'textoEs', 'textoUk' ]),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8'
            }, [ 'id', 'fechaRecogida' ])
        });
    },

    buildCheckboxSelectionModel : function()
    {
        var ref = this;
        this.checkboxSelectionModel = new Ext.grid.CheckboxSelectionModel(
        {
            listeners :
            {
                selectionchange : function(sm)
                {

                    var num = ref.gridCertificados.getSelectionModel().getCount();
                    if (num > 1)
                    {
                        ref.barraDeEstado.setText(num + ' certificats sel·leccionats');
                    }
                    else if (num === 1)
                    {
                        ref.barraDeEstado.setText(num + ' certificat sel·leccionat');
                    }
                    else
                    {
                        ref.barraDeEstado.setText('Cap certificat sel·leccionat');
                    }

                    filasSeleccionadas = sm.getSelections();

                    // this.panelDetalleFirma.formFirma.getForm().reset();
                    //ref.panelTextoCertificado.update();

                    if (filasSeleccionadas.length === 0)
                    {
                        ref.botonImprimir.disable();
                        ref.botonBorrarCertificado.disable();
                        ref.botonEditarFechaRecogida.disable();
                        ref.botonMotivoRechazo.disable();
                    }
                    else if (filasSeleccionadas.length === 1)
                    {

                        var r = filasSeleccionadas[0];

                        if (!r.data.moderacionAceptada)
                        {
                            ref.botonBorrarCertificado.enable();
                            ref.botonMotivoRechazo.disable();
                            return;
                        }

                        ref.botonBorrarCertificado.disable();
                        if (r.data.moderacionAceptada === "true")
                        {
                            ref.botonImprimir.enable();
                            ref.botonEditarFechaRecogida.enable();
                            ref.botonMotivoRechazo.disable();
                        }
                        else
                        {
                            ref.botonImprimir.disable();
                            ref.botonEditarFechaRecogida.disable();
                            ref.botonMotivoRechazo.enable();
                        }

                        // this.panelDetalleFirma.loadFirma(r.data.id);
                        //ref.panelTextoCertificado.update(r.data.textoCa);
                    }
                    else
                    {
                        var diferentes = false;
                        var r = filasSeleccionadas[0];

                        for (var i = 1; i < filasSeleccionadas.length; i++)
                        {
                            var r2 = filasSeleccionadas[i];
                            if (r.data.moderacionAceptada !== r2.data.moderacionAceptada)
                            {
                                ref.botonImprimir.disable();
                                ref.botonBorrarCertificado.disable();
                                return;

                            }
                        }

                        if (r.data.moderacionAceptada === "true")
                        {
                            ref.botonImprimir.enable();
                            ref.botonBorrarCertificado.disable();
                        }
                        else if (r.data.moderacionAceptada === "false")
                        {
                            ref.botonImprimir.disable();
                            ref.botonBorrarCertificado.disable();
                        }
                        else
                        {
                            ref.botonImprimir.disable();
                            ref.botonBorrarCertificado.enable();
                        }
                    }

                }
            }
        });
    },

    buildGridCertificados : function()
    {
        var ref = this;
        var userColumnsGridCertificados = [ this.checkboxSelectionModel,
        {
            header : 'Estat',
            dataIndex : 'moderacionAceptada',
            width : 10,
            renderer : function(value, p, record)
            {
                if (record.data.moderacionAceptada == 'true')
                {
                    return '<img src="/ceo/img/bola-verde.png" alt="Certificat aprovat" title="Certificat aprovat" />';
                }
                if (record.data.moderacionAceptada == 'false')
                {
                    return '<img src="/ceo/img/bola-roja.png" alt="Certificat rebutjat" title="Certificat rebutjat" />';
                }
                if (record.data.fechaRevision === null)
                {
                    return '<img src="/ceo/img/bola-marron.png" alt="Certificat pendent de revisar" title="Certificat pendent de revisar" />';
                }
                if (record.data.moderacionAceptada == "")
                {
                    return '<img src="/ceo/img/bola-naranja.png" alt="Certificat pendent de moderar" title="Certificat pendent de moderar" />';
                }
            }
        },
        {
            header : 'Id',
            dataIndex : 'id',
            sortable : true,
            width : 20,
            hidden : true
        },
        {
            header : 'Destinatari',
            dataIndex : 'destinatarioId',
            sortable : true,
            width : 20,
            hidden : true
        },
        {
            header : 'DNI Destinatari',
            dataIndex : 'dniDestinatario',
            sortable : true,
            width : 30
        },
        {
            header : 'Nom Destinatari',
            dataIndex : 'nombreDestinatario',
            sortable : true,
            width : 40
        },
        {
            header : 'Mail Destinatari',
            dataIndex : 'mailDestinatario',
            sortable : true,
            width : 40
        },
        {
            header : 'Data Creació',
            dataIndex : 'fechaCreacion',
            sortable : true,
            format : 'd/m/Y',
            xtype : 'datecolumn',
            width : 25
        },
        {
            header : 'Data Certificat',
            dataIndex : 'fechaCertificado',
            sortable : true,
            format : 'd/m/Y',
            xtype : 'datecolumn',
            width : 25
        },
        {
            header : 'Data Moderació',
            dataIndex : 'fechaModeracion',
            sortable : true,
            format : 'd/m/Y',
            xtype : 'datecolumn',
            width : 25
        },

        {
            header : 'Data Recollida',
            dataIndex : 'fechaRecogida',
            sortable : true,
            format : 'd/m/Y',
            xtype : 'datecolumn',
            width : 25
        } ];

        this.gridCertificados = new Ext.grid.GridPanel(
        {
            frame : true,
            loadMask : true,
            flex : 1,
            sortable : true,
            viewConfig :
            {
                forceFit : true
            },
            store : ref.storeCertificados,
            columns : userColumnsGridCertificados,
            sm : this.checkboxSelectionModel,
            tbar : [ this.botonCrearCertificado, '-', this.botonImprimir, '-', this.botonBorrarCertificado, this.botonEditarFechaRecogida, this.botonMotivoRechazo ],
            bbar : new Ext.Toolbar(
            {
                items : [ this.barraDeEstado ]
            }),

            listeners :
            {
                'rowdblclick' : function(grid, rowIndex, event)
                {
                    ref.botonEditarFechaRecogida.handler.call(ref.botonEditarFechaRecogida.scope, ref.botonEditarFechaRecogida, Ext.EventObject);
                }
            }
        });

    },
    buildPanelFiltros : function()
    {
        this.panelFiltros = new Ext.form.FormPanel(
        {
            title : 'Filtres',
            layout : 'table',
            frame : true,
            border : false,
            padding : '5px 0px',
            labelWidth : 65,
            defaults :
            {
                bodyStyle : 'padding:0px 10px'
            },
            layoutConfig :
            {
                columns : 4
            },
            items : [
            {
                layout : 'form',
                items : [ this.filtroTipoCertificado ]
            },
            {
                layout : 'form',
                items : [ this.filtroDescripcionCurso ]
            },
            {
                layout : 'form',
                items : [ this.filtroAnyoCurso ]
            },
            {
                layout : 'form',
                items : [ this.botonLimpiarFiltros ]
            } ]
        });
    },

    buildBotonEditarFechaRecogida : function()
    {
        var ref = this;

        this.botonEditarFechaRecogida = new Ext.Button(
        {
            text : 'Editar Data Recollida',
            iconCls : 'application-edit',
            disabled : true,
            handler : function(btn, evt)
            {
                var record = ref.gridCertificados.getSelectionModel().getSelections()[0];
                if (record && record.get("moderacionAceptada") === "true")
                {
                    if (record.get("fechaRecogida"))
                    {
                        ref.inputFechaRecogida.setValue(record.get("fechaRecogida"));
                    }
                    else
                    {
                        ref.inputFechaRecogida.setValue(new Date());
                    }
                    ref.ventanaFechaRecogida.show();
                }
            }
        });
    },

    buildBotonGuardar : function()
    {

        var ref = this;
        this.botonGuardar = new Ext.Button(
        {
            text : 'Guardar',
            handler : function(btn, evt)
            {
                var record = ref.gridCertificados.getSelectionModel().getSelections()[0];
                record.set("fechaRecogida", ref.inputFechaRecogida.getValue());
                ref.storeCertificados.commitChanges();
                ref.ventanaFechaRecogida.hide();
            }
        });
    },

    buildBotonCancelar : function()
    {

        var ref = this;

        this.botonCancelar = new Ext.Button(
        {
            text : 'Cancelar',
            handler : function(btn, evt)
            {
                ref.ventanaFechaRecogida.hide();
            }
        });

    },

    buildInputFechaRecogida : function()
    {
        this.inputFechaRecogida = new Ext.DatePicker();
    },

    buildFormFechaRecogida : function()
    {

        this.formFechaRecogida = new Ext.FormPanel(
        {
            labelWidth : 60,
            frame : true,
            bodyStyle : 'padding:5px 5px 0',
            width : 350,
            defaultType : 'textfield',

            plain : true,
            items : [ this.inputFechaRecogida ]
        });

    },

    buildVentanaFechaRecogida : function()
    {
        var ref = this;

        this.ventanaFechaRecogida = new Ext.Window(
        {
            title : 'Data recollida',
            layout : 'fit',
            modal : true,
            record : false,
            width : 210,
            height : 280,
            closeAction : 'hide',
            closable : true,
            items : [ ref.formFechaRecogida ],
            buttons : [ ref.botonGuardar, ref.botonCancelar ]

        });
    },

    buildPanelDetalleFirma : function()
    {
        this.panelDetalleFirma = new UJI.CEO.detalleCertificado(
        {
            layout : 'fit',
            flex : 1
        });
    },

    buildPanelTextoCertificado : function()
    {
        this.panelTextoCertificado = new Ext.Panel(
        {
            layout : 'fit',
            flex : 1,
            title : 'Text Certificat',
            autoScroll : true,
            padding : 10
        });
    },

    buildPanelCertificados : function()
    {
        this.panelCertificados = new Ext.Panel(
        {
            layout : 'hbox',
            height : 290,
            frame : true,
            flex : 1,
            items : [ this.panelTextoCertificado, this.panelDetalleFirma ]
        });
    }
});