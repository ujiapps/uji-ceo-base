Ext.ns('UJI.CEO');

UJI.CEO.EmisionCertificadosWizardDestinatario = Ext.extend(Ext.Panel,
{
    layout : 'form',
    padding : 10,
    border : false,

    initComponent : function()
    {
        var config = {};

        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.CEO.EmisionCertificadosWizardDestinatario.superclass.initComponent.call(this);

        this.initUI();
    },

    initUI : function()
    {
        this.buildRadioTipoUsuario();
        this.buildFieldSetUsuarioExterno();
        this.buildFieldSetUsuarioInterno();

        this.add(this.radioTipoUsuario);
        this.add(this.fieldSetUsuarioExterno);
        this.add(this.fieldSetUsuarioInterno);

        this.buildTextFieldDNIUsuarioExterno();
        this.buildTextFieldDestinatarioUsuarioExterno();
        this.buildTextFieldEMailUsuarioExterno();

        this.buildTextFieldDNIUsuarioInterno();
        this.buildTextFieldDestinatarioUsuarioInterno();
        this.buildTextFieldEMailUsuarioInterno();

        this.fieldSetUsuarioExterno.add(this.textFieldDNIUsuarioExterno);
        this.fieldSetUsuarioExterno.add(this.textFieldDestinatarioUsuarioExterno);
        this.fieldSetUsuarioExterno.add(this.textFieldEMailUsuarioExterno);

        this.fieldSetUsuarioInterno.add(this.textFieldDNIUsuarioInterno);
        this.fieldSetUsuarioInterno.add(this.textFieldDestinatarioUsuarioInterno);
        this.fieldSetUsuarioInterno.add(this.textFieldEMailUsuarioInterno);
    },

    buildRadioTipoUsuario : function()
    {
        var ref = this;

        this.radioTipoUsuario = new Ext.form.RadioGroup(
        {
            fieldLabel : "Tipus d'usuari",
            anchor : '35%',
            items : [
            {
                boxLabel : 'Intern',
                name : 'tipoUsuario',
                inputValue : UJI.CEO.TipoUsuario.INTERNO,
                checked : true
            },
            {
                boxLabel : 'Extern',
                name : 'tipoUsuario',
                inputValue : UJI.CEO.TipoUsuario.EXTERNO
            } ],
            listeners :
            {
                change : function(radioGroup, radio)
                {
                    if (radio.inputValue == UJI.CEO.TipoUsuario.EXTERNO)
                    {
                        ref.fieldSetUsuarioExterno.setVisible(true);
                        ref.fieldSetUsuarioInterno.setVisible(false);
                    }
                    else
                    {
                        ref.fieldSetUsuarioExterno.setVisible(false);
                        ref.fieldSetUsuarioInterno.setVisible(true);
                    }
                }
            }
        });
    },

    buildFieldSetUsuarioExterno : function()
    {
        this.fieldSetUsuarioExterno = new Ext.form.FieldSet(
        {
            flex : 1,
            border : false,
            width : '100%',
            labelWidth : 60,
            padding : 5,
            hidden : true
        });
    },

    buildFieldSetUsuarioInterno : function()
    {
        this.fieldSetUsuarioInterno = new Ext.form.FieldSet(
        {
            flex : 1,
            border : false,
            width : '100%',
            labelWidth : 60,
            padding : 5
        });
    },

    buildTextFieldDNIUsuarioExterno : function()
    {
        var ref = this;

        this.textFieldDNIUsuarioExterno = new Ext.form.TextField(
        {
            name : 'DNIUsuarioExterno',
            fieldLabel : 'DNI',
            allowBlank : false,
            width : 225,
            maxLength : 20,
            maxLengthText : 'Text massa llarg, es pot produir una errada al desar les dades',
            listeners :
            {
                change : function(field, newValue, oldValue)
                {
                    Ext.Ajax.request(
                    {
                        url : '/ceo/rest/persona/',
                        method : 'GET',
                        params :
                        {
                            dni : newValue.trim()
                        },
                        success : function(response)
                        {
                            ref.actualizaCamposDeUsuarioInterno(response);
                        }
                    });
                }
            }
        });
    },

    actualizaCamposDeUsuarioInterno : function(response)
    {
        var ref = this;

        var persona = response.responseXML;
        var dni = Ext.DomQuery.selectValue("identificacion", persona);

        if (dni)
        {
            var email = Ext.DomQuery.selectValue("mail", persona);
            var nombre = Ext.DomQuery.selectValue("nombreCompleto", persona);

            if (email)
            {
                email = email.trim();
            }

            ref.textFieldDestinatarioUsuarioExterno.setValue(nombre);
            ref.textFieldEMailUsuarioExterno.setValue(email);

            ref.textFieldDestinatarioUsuarioExterno.setDisabled(false);
        }
        else
        {
            ref.textFieldDestinatarioUsuarioExterno.setDisabled(false);
        }
    },

    buildTextFieldDestinatarioUsuarioExterno : function()
    {
        this.textFieldDestinatarioUsuarioExterno = new Ext.form.TextField(
        {
            name : 'DestinatarioUsuarioExterno',
            hiddenName : 'DestinatarioUsuarioExterno',
            allowBlank : false,
            fieldLabel : 'Nom',
            width : '98%',
            maxLength : 194,
            maxLengthText : 'Text massa llarg, es pot produir una errada al desar les dades'
        });
    },

    buildTextFieldEMailUsuarioExterno : function()
    {
        this.textFieldEMailUsuarioExterno = new Ext.form.TextField(
        {
            name : 'EMailUsuarioExterno',
            allowBlank : false,
            fieldLabel : 'Email',
            width : '98%',
            maxLength : 194,
            maxLengthText : 'Text massa llarg, es pot produir una errada al desar les dades',
            vtype : 'email'
        });
    },

    buildTextFieldDNIUsuarioInterno : function()
    {
        var ref = this;

        this.textFieldDNIUsuarioInterno = new Ext.ux.uji.form.LookupComboBox(
        {
            name : 'DNIUsuarioInterno',
            hiddenName : 'DNIUsuarioInterno',
            fieldLabel : 'DNI',
            appPrefix : 'ceo',
            bean : 'persona',
            extraFields : [ 'DNI', 'EMail' ],
            windowWidth : 700,
            displayField : 'DNI',
            width : 200,
            allowBlank : false
        });

        this.textFieldDNIUsuarioInterno.on('LookoupWindowClickSeleccion', function(record)
        {
            ref.actualizaValoresDeLosCamposDeUsuarioInterno(record);
        });
    },

    buildTextFieldDestinatarioUsuarioInterno : function()
    {
        var ref = this;

        this.textFieldDestinatarioUsuarioInterno = new Ext.ux.uji.form.LookupComboBox(
        {
            name : 'DestinatarioUsuarioInterno',
            hiddenName : 'DestinatarioUsuarioInterno',
            fieldLabel : 'Nom',
            width : 599,
            appPrefix : 'ceo',
            bean : 'persona',
            extraFields : [ 'DNI', 'EMail' ],
            windowWidth : 700,
            displayField : 'nombre',
            allowBlank : false
        });

        this.textFieldDestinatarioUsuarioInterno.on('LookoupWindowClickSeleccion', function(record)
        {
            ref.actualizaValoresDeLosCamposDeUsuarioInterno(record);
        });
    },

    buildTextFieldEMailUsuarioInterno : function()
    {
        this.textFieldEMailUsuarioInterno = new Ext.form.TextField(
        {
            name : 'EMailUsuarioInterno',
            allowBlank : false,
            fieldLabel : 'Email',
            width : '98%',
            maxLength : 194,
            maxLengthText : 'Text massa llarg, es pot produir una errada al desar les dades',
            vtype : 'email'
        });
    },

    actualizaValoresDeLosCamposDeUsuarioInterno : function(record)
    {
        this.actualizaValorEnCombo(record, this.textFieldDestinatarioUsuarioInterno);
        this.actualizaValorEnCombo(record, this.textFieldDNIUsuarioInterno);

        this.textFieldEMailUsuarioInterno.setValue(record.data.EMail);
    },

    actualizaValorEnCombo : function(record, combo)
    {
        combo.store.removeAll();
        combo.clearValue();

        combo.store.add([ record ]);
        combo.store.commitChanges();

        combo.setValue(record.id);
    },

    checkMandatoryFields : function()
    {
        var tipoUsuario = this.radioTipoUsuario.getValue().inputValue;
        var dni = "";
        var nombre = "";
        var email = "";

        if (tipoUsuario == UJI.CEO.TipoUsuario.INTERNO)
        {
            dni = this.textFieldDNIUsuarioInterno;
            nombre = this.textFieldDestinatarioUsuarioInterno;
            email = this.textFieldEMailUsuarioInterno;
        }
        else
        {
            dni = this.textFieldDNIUsuarioExterno;
            nombre = this.textFieldDestinatarioUsuarioExterno;
            email = this.textFieldEMailUsuarioExterno;
        }

        if (!dni.getValue())
        {
            dni.markInvalid();
        }
        if (!nombre.getValue())
        {
            nombre.markInvalid();
        }
        if (!email.getValue())
        {
            email.markInvalid();
        }

        if (dni.getValue() && nombre.getValue() && email.getValue())
        {
            return true;
        }

        Ext.MessageBox.show(
        {
            title : 'Error',
            msg : 'És obligatori reomplir tots els camps',
            buttons : Ext.MessageBox.OK,
            icon : Ext.MessageBox.ERROR
        });

    }
});