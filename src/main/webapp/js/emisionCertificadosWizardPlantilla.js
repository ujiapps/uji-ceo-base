Ext.ns('UJI.CEO');

UJI.CEO.EmisionCertificadosWizardPlantilla = Ext.extend(Ext.Panel,
{
    layout : 'form',
    padding : 10,
    border : 0,
    labelWidth : 65,
    textoCa : '',
    textoEs : '',
    textoUk : '',
    dateField : {},
    datePanel : {},
    panelComodinesCa : {},
    panelComodinesEs : {},
    panelComodinesUk : {},
    tabPanelPlantillaCertificado : {},
    fechaInicioCertificadoField : {},
    plantillaCa : {},
    plantillaEs : {},
    plantillaUk : {},
    actualiza : false,
    textoCa : '',
    textoEs : '',
    textoUk : '',

    initComponent : function()
    {
        var config = {};

        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.CEO.EmisionCertificadosWizardPlantilla.superclass.initComponent.call(this);

        this.initUI();
    },

    initUI : function()
    {

        this.buildDateField();
        this.buildFechaInicioCertificadoField();
        this.buildDatePanel();
        this.buildPanelComodinesCa();
        this.buildPanelComodinesEs();
        this.buildPanelComodinesUk();
        this.buildPlantillaCa();
        this.buildPlantillaEs();
        this.buildPlantillaUk();
        this.buildTabPanelPlantillaCertificado();

        this.add(this.datePanel);
        this.add(this.tabPanelPlantillaCertificado);

    },

    listeners :
    {
        activate : function()
        {
            if (this.actualiza)
            {
                this.plantillaCa.setValue(this.textoCa);
                this.plantillaEs.setValue(this.textoEs);
                this.plantillaUk.setValue(this.textoUk);
                this.setTags();
                this.actualiza = false;
            }
        }
    },

    buildDatePanel : function()
    {
        this.datePanel = new Ext.Panel(
        {
            frame: false,
            layout :
            {
                type : 'hbox',
                padding : 5,
                border: false
            },
            items : [
            {
                xtype : 'panel',
                layout: 'form',
                flex: 1,
                frame: false,
                border: false,
                labelWidth : 120,
                items : [ this.dateField ]
            },
            {
                xtype : 'panel',
                layout: 'form',
                flex: 1,
                frame: false,
                border: false,
                labelWidth : 120,
                items : [ this.fechaInicioCertificadoField ]
            } ]
        })
    },

    buildTabPanelPlantillaCertificado : function()
    {
        this.tabPanelPlantillaCertificado = new Ext.TabPanel(
        {
            activeTab : 0,
            padding : 20,
            items : [
            {
                xtype : 'panel',
                layout : 'form',
                title : 'Català',
                padding : 10,
                autoHeight : true,
                items : [ this.plantillaCa, this.panelComodinesCa ]

            },
            {
                xtype : 'panel',
                layout : 'form',
                title : 'Castellano',
                padding : 10,
                autoHeight : true,
                items : [ this.plantillaEs, this.panelComodinesEs ]

            },
            {
                xtype : 'panel',
                layout : 'form',
                title : 'English',
                padding : 10,
                autoHeight : true,
                items : [ this.plantillaUk, this.panelComodinesUk ]

            } ]
        });
    },

    buildPlantillaCa : function()
    {
        this.plantillaCa = new Ext.form.TextArea(
        {
            fieldLabel : 'Text',
            name : 'textoSinProcesarCa',
            width : 580,
            height : 100,
            readOnly : true
        });
    },

    buildPlantillaEs : function()
    {
        this.plantillaEs = new Ext.form.TextArea(
        {
            fieldLabel : 'Text',
            name : 'textoSinProcesarEs',
            width : 580,
            height : 100,
            readOnly : true
        });
    },

    buildPlantillaUk : function()
    {
        this.plantillaUk = new Ext.form.TextArea(
        {
            fieldLabel : 'Text',
            name : 'textoSinProcesarUk',
            width : 580,
            height : 100,
            readOnly : true
        });
    },

    buildDateField : function()
    {
        this.dateField = new Ext.form.DateField(
        {
            name : 'fechaCertificado',
            fieldLabel : 'Data emissió',
            flex: 1,
            frame: false,
            border: false,
            allowBlank : false,
            width: 100,
            format : 'd/m/Y',
            value : new Date(),
            startDay : 1
        });
    },

    buildFechaInicioCertificadoField : function()
    {
        this.fechaInicioCertificadoField = new Ext.form.DateField(
        {
            name : 'fechaInicioCertificado',
            frame: false,
            border: false,
            fieldLabel : 'Data inici certificat',
            width: 100,
            allowBlank : false,
            format : 'd/m/Y',
            value : new Date(),
            startDay : 1
        });
    },

    buildPanelComodinesCa : function()
    {
        this.panelComodinesCa = new Ext.Panel(
        {
            layout : 'form',
            autoScroll : true,
            border : true,
            fieldLabel : 'Comodins',
            width : 580,
            height : 180,
            labelWidth : 160,
            padding : 2
        });
    },

    buildPanelComodinesEs : function()
    {
        this.panelComodinesEs = new Ext.Panel(
        {
            layout : 'form',
            autoScroll : true,
            border : true,
            fieldLabel : 'Comodines',
            width : 580,
            height : 180,
            labelWidth : 160,
            padding : 2
        });
    },

    buildPanelComodinesUk : function()
    {
        this.panelComodinesUk = new Ext.Panel(
        {
            layout : 'form',
            autoScroll : true,
            border : true,
            fieldLabel : 'Wildcards',
            width : 580,
            height : 180,
            labelWidth : 160,
            padding : 2
        });
    },

    updateGridPlantillaCertificado : function()
    {
        var items = this.panelPlantillaCertificado.items.items;

        for (var j = 0; j < items.length; j++)
        {
            if (items[j].disabled)
            {
                var variable = items[j].name;
                var descripcion = this.getDescripcionFromTag(variable);

                items[j].setValue(descripcion);
            }
        }
    },

    setTexto : function(textoCa, textoEs, textoUk)
    {
        this.textoCa = textoCa;
        this.textoEs = textoEs;
        this.textoUk = textoUk;
        this.actualiza = true;
    },

    getPanelComodines : function(lang)
    {
        var panelComodines = this.panelComodinesCa;
        if (lang == "es")
        {
            panelComodines = this.panelComodinesEs;
        }
        else if (lang == "uk")
        {
            panelComodines = this.panelComodinesUk;
        }
        return panelComodines;
    },

    getTexto : function(lang)
    {

        if (lang == "ca")
        {
            return this.textoCa;
        }
        else if (lang == "es")
        {
            return this.textoEs;
        }
        else
        {
            return this.textoUk;
        }
    },
    setTags : function()
    {

        var ref = this;
        Ext.Ajax.request(
        {
            url : 'rest/variable-plantilla',
            method : 'GET',
            success : function(response)
            {
                var base = response.responseXML.firstChild.childNodes;
                var langs = [ 'ca', 'es', 'uk' ];
                for (var i = 0; i < langs.length; i++)
                {
                    var lang = langs[i];

                    var panelComodines = ref.getPanelComodines(lang);
                    var texto = ref.getTexto(lang);
                    panelComodines.removeAll();
                    var comodines = [];

                    for (var j = 0; j < base.length; j++)
                    {
                        var tag = Ext.DomQuery.selectValue('variable', base[j]);

                        if (base[j].nodeType == 1 && comodines.indexOf(tag))
                        {
                            var descripcion = ref.getDescripcionFromTag(tag);
                            comodines.push(tag);

                            var field = new Ext.form.TextField(
                            {
                                name : tag,
                                fieldLabel : tag,
                                disabled : true,
                                width : '90%'
                            });

                            field.setValue(descripcion);

                            panelComodines.add(field);
                        }
                    }

                    panelComodines.doLayout();

                    ref.setTagsFromPlantilla(texto, lang);
                }
            },
            failure : function()
            {
                Ext.MessageBox.show(
                {
                    title : 'Errada',
                    msg : "Errada a l''importar els comodins comuns",
                    buttons : Ext.MessageBox.OK,
                    icon : Ext.MessageBox.ERROR
                });
            }
        });
    },

    getDescripcionFromTag : function(tag)
    {
        var descripcion = "";

        var panel = this.ownerCt.emisionCertificadosWizardDestinatario;

        tipoUsuario = panel.radioTipoUsuario.getValue().inputValue;

        if (tag == '${dni}')
        {
            if (tipoUsuario == UJI.CEO.TipoUsuario.EXTERNO)
            {
                descripcion = panel.textFieldDNIUsuarioExterno.getValue();
            }
            else
            {
                descripcion = panel.textFieldDNIUsuarioInterno.lastSelectionText;
            }
        }

        if (tag == '${email}')
        {
            if (tipoUsuario == UJI.CEO.TipoUsuario.EXTERNO)
            {
                descripcion = panel.textFieldEMailUsuarioExterno.getValue();
            }
            else
            {
                descripcion = panel.textFieldEMailUsuarioInterno.getValue();
            }
        }

        if (tag == '${nombre}')
        {
            if (tipoUsuario == UJI.CEO.TipoUsuario.EXTERNO)
            {
                descripcion = panel.textFieldDestinatarioUsuarioExterno.getValue();
            }
            else
            {
                descripcion = panel.textFieldDestinatarioUsuarioInterno.lastSelectionText;
            }
        }

        return descripcion;
    },

    setTagsFromPlantilla : function(texto, lang)
    {
        var panelComodines = this.getPanelComodines(lang);
        var ref = this;
        var etiquetas = UJI.CEO.Application.extractTags(texto);

        if (etiquetas)
        {
            for (var i = 0; i < etiquetas.length; i++)
            {
                if (!ref.panelContains(etiquetas[i], lang))
                {
                    var field = new Ext.form.TextField(
                    {
                        name : etiquetas[i],
                        fieldLabel : etiquetas[i],
                        width : '90%'
                    });

                    panelComodines.add(field);
                }
            }
            panelComodines.doLayout();
        }
    },

    panelContains : function(item, lang)
    {
        var panelComodines = this.getPanelComodines(lang);

        var items = panelComodines.items.items;

        for (var j = 0; j < items.length; j++)
        {
            var variable = items[j].name;

            if (variable == item)
            {
                return true;
            }
        }

        return false;
    },

    checkMandatoryFields : function()
    {
        return true;
    }
});