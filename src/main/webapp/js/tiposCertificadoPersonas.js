Ext.ns('UJI.CEO');

var RegistroNoSeleccionado = 0;

UJI.CEO.TiposCertificadoPersonas = Ext.extend(Ext.Panel,
{
    title : 'Persones',
    layout : 'vbox',
    layoutConfig :
    {
        align : 'stretch'
    },
    botonBorrarPersona : {},
    botonAnadirPersona : {},
    storeCertificadoPersonas : {},
    gridPersonas : {},
    tipoCertificadoId : null,

    initComponent : function()
    {
        var config = {};

        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.CEO.TiposCertificadoPersonas.superclass.initComponent.call(this);

        this.initUI();

        this.add(this.gridPersonas);
    },

    initUI : function()
    {
        this.buildStoreCertificadoPersonas();

        this.buildBotonBorrarPersona();
        this.buildBotonAnadirPersona();

        this.buildGridPersonas();
    },

    buildStoreCertificadoPersonas : function()
    {
        this.storeCertificadoPersonas = new Ext.data.Store(
        {
            restful : true,
            proxy : new Ext.data.HttpProxy(
            {
                url : '/ceo/rest/tipocertificado/' + this.tipoCertificadoId + '/permisopersona'
            }),
            autoLoad : false,
            reader : new Ext.data.XmlReader(
            {
                record : 'Persona',
                id : 'id'
            }, [ 'id', 'identificacion', 'nombreCompleto' ]),
            writer : new Ext.data.XmlWriter(
            {
                xmlEncoding : 'UTF-8',
                writeAllFields : true
            })
        });
    },

    buildBotonAnadirPersona : function()
    {
        var ref = this;

        this.addEvents('LookoupWindowClickSeleccion');
        var window = new Ext.ux.uji.form.LookupWindow(
        {
            appPrefix : 'ceo',
            bean : 'persona'
        });

        window.on('LookoupWindowClickSeleccion', function(record)
        {
            var rec = new ref.storeCertificadoPersonas.recordType(
            {
                id : record.data.id,
                nombreCompleto : record.data.nombre
            });

            ref.storeCertificadoPersonas.proxy.setUrl('/ceo/rest/tipocertificado/' + ref.tipoCertificadoId + '/permisopersona');
            ref.storeCertificadoPersonas.insert(0, rec);
        });

        this.botonAnadirPersona = new Ext.Button(
        {
            text : 'Afegir',
            iconCls : 'application-add',
            handler : function(button)
            {
                if (ref.tipoCertificadoId)
                {
                    window.show();
                }
            }
        });
    },

    buildBotonBorrarPersona : function()
    {
        var ref = this;

        this.botonBorrarPersona = new Ext.Button(
        {
            text : 'Esborrar',
            iconCls : 'application-delete',
            handler : function(boton, evento)
            {
                if (ref.tipoCertificadoId)
                {
                    var registroSeleccionado = ref.gridPersonas.getSelectionModel().getSelected();

                    if (!registroSeleccionado)
                    {
                        Ext.MessageBox.show(
                        {
                            title : 'Error',
                            msg : "Cal seleccionar un registre a esborrar",
                            buttons : Ext.MessageBox.OK,
                            icon : Ext.MessageBox.ERROR
                        });
                    }
                    else
                    {
                        Ext.Msg.confirm("Confirmació de la operació", "Realment dessitja esborrar aquest registre?", function(button, text)
                        {
                            if (button == "yes")
                            {
                                ref.storeCertificadoPersonas.proxy.setUrl('/ceo/rest/tipocertificado/' + ref.tipoCertificadoId + '/permisopersona');
                                ref.storeCertificadoPersonas.remove(registroSeleccionado);
                            }
                        });
                    }
                }
            }
        });
    },

    buildGridPersonas : function()
    {
        var ref = this;

        var colModel = new Ext.grid.ColumnModel(
        {
            defaults :
            {
                sortable : true
            },
            columns : [
            {
                header : "id",
                width : 25,
                dataIndex : 'id',
                hidden : true
            },
            {
                header : 'Nom',
                dataIndex : 'nombreCompleto'
            },
            {
                header : 'DNI',
                dataIndex : 'identificacion'
            } ]
        });

        this.gridPersonas = new Ext.grid.GridPanel(
        {
            frame : true,
            flex : 1,
            tbar :
            {
                items : [ ref.botonAnadirPersona, ref.botonBorrarPersona ]
            },
            viewConfig :
            {
                forceFit : true
            },
            store : ref.storeCertificadoPersonas,
            sm : new Ext.grid.RowSelectionModel(
            {
                singleSelect : true
            }),
            colModel : colModel
        });

    },
    setTipoCertificadoId : function(tipoCertificadoId)
    {
        this.tipoCertificadoId = tipoCertificadoId;
    },

    loadStore : function(mask)
    {
        this.storeCertificadoPersonas.proxy.setUrl('/ceo/rest/tipocertificado/' + this.tipoCertificadoId + '/permisopersona');
        this.storeCertificadoPersonas.load(
        {
            callback : function()
            {
                mask.hide();
            }
        });
    }
});
