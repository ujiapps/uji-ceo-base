Ext.ns('UJI.CEO');

UJI.CEO.TiposCertificadoTabPlantillas = Ext
        .extend(
                Ext.FormPanel,
                {
                    title : 'Plantilla',
                    layout : 'hbox',
                    autoScroll : true,
                    plantillaCa : null,
                    plantillaEs : null,
                    plantillaUk : null,
                    saveButton : null,
                    fbar : [],
                    flex : 1,
                    gridCertificados : {},
                    tipoCertificadoId : 0,
                    comodinesPanel : {},
                    botonEliminar : {},
                    formGestionImagenes : {},
                    dataviewImagenes : {},
                    campoContentType : {},
                    panelImagenes : {},
                    panelIzquierda : {},
                    previewButtonCa : {},
                    previewButtonEs : {},
                    previewButtonEn : {},
                    storeTipoCertificado : {},
                    panelDerecha : {},
                    tabPanelIzquierda : {},
                    storeImagenTipoCertificado : {},
                    fileUpload : true,

                    initComponent : function()
                    {
                        var config = {};

                        Ext.apply(this, Ext.apply(this.initialConfig, config));
                        UJI.CEO.TiposCertificadoTabPlantillas.superclass.initComponent.call(this);

                        this.initUI();
                        this.setReader();
                    },

                    initUI : function()
                    {
                        this.buildStoreImagenTipoCertificado();
                        this.buildBotonEliminar();
                        this.buildPreviewButtonCa();
                        this.buildPreviewButtonEs();
                        this.buildPreviewButtonEn();
                        this.buildCampoContentType();
                        this.buildCampoImagen();
                        this.buildDataviewImagenes();
                        this.buildFormGestionImagenes();
                        this.buildPanelImagenes();
                        this.buildPanelEtiquetasFormato();

                        this.plantillaCa = this.getFormPlantilla("Ca", "Català", "Text");
                        this.plantillaEs = this.getFormPlantilla("Es", "Castellà", "Texto");
                        this.plantillaUk = this.getFormPlantilla("Uk", "Anglés", "Text");

                        this.buildComodinesPanel();
                        this.buildTabPanelIzquierda();
                        this.buildPanelDerecha();

                        this.add(this.tabPanelIzquierda);
                        this.add(this.panelDerecha);
                        this.add(
                        {
                            xtype : 'hidden',
                            name : 'id'
                        });

                        this.buildSaveButton();
                        this.getFooterToolbar().add(this.saveButton);

                    },

                    buildStoreImagenTipoCertificado : function()
                    {
                        var ref = this;
                        this.storeImagenTipoCertificado = new Ext.data.Store(
                        {
                            restful : true,
                            url : '/ceo/rest/tipocertificado',
                            autoLoad : false,
                            reader : new Ext.data.XmlReader(
                            {
                                record : 'TipoCertificado',
                                id : 'id'
                            }, [ 'id' ]),
                            writer : new Ext.data.XmlWriter(
                            {
                                xmlEncoding : 'UTF-8',
                                writeAllFields : true
                            }),
                            listeners :
                            {
                                beforeload : function(store)
                                {
                                    store.proxy.setUrl("/ceo/rest/tipocertificado/" + ref.tipoCertificadoId + "/imagen");
                                }
                            }
                        });
                    },

                    buildPanelDerecha : function()
                    {
                        var ref = this;

                        this.panelDerecha = new Ext.form.FieldSet(
                        {
                            flex : 1,
                            border : false,
                            width : (ref.label ? 280 : 250),
                            labelWidth : (ref.label ? 35 : 5),
                            items : [ this.comodinesPanel, this.panelEtiquetasFormato ]
                        });
                    },

                    setReader : function()
                    {
                        var listaParametros = [ "id", "descripcionCa", "descripcionEs", "descripcionEn", "plantillaCa", "plantillaEs", "plantillaUk", "contentType" ];

                        this.getForm().reader = new Ext.data.XmlReader(
                        {
                            record : 'TipoCertificado'
                        }, listaParametros);

                        this.getForm().errorReader = new Ext.data.XmlReader(
                        {
                            record : 'responseMessage',
                            success : 'success'
                        }, [ 'success', 'msg' ]);
                    },

                    loadForm : function(mask)
                    {
                        var ref = this;
                        this.getForm().load(
                        {
                            url : "/ceo/rest/tipocertificado/" + this.tipoCertificadoId,
                            method : 'GET',
                            failure : function(f, action)
                            {
                                Ext.MessageBox.show(
                                {
                                    title : 'Error',
                                    msg : "Errada carregant el formulari",
                                    buttons : Ext.MessageBox.OK,
                                    icon : Ext.MessageBox.ERROR
                                });
                            },
                            success : function(f, action)
                            {
                                ref.storeImagenTipoCertificado.load(
                                {
                                    callback : function(r)
                                    {
                                        if (r.length == 1)
                                        {
                                            ref.botonEliminar.setDisabled(false);
                                        }
                                        else
                                        {
                                            ref.botonEliminar.setDisabled(true);
                                        }
                                    }
                                });
                                if (mask) {
                                    mask.hide();
                                }
                            }
                        });
                    },

                    imprimirCertificado : function(idioma)
                    {
                        idiomaId = idioma;
                        window.open("/ceo/rest/pdf/preview/?idiomaId=" + idiomaId + "&tipoCertificadoId=" + this.tipoCertificadoId);
                    },

                    buildPreviewButtonCa : function()
                    {
                        var ref = this;
                        this.previewButtonCa = new Ext.Button(
                        {
                            text : 'Previsualitzar',
                            iconCls : 'printer',
                            handler : function(button, event)
                            {
                                ref.imprimirCertificado("CA");
                            }
                        });
                    },

                    buildPreviewButtonEs : function()
                    {
                        var ref = this;
                        this.previewButtonEs = new Ext.Button(
                        {
                            text : 'Previsualitzar',
                            iconCls : 'printer',
                            handler : function(button, event)
                            {
                                ref.imprimirCertificado("ES");
                            }
                        });
                    },

                    buildPreviewButtonEn : function()
                    {
                        var ref = this;
                        this.previewButtonEn = new Ext.Button(
                        {
                            text : 'Previsualitzar',
                            iconCls : 'printer',
                            handler : function(button, event)
                            {
                                ref.imprimirCertificado("EN");
                            }
                        });
                    },

                    buildSaveButton : function()
                    {
                        var ref = this;

                        this.saveButton = new Ext.Button(
                        {
                            text : 'Desar',
                            handler : function(button, event)
                            {
                                if (ref.getForm().isValid() && ref.tipoCertificadoId)
                                {
                                    var url = "/ceo/rest/tipocertificado/" + ref.tipoCertificadoId + "/plantillas";
                                    var tabName = ref.tabPanelIzquierda.activeTab.name;
                                    if (tabName === "logo")
                                    {
                                        url = "/ceo/rest/tipocertificado/" + ref.tipoCertificadoId + "/logo";
                                    }

                                    ref.getForm().submit(
                                    {
                                        url : url,
                                        method : 'post',
                                        success : function(form, action)
                                        {
                                            ref.loadForm();
                                        },
                                        failure : function(form, action)
                                        {
                                            Ext.MessageBox.show(
                                            {
                                                title : 'Error',
                                                msg : "Errada al desar les dades",
                                                buttons : Ext.MessageBox.OK,
                                                icon : Ext.MessageBox.ERROR
                                            });
                                        }
                                    });
                                }
                                else
                                {
                                    Ext.MessageBox.show(
                                    {
                                        title : 'Error',
                                        msg : "Reviseu tots els camps del formulari i que estiga seleccionada una fila vàlida de la taula superior",
                                        buttons : Ext.MessageBox.OK,
                                        icon : Ext.MessageBox.ERROR
                                    });
                                }
                            }
                        });
                    },

                    buildBotonEliminar : function()
                    {
                        var ref = this;
                        this.botonEliminar = new Ext.Button(
                        {
                            text : 'Esborrar imatge',
                            iconCls : 'cancel',
                            handler : function()
                            {
                                if (ref.storeImagenTipoCertificado.data.length > 0)
                                {
                                    Ext.Msg.confirm('Esborrar imatge', 'Esteu segur/a de voler esborrar la imatge ?', function(btn, text)
                                    {
                                        if (btn == 'yes')
                                        {
                                            Ext.Ajax.request(
                                            {
                                                url : "/ceo/rest/tipocertificado/" + ref.tipoCertificadoId + "/imagen",
                                                method : "DELETE",
                                                success : function()
                                                {
                                                    ref.storeImagenTipoCertificado.reload();
                                                }
                                            });
                                        }
                                    });
                                }
                            }
                        });
                    },

                    buildCampoContentType : function()
                    {
                        this.campoContentType = new Ext.form.TextField(
                        {
                            name : "contentType",
                            allowBlank : true,
                            hidden : true
                        });
                    },

                    buildCampoImagen : function()
                    {
                        this.campoImagen = new Ext.form.TextField(
                        {
                            fieldLabel : 'Seleccionar imatge',
                            name : 'imagen',
                            allowBlank : true,
                            inputType : 'file'
                        });
                    },
                    buildFormGestionImagenes : function()
                    {
                        this.formGestionImagenes = new Ext.Panel(
                        {
                            items : [
                            {
                                'xtype' : 'panel',
                                layout : 'hbox',
                                items : [
                                {
                                    'xtype' : 'fieldset',
                                    'title' : 'Pujar una nova imatge',
                                    layout : 'hbox',
                                    height : 70,
                                    flex : 1,
                                    labelWidth : 150,
                                    items : [ this.campoImagen, this.campoContentType ]
                                } ]
                            } ]
                        });
                    },

                    buildDataviewImagenes : function()
                    {
                        var imageTemplate = new Ext.XTemplate('<tpl for=".">', '<div class="thumb-wrap">', '<div class="thumb"><img src="/ceo/rest/tipocertificado/{id}/imagen/raw" /></div>',
                                '</div><br style="clear: both;" />', '</tpl>');

                        this.dataviewImagenes = new Ext.DataView(
                        {
                            store : this.storeImagenTipoCertificado,
                            tpl : imageTemplate,
                            singleSelect : true,
                            overClass : 'x-view-over',
                            itemSelector : 'div.thumb-wrap',
                            loadingText : 'Loading Images...'
                        });
                    },

                    buildPanelImagenes : function()
                    {
                        var ref = this;

                        this.panelImagenes = new Ext.Panel(
                        {
                            frame : true,
                            height : 300,
                            collapsible : false,
                            tbar : [ ref.botonEliminar ],
                            title : 'Logo',
                            name : 'logo',
                            items : [ ref.dataviewImagenes, ref.formGestionImagenes ]
                        });
                    },

                    getFormPlantilla : function(lang, title, label)
                    {
                        var button = undefined;
                        if (lang === "Ca")
                        {
                            button = this.previewButtonCa;
                        }
                        else if (lang === "Es")
                        {
                            button = this.previewButtonEs;
                        }
                        else
                        {
                            button = this.previewButtonEn;
                        }
                        return new Ext.Panel(
                        {
                            title : title,
                            name : 'plantilla' + lang,
                            padding : 2,
                            border : false,
                            labelWidth : 40,
                            flex : 1,
                            autoHeight : true,
                            layout : "fit",
                            tbar : [ button ],
                            items : [
                            {
                                xtype : 'fieldset',
                                border : false,
                                flex : 1,
                                autoHeight : true,
                                items : [
                                {
                                    xtype : 'textarea',
                                    authHeight : true,
                                    height : 300,
                                    anchor : "90%",
                                    fieldLabel : label,
                                    name : 'plantilla' + lang
                                } ]
                            } ]
                        });
                    },

                    buildTabPanelIzquierda : function()
                    {
                        this.tabPanelIzquierda = new Ext.TabPanel(
                        {
                            activeTab : 0,
                            border : false,
                            autoHeight : true,
                            deferredRender : true,
                            flex : 1,
                            items : [ this.plantillaCa, this.plantillaEs, this.plantillaUk, this.panelImagenes ]
                        });

                    },
                    buildComodinesPanel : function()
                    {
                        this.comodinesPanel = new UJI.CEO.VariablesPlantillaPanel(
                        {
                            flex : 1,
                            padding : 10,
                            title : 'Comodins'
                        });
                    },

                    buildPanelEtiquetasFormato : function()
                    {
                        this.panelEtiquetasFormato = new Ext.Panel(
                                {
                                    flex : 1,
                                    padding : 10,
                                    style : 'margin-top: 10px',
                                    title : 'Etiquetes de formateig',
                                    html : '<span class="comodin-nombre">&lt;b&gt;</span><span style="font-weight: bold;">Negreta</span><span class="comodin-nombre">&lt;/b&gt;</span><br /><span class="comodin-nombre">&lt;i&gt;</span><span style="font-style: italic;">Cursiva</span><span class="comodin-nombre">&lt;/i&gt;</span><br /><span class="comodin-nombre">&lt;u&gt;</span><span style="text-decoration: underline;">Subratllat</span><span class="comodin-nombre">&lt;/u&gt;</span>'
                                });
                    },

                    setTipoCertificadoId : function(tipoCertificadoId)
                    {
                        this.tipoCertificadoId = tipoCertificadoId;
                    },

                    loadStore : function(mask)
                    {
                        this.loadForm(mask);
                    }

                });