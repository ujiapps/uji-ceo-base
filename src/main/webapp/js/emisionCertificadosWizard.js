Ext.ns('UJI.CEO');

UJI.CEO.EmisionCertificadosWizard = Ext.extend(Ext.FormPanel,
{
    layout : 'card',
    activeItem : 0,
    bbar : [],
    textItem : {},
    atrasButton : {},
    adelanteButton : {},
    closeButton : {},
    emisionCertificadosWizardTipoCertificado : {},
    emisionCertificadosWizardDescripcionYAnverso : {},
    emisionCertificadosWizardDestinatario : {},
    emisionCertificadosWizardPlantilla : {},
    emisionCertificadosWizardPrevisualizacion : {},

    initComponent : function()
    {
        var config = {};

        Ext.apply(this, Ext.apply(this.initialConfig, config));
        UJI.CEO.EmisionCertificadosWizard.superclass.initComponent.call(this);

        this.initUI();

        this.getForm().errorReader = new Ext.data.XmlReader(
        {
            record : 'responseMessage',
            success : 'success'
        }, [ 'success', 'msg' ]);
    },

    initUI : function()
    {
        this.buildEmisionCertificadosWizardTipoCertificado();
        this.buildEmisionCertificadosWizardDescripcionYAnverso();
        this.buildEmisionCertificadosWizardDestinatario();
        this.buildEmisionCertificadosWizardPlantilla();
        this.buildEmisionCertificadosWizardPrevisualizacion();

        this.add(this.emisionCertificadosWizardTipoCertificado);
        this.add(this.emisionCertificadosWizardDescripcionYAnverso);
        this.add(this.emisionCertificadosWizardDestinatario);
        this.add(this.emisionCertificadosWizardPlantilla);
        this.add(this.emisionCertificadosWizardPrevisualizacion);

        this.buildText();
        this.buildAtrasButton();
        this.buildAdelanteButton();
        this.buildCloseButton();

        this.getBottomToolbar().addItem(this.textItem);
        this.getBottomToolbar().addFill();
        this.getBottomToolbar().addButton(this.atrasButton);
        this.getBottomToolbar().addButton(this.adelanteButton);
        this.getBottomToolbar().addSpacer();
        this.getBottomToolbar().addSeparator();
        this.getBottomToolbar().addSpacer();
        this.getBottomToolbar().addButton(this.closeButton);
    },

    buildText : function()
    {
        var maxItems = this.items.items.length;

        this.textItem = new Ext.Toolbar.TextItem(
        {
            text : 'Pas 1 de ' + maxItems
        });
    },

    buildAdelanteButton : function()
    {
        var ref = this;

        this.adelanteButton = new Ext.Button(
        {
            text : 'Següent &raquo;',
            handler : function()
            {
                ref.cambiaLayout(1);
            }
        });
    },

    buildAtrasButton : function()
    {
        var ref = this;

        this.atrasButton = new Ext.Button(
        {
            text : '&laquo; Previ',
            disabled : true,
            handler : function()
            {
                ref.cambiaLayout(-1);
            }
        });
    },

    buildCloseButton : function()
    {
        var ref = this;

        this.closeButton = new Ext.Button(
        {
            text : 'Cancel·lar',
            handler : function()
            {
                ref.ownerCt.close();
            }
        });
    },

    cambiaLayout : function(valor)
    {
        var layout = this.getLayout();

        if (this.activeItem === 0 && valor === 1)
        {
            this.emisionCertificadosWizardPlantilla.actualiza = true;
        }

        if (valor === -1 || layout.activeItem.checkMandatoryFields())
        {
            var activeItem = this.activeItem;
            var maxItems = this.items.items.length;

            layout.setActiveItem(activeItem + valor);
            this.activeItem = activeItem + valor;

            this.atrasButton.setDisabled(this.activeItem == 0);
            this.adelanteButton.setDisabled((this.activeItem + 1) == maxItems);

            this.textItem.update('Pas ' + (this.activeItem + 1) + ' de ' + maxItems);
        }
    },

    buildEmisionCertificadosWizardTipoCertificado : function()
    {
        this.emisionCertificadosWizardTipoCertificado = new UJI.CEO.EmisionCertificadosWizardTipoCertificado({

        });
    },

    buildEmisionCertificadosWizardDescripcionYAnverso : function()
    {
        this.emisionCertificadosWizardDescripcionYAnverso = new UJI.CEO.EmisionCertificadosWizardDescripcionYAnverso({

        });
    },

    buildEmisionCertificadosWizardDestinatario : function()
    {
        this.emisionCertificadosWizardDestinatario = new UJI.CEO.EmisionCertificadosWizardDestinatario({

        });
    },

    buildEmisionCertificadosWizardPlantilla : function()
    {
        this.emisionCertificadosWizardPlantilla = new UJI.CEO.EmisionCertificadosWizardPlantilla({

        });
    },

    buildEmisionCertificadosWizardPrevisualizacion : function()
    {
        this.emisionCertificadosWizardPrevisualizacion = new UJI.CEO.EmisionCertificadosWizardPrevisualizacion({

        });
    }
});